import json
import MySQLdb
import redis
from config import REDIS_DB_HOST, MYSQL_DB_USER, MYSQL_DB_HOST, MYSQL_DB_PASSWORD, MYSQL_DB_PORT, MYSQL_DB_NAME

def get_finished_schedule_request():
    employee_schedule = []
    schedule_request_id_list = []
    r = redis.Redis(host=REDIS_DB_HOST)
    for _ in range(5):
        schedule_request_id = r.lpop("finished_schedule_request_queue")
        if not schedule_request_id:
            break
        schedule_request_id_list.append(schedule_request_id.decode('utf-8'))
        res_str = r.get(f"schedule_request_result:{schedule_request_id.decode('utf-8')}")
        r.delete(f"schedule_request_result:{schedule_request_id.decode('utf-8')}")
        res_list = json.loads(res_str.decode('utf-8'))
        employee_schedule += res_list
    
    return employee_schedule, schedule_request_id_list

def get_schedule_request_update_query(schedule_request_id_list):
    return f"""
    UPDATE schedule_request SET status='DONE' WHERE {' or '.join([f"id = {schedule_req_id}" for schedule_req_id in schedule_request_id_list])}
    """

def bulk_insert_employee_schedule():
    mysql_conn = MySQLdb.connect(user=MYSQL_DB_USER, host=MYSQL_DB_HOST, password=MYSQL_DB_PASSWORD, port=int(MYSQL_DB_PORT), database=MYSQL_DB_NAME)
    cursor = mysql_conn.cursor()
    employee_schedules, schedule_request_id_list = get_finished_schedule_request()
    bulk_insert_query = "INSERT INTO employee_schedule (`schedule_request_id`, `employee_id`, `shift_id`, `day`, `schedule_status`) VALUES "
    
    if len(employee_schedules) == 0:
        return

    try:
        for iter_idx in range(len(employee_schedules)):
            employee_schedule = employee_schedules[iter_idx]
            
            if (iter_idx % 40 == 0 and iter_idx != 0) or (iter_idx  == len(employee_schedules) - 1):
                bulk_insert_query += f"('{employee_schedule['schedule_request_id']}', '{employee_schedule['employee_id']}', '{employee_schedule['shift_id']}', '{employee_schedule['day']}', '{employee_schedule['schedule_status']}');"
                print("inserting 40 employee_schedule...")
                cursor.execute(bulk_insert_query)
                bulk_insert_query = "INSERT INTO employee_schedule (`schedule_request_id`, `employee_id`, `shift_id`, `day`, `schedule_status`) VALUES "
            else:
                bulk_insert_query += f"('{employee_schedule['schedule_request_id']}', '{employee_schedule['employee_id']}', '{employee_schedule['shift_id']}', '{employee_schedule['day']}', '{employee_schedule['schedule_status']}'), "   
        
        cursor.execute(get_schedule_request_update_query(schedule_request_id_list))
        mysql_conn.commit()
        cursor.close()
    except MySQLdb.Error as e:
        mysql_conn.rollback()
        cursor.close()
        raise e

bulk_insert_employee_schedule()