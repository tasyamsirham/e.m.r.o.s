import requests

from fastapi import FastAPI
from fastapi.responses import JSONResponse

from response_models import CreateScheduleResponse
from request_models import CreateScheduleRequest

from exceptions import ScheduleRequestInitiateException, ScheduleRequestPublishException
from utils import initiate_schedule_request, publish_message

app = FastAPI()

@app.post("/schedule", 
        response_model=CreateScheduleResponse, 
        responses={
            500: {"model": CreateScheduleResponse},
        })
async def schedule(request: CreateScheduleRequest):    
    try:
        initiate_schedule_request(request)
        publish_message(request)
    except ScheduleRequestInitiateException:
        return JSONResponse(status_code=500, content={
            "success": False,
            "message": "Schedule Request Initiation Gone Wrong. Please Try Again Later"
        })
    except ScheduleRequestPublishException:
        return JSONResponse(status_code=500, content={
            "success": False,
            "message": "Schedule Request Publish to Message Queue Gone Wrong. Please Try Again Later"
        })

    return JSONResponse(status_code=200, content={
        "success": True,
        "message": "Schedule Request Successfully Initiated. Check Schedule Request Details for More Info"
    })