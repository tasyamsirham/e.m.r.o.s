import os
import json
import MySQLdb
from config import PROJECT_ID, TOPIC_ID, DATABASE_USER, DATABASE_HOST, DATABASE_PASSWORD, DATABASE_PORT, DATABASE_NAME
from exceptions import ScheduleRequestPublishException, ScheduleRequestInitiateException
from google.cloud import pubsub_v1

def initiate_schedule_request(record):
    mysql_conn = MySQLdb.connect(user=DATABASE_USER, host=DATABASE_HOST, password=DATABASE_PASSWORD, port=int(DATABASE_PORT), database=DATABASE_NAME)
    cursor = mysql_conn.cursor()
    insert_query = f"""
    INSERT INTO `schedule_request` (`department_id`, `start_date`, `end_date`, `status`, `minimum_work_hour_weight`, `maximum_work_hour_weight`, `minimum_onsite_weight`,
    `room_capacity_weight`, `preference_weight`, `different_employee_per_shift_weight`, `different_employee_total_weight`) 
    VALUES ('{record.department_id}', '{record.start_date}', '{record.end_date}', 'PENDING', '{record.minimum_work_hour_weight}', '{record.maximum_work_hour_weight}', 
    '{record.minimum_onsite_weight}', '{record.room_capacity_weight}', '{record.preference_weight}', '{record.different_employee_per_shift_weight}', 
    '{record.different_employee_total_weight}');
    """
    
    try:
        cursor.execute(insert_query)
        mysql_conn.commit()
        cursor.close()
    except MySQLdb.Error as e:
        mysql_conn.rollback()
        cursor.close()
        raise ScheduleRequestInitiateException("Cannot initiate schedule request to DB, please try again later")

def publish_message(record):
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(PROJECT_ID, TOPIC_ID)
    
    dict_data = {
        "department_id": record.department_id, 
        "start_date": record.start_date, 
        "end_date": record.end_date,
        "minimum_work_hour_weight": record.minimum_work_hour_weight,
        "maximum_work_hour_weight": record.maximum_work_hour_weight,
        "minimum_onsite_weight": record.minimum_onsite_weight,
        "room_capacity_weight": record.room_capacity_weight,
        "preference_weight": record.preference_weight,
        "different_employee_per_shift_weight": record.different_employee_per_shift_weight,
        "different_employee_total_weight": record.different_employee_total_weight,
    }
    data = json.dumps(dict_data).encode("utf-8")
    
    try:
        future = publisher.publish(topic_path, data)
        print(future.result())
        print(f"Published messages to {topic_path}.")
    except:
        raise ScheduleRequestPublishException("Cannot publish schedule request to message queue, please try again later")