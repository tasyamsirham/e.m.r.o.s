import os

DATABASE_HOST = os.environ.get("MYSQL_DB_HOST")
DATABASE_NAME = os.environ.get("MYSQL_DB_NAME")
DATABASE_USER = os.environ.get("MYSQL_DB_USER")
DATABASE_PASSWORD = os.environ.get("MYSQL_DB_PASS")
DATABASE_PORT = os.environ.get("MYSQL_DB_PORT")

PROJECT_ID = os.environ.get("PROJECT_ID")
TOPIC_ID = os.environ.get("TOPIC_ID")

DATABASE_URL = f"mysql://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/{DATABASE_NAME}"