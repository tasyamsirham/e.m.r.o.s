from pydantic import BaseModel

class CreateScheduleRequest(BaseModel):
    department_id: int
    start_date: str
    end_date: str
    minimum_work_hour_weight: float
    maximum_work_hour_weight: float
    minimum_onsite_weight: float
    room_capacity_weight: float
    preference_weight: float
    different_employee_per_shift_weight: float
    different_employee_total_weight: float