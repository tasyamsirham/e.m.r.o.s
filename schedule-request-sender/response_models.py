from pydantic import BaseModel

class BaseResponse(BaseModel):
    success: bool

class CreateScheduleResponse(BaseResponse):
    message: str