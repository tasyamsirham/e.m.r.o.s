cd config
. deployment.sh

cd ../era
. deployment.sh

cd ../auth
. deployment.sh

cd ../schedule-request-sender
. deployment.sh

cd ../schedule-request-receiver
. deployment.sh

cd ../schedule-data-pipeline
. deployment.sh

cd ..