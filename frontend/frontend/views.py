from django.shortcuts import render, redirect


def dashboard_page(request):
    return render(request, "dashboard/dashboard.html")

def schedule_page(request):
    return render(request, "dashboard/schedule.html")

def department_page(request):
    return render(request, "dashboard/department.html")

def room_page(request):
    return render(request, "dashboard/room.html")

def shift_page(request):
    return render(request, "dashboard/shift.html")

def contract_page(request):
    return render(request, "dashboard/contract.html")

def schedule_request_page(request):
    return render(request, "dashboard/schedule_request.html")
    
def employee_page(request):
    return render(request, "dashboard/employee.html")

def login_page(request):
    return render(request, "login.html")


def register_page(request):
    return render(request, "sign-up.html")
