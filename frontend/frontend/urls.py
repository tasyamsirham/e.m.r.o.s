from django.urls import path
from frontend import views

urlpatterns = [
    path("", views.dashboard_page, name="dashboard_page"),
    path("schedule", views.schedule_page, name="schedule_page"),
    path("department", views.department_page, name="department_page"),
    path("room", views.room_page, name="room_page"),
    path("shift", views.shift_page, name="shift_page"),
    path("employee", views.employee_page, name="employee_page"),
    path("contract", views.contract_page, name="contract_page"),
    path("schedule_request", views.schedule_request_page, name="schedule_request_page"),
    path("login", views.login_page, name="login_page"),
    path("register", views.register_page, name="register_page"),
]
