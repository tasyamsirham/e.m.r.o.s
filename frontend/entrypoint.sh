#!/bin/bash
# set -x
set -eo pipefail

#Wrap to run on Bash Shell (used in non-interactive shell)
"$@"