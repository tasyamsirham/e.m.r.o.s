const create_preference_status = (employeeId, departmentId, shiftId, day, preferenceStatus) => {
    return $.ajax({
        type: "POST",
        url: "http://34.101.124.253/api/era/preference/v1/add",
        data: JSON.stringify({
            "employeeId": employeeId,
            "departmentId": departmentId,
            "shiftId": shiftId,
            "day": day,
            "preferenceStatus": preferenceStatus
        }),
        contentType: "application/json",
        dataType: 'json',
        error: (data) => {
          alert("Error:" + data.message)
        },
    });
}

const get_preference_status = (employee_id, start_date, end_date) => {
    return $.ajax({
        type: "GET",
        url: "http://34.101.124.253/api/era/preference/v1/all/employee",
        data: {
            "employeeId": employee_id,
            "day1": start_date.format("yyyy-MM-dd"),
            "day2": end_date.format("yyyy-MM-dd"),
        },
        contentType: "application/json",
        dataType: 'json',
        error: (data) => {
          alert("Error:" + data.message)
        },
    });
}

const delete_preference = (id) => {
    return $.ajax({
        type: "DELETE",
        url: `http://34.101.124.253/api/era/preference/v1/delete/${id}`,
        contentType: "application/json",
        dataType: 'text',
        error: (data) => {
          alert("Error:" + data.message)
        },
    })
}