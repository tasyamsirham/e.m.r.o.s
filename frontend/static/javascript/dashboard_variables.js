const date = new Date();

const days_class = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

const scheduleStatusMapping = ["On-site", "Remote", "Not-working"];

const scheduleColorMapping = ["light-slate-gray", "medium-purple", "picton-blue"];

const shiftById = {};

var isShiftFetched = false;

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const zeroPad = (num, places) => String(num).padStart(places, '0')

const fetchShiftData = () => {
  return $.ajax({
      method: "GET",
      url: `http://34.101.124.253/api/era/shift/v1/department/${localStorage.getItem("department_id")}`,
      xhrFields: {
        withCredentials: true
      },
      dataType: 'json',
      success: (data) => {
        for (const shift of data) {
            shiftById[shift.id] = shift;
        }
      }
  });
}

const fetchShiftPromise = fetchShiftData();