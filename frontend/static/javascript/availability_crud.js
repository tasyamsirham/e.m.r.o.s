const create_availability_status = (employeeId, departmentId, shiftId, day, availabilityStatus) => {
    return $.ajax({
        type: "POST",
        url: "http://34.101.124.253/api/era/availability/v1/add",
        data: JSON.stringify({
            "employeeId": employeeId,
            "departmentId": departmentId,
            "shiftId": shiftId,
            "day": day,
            "availabilityStatus": availabilityStatus
        }),
        error: (data) => {
          alert("Error:" + data.message)
        },
        contentType: "application/json",
        dataType: 'json'
    });
}

const get_availability_status = (employee_id, start_date, end_date) => {
    return $.ajax({
        type: "GET",
        url: "http://34.101.124.253/api/era/availability/v1/all/employee",
        data: {
            "employeeId": employee_id,
            "day1": start_date.format("yyyy-MM-dd"),
            "day2": end_date.format("yyyy-MM-dd"),
        },
        error: (data) => {
          alert("Error:" + data.message)
        },
        contentType: "application/json",
        dataType: 'json'
    });
}

const delete_availability = (id) => {
    return $.ajax({
        type: "DELETE",
        url: `http://34.101.124.253/api/era/availability/v1/delete/${id}`,
        contentType: "application/json",
        dataType: 'text',
        error: (data) => {
          alert("Error:" + data.message)
        },
    });
}