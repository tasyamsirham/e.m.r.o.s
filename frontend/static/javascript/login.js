$('div.sign-in-button').off('click').click(function () {
  email = $('#email').val();
  password = $('#password').val();
  
  return $.ajax({
      method: "POST",
      url: "http://34.101.219.147/api/auth/user-auth",
      xhrFields: {
        withCredentials: true
      },
      data: JSON.stringify({
        "email": email,
        "password": password
      }),
      success: (data) => { 
        localStorage.setItem("employee_id", data.employee_id);
        localStorage.setItem("department_id", data.department_id);
        localStorage.setItem("company_id", data.company_id);
        localStorage.setItem("role", data.role);
        console.log(document.cookie);
        //window.location.href = "/";
      },
      error: (data) => {
        alert("Error:" + data.message)
      },
      contentType: "application/json",
      dataType: 'json'
  });
});