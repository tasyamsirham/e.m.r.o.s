$('div.logout-button').off('click').click(function () {    
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/auth/user-logout",
        xhrFields: {
          withCredentials: true
        },
        success: (data) => { 
          localStorage.clear();
          window.location.href = "login";
        },
        error: (data) => {
          alert("Error:" + data.message)
        },
        contentType: "application/json",
        dataType: 'json'
    });
  });