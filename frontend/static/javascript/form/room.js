get_row_content = (room) => {
    return `
    <tr>
        <td>${room.name}</td>
        <td>${room.capacity} Employee</td>
        <td>
            <div class="action-flex">
                <div class="edit-button" id="${room.id}"></div>
                <div class="delete-button" id="${room.id}"></div>
            </div>
        </td>
    </tr>`
}

const activate_delete_button = () => {
    return $('div.delete-button').off('click').click(function () {
        id = $(this).attr("id");
        $.ajax({
            type: "DELETE",
            url: `http://34.101.124.253/api/era/room/v1/delete/${id}`,
            contentType: "application/json",
            dataType: 'text',
            success: (data) => {
                window.location.href = "room";
            }
        });
    });
}

edit_off = () => {
    $("#overlay-edit").hide();
}

edit_overlay_content = (id) => {
    return `<div class="sign-in-box background-white">    
        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div style="width: 80%; margin-right: 5px;">
                <p>Room</p>
                <input type="text" id="room-name">
            </div>

            <div style="width: 20%; margin-left: 5px;">
                <p>Capacity</p>
                <input type="number" id="capacity" style="text-align: center;">
            </div>
        </div>

        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div class="sign-in-button update-button" style="width: 75%;margin-right: 5px; margin-left: 5px;" id="${id}">Update</div>
            <div class="cancel-button" style="width: 25%;margin-right: 5px; margin-left: 5px;" onclick="edit_off()">Cancel</div>
        </div>
    </div>`
}

const activate_update_button = () => {
    return $('.update-button').off('click').click(function () {
        id = $(this).attr("id");
        room_name = $("#overlay-edit").find('#room-name').val();
        capacity = $("#overlay-edit").find('#capacity').val();
        
        $.ajax({
            method: "PUT",
            url: `http://34.101.124.253/api/era/room/v1/update/${id}`,
            xhrFields: {
                withCredentials: true
            },
            data: JSON.stringify({
                "name": room_name,
                "capacity": parseInt(capacity)
            }),
            success: (data) => { 
                alert("Success!");
                window.location.href = "room";
            },
            error: (data) => {
                alert("Error:" + data.message)
            },
            contentType: "application/json",
            dataType: 'json'
        });
    });
}

const activate_edit_button = () => {
    return $('div.edit-button').off('click').click(function () {
        id = $(this).attr("id");
        $("#overlay-edit").children(".overlay-container").html(edit_overlay_content(id));
        $("#overlay-edit").children(".overlay-container").hide();
        $("#overlay-edit").show();
        $("#overlay-edit").children(".overlay-container").slideToggle("slow");
        activate_update_button();
    });
}

$.ajax({
    type: "GET",
    url: `http://34.101.124.253/api/era/room/v1/all/${localStorage.getItem("company_id")}`,
    contentType: "application/json",
    dataType: 'json',
    success: (data) => {
        row_content = "";
        for (const room of data) {
            row_content += get_row_content(room);
        };
        $("table").append(row_content);
        activate_delete_button();
        activate_edit_button();
    }
});

$('.sign-in-button').off('click').click(function () {
    room_name = $("#overlay").find('#room-name').val();
    capacity = $("#overlay").find('#capacity').val();
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/era/room/v1/add",
        xhrFields: {
          withCredentials: true
        },
        data: JSON.stringify({
            "companyId": localStorage.getItem("company_id"),
            "name": room_name,
            "capacity": parseInt(capacity)
        }),
        success: (data) => { 
          alert("Success!");
          window.location.href = "room";
        },
        error: (data) => {
          alert("Error:" + data.message)
        },
        contentType: "application/json",
        dataType: 'json'
    });
  });