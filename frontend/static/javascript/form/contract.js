zeroPad = (num, places) => String(num).padStart(places, '0')

get_row_content = (contract) => {
    return `
    <tr>
        <td>${contract.name}</td>
        <td>${zeroPad(contract.minimumWorkHour, 2)}:00</td>
        <td>${zeroPad(contract.maximumWorkHour, 2)}:00</td>
        <td>
            <div class="action-flex">
                <div class="edit-button" id="${contract.id}"></div>
                <div class="delete-button" id="${contract.id}"></div>
            </div>
        </td>
    </tr>`
}

const activate_delete_button = () => {
    return $('div.delete-button').off('click').click(function () {
        id = $(this).attr("id");
        $.ajax({
            type: "DELETE",
            url: `http://34.101.124.253/api/era/contract/v1/delete/${id}`,
            contentType: "application/json",
            dataType: 'text',
            success: (data) => {
                window.location.href = "contract";
            }
        });
    });
}

edit_off = () => {
    $("#overlay-edit").hide();
}

edit_overlay_content = (id) => {
    return `<div class="sign-in-box background-white">
        <div>
        <p>Contract</p>
        <input type="text" id="contract-name">
        </div>
        
        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div style="width: 50%; margin-right: 5px;">
                <p>Minimum work hour</p>
                <input type="number" id="minimum-work-hour" style="text-align: center;">
            </div>

            <div style="width: 50%; margin-left: 5px;">
                <p>Maximum work hour</p>
                <input type="number" id="maximum-work-hour" style="text-align: center;">
            </div>
        </div>

        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div class="sign-in-button update-button" style="width: 75%;margin-right: 5px; margin-left: 5px;" id="${id}">Update</div>
            <div class="cancel-button" style="width: 25%;margin-right: 5px; margin-left: 5px;" onclick="edit_off()">Cancel</div>
        </div>
    </div>`
}

const activate_update_button = () => {
    return $('.update-button').off('click').click(function () {
        id = $(this).attr("id");
        contract_name = $("#overlay-edit").find('#contract-name').val();
        minimum_work_hour = $("#overlay-edit").find('#minimum-work-hour').val();
        maximum_work_hour = $("#overlay-edit").find('#maximum-work-hour').val();
        
        $.ajax({
            method: "PUT",
            url: `http://34.101.124.253/api/era/contract/v1/update/${id}`,
            xhrFields: {
                withCredentials: true
            },
            data: JSON.stringify({
                "name": contract_name,
                "minimumWorkHour": parseInt(minimum_work_hour),
                "maximumWorkHour": parseInt(maximum_work_hour)
            }),
            success: (data) => { 
                alert("Success!");
                window.location.href = "contract";
            },
            error: (data) => {
                alert("Error:" + data.message)
            },
            contentType: "application/json",
            dataType: 'json'
        });
    });
}

const activate_edit_button = () => {
    return $('div.edit-button').off('click').click(function () {
        id = $(this).attr("id");
        $("#overlay-edit").children(".overlay-container").html(edit_overlay_content(id));
        $("#overlay-edit").children(".overlay-container").hide();
        $("#overlay-edit").show();
        $("#overlay-edit").children(".overlay-container").slideToggle("slow");
        activate_update_button();
    });
}

$.ajax({
    type: "GET",
    url: `http://34.101.124.253/api/era/contract/v1/company/${localStorage.getItem("company_id")}`,
    contentType: "application/json",
    dataType: 'json',
    success: (data) => {
        row_content = "";
        for (const contract of data) {
            row_content += get_row_content(contract);
        };
        $("table").append(row_content);
        activate_delete_button();
        activate_edit_button();
    }
});

$('.sign-in-button').off('click').click(function () {
    contract_name = $("#overlay").find('#contract-name').val();
    minimum_work_hour = $("#overlay").find('#minimum-work-hour').val();
    maximum_work_hour = $("#overlay").find('#maximum-work-hour').val();
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/era/contract/v1/add",
        xhrFields: {
          withCredentials: true
        },
        data: JSON.stringify({
            "companyId": localStorage.getItem("company_id"),
            "name": contract_name,
            "minimumWorkHour": parseInt(minimum_work_hour),
            "maximumWorkHour": parseInt(maximum_work_hour)
        }),
        success: (data) => { 
          alert("Success!");
          window.location.href = "contract";
        },
        error: (data) => {
          alert("Error:" + data.message)
        },
        contentType: "application/json",
        dataType: 'json'
    });
  });