zeroPad = (num, places) => String(num).padStart(places, '0')

const get_option_contract_content = () => {
    option_contract_content = ""

    for(const [contract_id, contract] of Object.entries(contractById)){
        option_contract_content += `<option>${contract_id} - ${contract['name']}</option>`
    }

    if (option_contract_content.length === 0){
        option_contract_content += `<option>? - No Contract Defined</option>`
    }
    
    return option_contract_content
}

const get_option_room_content = () => {
    option_room_content = ""

    for(const [room_id, room] of Object.entries(roomById)){
        option_room_content += `<option>${room_id} - ${room['name']}</option>`
    }

    if (option_room_content.length === 0){
        option_room_content += `<option>? - No Room Defined</option>`
    }
    
    return option_room_content
}

get_row_content = (department) => {
    return `
    <tr>
        <td>${department.name}</td>
        <td>${roomById[department.roomId]["name"]}</td>
        <td>${contractById[department.contractId]["name"]}</td>
        <td>${department.minimumEmployeeOnsite} Employee</td>
        <td>
            <div class="action-flex">
                <div class="edit-button" id="${department.id}"></div>
                <div class="delete-button" id="${department.id}"></div>
            </div>
        </td>
    </tr>`
}

const activate_delete_button = () => {
    return $('div.delete-button').off('click').click(function () {
        id = $(this).attr("id");
        $.ajax({
            type: "DELETE",
            url: `http://34.101.124.253/api/era/department/v1/delete/${id}`,
            contentType: "application/json",
            dataType: 'text',
            success: (data) => {
                window.location.href = "department";
            }
        });
    });
}

create_off = () => {
    $("#overlay").hide();
}

edit_off = () => {
    $("#overlay-edit").hide();
}

create_overlay_content = () => {
    return `<div class="sign-in-box background-white">
        <div>
            <p>Department</p>
            <input type="text" id="department-name">
        </div>
        
        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div style="width: 100%; margin-right: 5px;">
                <p>Room</p>
                <div class="custom-select" id="room-create-select">
                    <select>
                        <option selected>Room</option>
                        ${get_option_room_content()}
                    </select>
                </div>
            </div>

            <div style="width: 100%; margin-left: 5px;">
                <p>Contract</p>
                <div class="custom-select" id="contract-create-select">
                    <select>
                        <option selected>Contract</option>
                        ${get_option_contract_content()}
                    </select>
                </div>
            </div>
        </div>

        <div style="width: auto; margin-right: auto;">
            <p>Minimum onsite</p>
            <input type="number" id="minimum-onsite" style="text-align: center;">
        </div>

        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div class="sign-in-button" style="width: 75%;margin-right: 5px; margin-left: 5px;" >Create</div>
            <div class="cancel-button" style="width: 25%;margin-right: 5px; margin-left: 5px;" onclick="create_off()">Cancel</div>
        </div>
    </div>`
}

const activate_create_submit_button = () => {
    return $('.sign-in-button').off('click').click(function () {
        department_name = $("#overlay").find('#department-name').val();
        room_selected_value = $("#room-create-select").find(".select-selected").text();
        contract_selected_value = $("#contract-create-select").find(".select-selected").text();
        minimum_onsite = $("#overlay").find('#minimum-onsite').val();
        room_id = room_selected_value.split(" - ")[0];
        contract_id = contract_selected_value.split(" - ")[0];
        return $.ajax({
            method: "POST",
            url: "http://34.101.124.253/api/era/department/v1/add",
            xhrFields: {
              withCredentials: true
            },
            data: JSON.stringify({
                "companyId": localStorage.getItem("company_id"),
                "name": department_name,
                "minimumEmployeeOnsite": parseInt(minimum_onsite),
                "roomId": parseInt(room_id),
                "contractId": parseInt(contract_id),
            }),
            success: (data) => { 
              alert("Success!");
              window.location.href = "department";
            },
            error: (data) => {
              alert("Error:" + data.message)
            },
            contentType: "application/json",
            dataType: 'json'
        });
      });
}

const activate_create_button = () => {
    return $('div.create-button').off('click').click(function () {
        id = $(this).attr("id");
        $("#overlay").children(".overlay-container").html(create_overlay_content());
        $("#overlay").children(".overlay-container").hide();
        $("#overlay").show();
        $("#overlay").children(".overlay-container").slideToggle("slow");
        activate_create_submit_button();
        initiate_custom_select();
    });
}

edit_overlay_content = (id) => {
    return `<div class="sign-in-box background-white">
        <div>
            <p>Department</p>
            <input type="text" id="department-name">
        </div>
        
        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div style="width: 100%; margin-right: 5px;">
                <p>Room</p>
                <div class="custom-select" id="room-update-select">
                    <select>
                        <option selected>Room</option>
                        ${get_option_room_content()}
                    </select>
                </div>
            </div>

            <div style="width: 100%; margin-left: 5px;">
                <p>Contract</p>
                <div class="custom-select" id="contract-update-select">
                    <select>
                        <option selected>Contract</option>
                        ${get_option_contract_content()}
                    </select>
                </div>
            </div>
        </div>

        <div style="width: auto; margin-right: auto;">
            <p>Minimum onsite</p>
            <input type="number" id="minimum-onsite" style="text-align: center;">
        </div>

        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div class="sign-in-button update-button" style="width: 75%;margin-right: 5px; margin-left: 5px;" id="${id}">Update</div>
            <div class="cancel-button" style="width: 25%;margin-right: 5px; margin-left: 5px;" onclick="edit_off()">Cancel</div>
        </div>
    </div>`
}

const activate_update_button = () => {
    return $('.update-button').off('click').click(function () {
        id = $(this).attr("id");
        department_name = $("#overlay-edit").find('#department-name').val();
        room_selected_value = $("#room-update-select").find(".select-selected").text();
        contract_selected_value = $("#contract-update-select").find(".select-selected").text();
        minimum_onsite = $("#overlay-edit").find('#minimum-onsite').val();
        room_id = room_selected_value.split(" - ")[0];
        contract_id = contract_selected_value.split(" - ")[0];
        
        $.ajax({
            method: "PUT",
            url: `http://34.101.124.253/api/era/department/v1/update/${id}`,
            xhrFields: {
                withCredentials: true
            },
            data: JSON.stringify({
                "name": department_name,
                "minimumEmployeeOnsite": parseInt(minimum_onsite),
                "roomId": parseInt(room_id),
                "contractId": parseInt(contract_id),
            }),
            success: (data) => { 
                alert("Success!");
                window.location.href = "department";
            },
            error: (data) => {
                alert("Error:" + data.message)
            },
            contentType: "application/json",
            dataType: 'json'
        });
    });
}

const activate_edit_button = () => {
    return $('div.edit-button').off('click').click(function () {
        id = $(this).attr("id");
        $("#overlay-edit").children(".overlay-container").html(edit_overlay_content(id));
        $("#overlay-edit").children(".overlay-container").hide();
        $("#overlay-edit").show();
        $("#overlay-edit").children(".overlay-container").slideToggle("slow");
        activate_update_button();
        initiate_custom_select();
    });
}

activate_create_button();

fetchDepartmentPromise.then( () => {
    $.ajax({
        type: "GET",
        url: `http://34.101.124.253/api/era/department/v1/all/${localStorage.getItem("company_id")}`,
        contentType: "application/json",
        dataType: 'json',
        success: (data) => {
            row_content = "";
            for (const department of data) {
                row_content += get_row_content(department);
            };
            $("table").append(row_content);
            activate_delete_button();
            activate_edit_button();
        }
    });
});