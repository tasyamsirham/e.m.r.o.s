get_row_content = (schedule_request) => {
    return `
    <tr>
        <td>${schedule_request.startDate}</td>
        <td>${schedule_request.endDate}</td>
        <td>${schedule_request.status}</td>
        <td>${schedule_request.minimumWorkHourWeight}</td>
        <td>${schedule_request.maximumWorkHourWeight}</td>
        <td>${schedule_request.minimumOnsiteWeight}</td>
        <td>${schedule_request.roomCapacityWeight}</td>
        <td>${schedule_request.preferenceWeight}</td>
        <td>${schedule_request.differentEmployeePerShiftWeight}</td>
        <td>${schedule_request.differentEmployeeTotalWeight}</td>
        <td>
            <div class="action-flex">
                <div class="delete-button" id="${schedule_request.id}"></div>
            </div>
        </td>
    </tr>`
}

const activate_delete_button = () => {
    return $('div.delete-button').off('click').click(function () {
        id = $(this).attr("id");
        $.ajax({
            type: "DELETE",
            url: `http://34.101.124.253/api/era/schedule-request/v1/delete/${id}`,
            contentType: "application/json",
            dataType: 'text',
            success: (data) => {
                window.location.href = "schedule_request";
            }
        });
    });
}

$.ajax({
    type: "GET",
    url: `http://34.101.124.253/api/era/schedule-request/v1/department/${localStorage.getItem("department_id")}`,
    contentType: "application/json",
    dataType: 'json',
    success: (data) => {
        row_content = "";
        for (const schedule_request of data) {
            row_content += get_row_content(schedule_request);
        };
        $("table").append(row_content);
        activate_delete_button();
    }
});

$('.sign-in-button').off('click').click(function () {
    start_date = $('#start-date').val();
    end_date = $('#end-date').val();
    miwh_weight = $('#miwh-weight').val();
    mawh_weight = $('#mawh-weight').val();
    mos_weight = $('#mos-weight').val();
    preference_weight = $('#preference-weight').val();
    rc_weight = $('#rc-weight').val();
    deps_weight = $('#deps-weight').val();
    det_weight = $('#det-weight').val();
    
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/schedule-sender/schedule",
        xhrFields: {
          withCredentials: true
        },
        data: JSON.stringify({
            "department_id": localStorage.getItem("department_id"),
            "start_date": start_date,
            "end_date": end_date,
            "minimum_work_hour_weight": miwh_weight,
            "maximum_work_hour_weight": mawh_weight,
            "minimum_onsite_weight": mos_weight,
            "room_capacity_weight": rc_weight,
            "preference_weight": preference_weight,
            "different_employee_per_shift_weight": deps_weight,
            "different_employee_total_weight": det_weight
        }),
        success: (data) => { 
          alert("Success!");
          window.location.href = "schedule_request";
        },
        error: (data) => {
          alert("Error:" + data.message)
        },
        contentType: "application/json",
        dataType: 'json'
    });
  });