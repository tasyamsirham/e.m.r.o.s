zeroPad = (num, places) => String(num).padStart(places, '0')

get_row_content = (shift) => {
    return `
    <tr>
        <td>${shift.name}</td>
        <td>${zeroPad(shift.startHour, 2)}:00</td>
        <td>${zeroPad(shift.endHour, 2)}:00</td>
        <td>
            <div class="action-flex">
                <div class="edit-button" id="${shift.id}"></div>
                <div class="delete-button" id="${shift.id}"></div>
            </div>
        </td>
    </tr>`
}

const activate_delete_button = () => {
    return $('div.delete-button').off('click').click(function () {
        id = $(this).attr("id");
        $.ajax({
            type: "DELETE",
            url: `http://34.101.124.253/api/era/shift/v1/delete/${id}`,
            contentType: "application/json",
            dataType: 'text',
            success: (data) => {
                window.location.href = "shift";
            }
        });
    });
}

edit_off = () => {
    $("#overlay-edit").hide();
}

edit_overlay_content = (id) => {
    return `<div class="sign-in-box background-white">
        <div>
            <p>Shift</p>
            <input type="text" id="shift-name">
        </div>
        
        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div style="width: 50%; margin-right: 5px;">
                <p>Start hour</p>
                <input type="number" id="start-hour" style="text-align: center;">
            </div>

            <div style="width: 50%; margin-left: 5px;">
                <p>End hour</p>
                <input type="number" id="end-hour" style="text-align: center;">
            </div>
        </div>

        <div style="display:flex; flex-direction: row; justify-content: space-between;">
            <div class="sign-in-button update-button" style="width: 75%;margin-right: 5px; margin-left: 5px;" id="${id}">Update</div>
            <div class="cancel-button" style="width: 25%;margin-right: 5px; margin-left: 5px;" onclick="edit_off()">Cancel</div>
        </div>
    </div>`
}

const activate_update_button = () => {
    return $('.update-button').off('click').click(function () {
        id = $(this).attr("id");
        shift_name = $("#overlay-edit").find('#shift-name').val();
        start_hour = $("#overlay-edit").find('#start-hour').val();
        end_hour = $("#overlay-edit").find('#end-hour').val();
        
        $.ajax({
            method: "PUT",
            url: `http://34.101.124.253/api/era/shift/v1/update/${id}`,
            xhrFields: {
                withCredentials: true
            },
            data: JSON.stringify({
                "name": shift_name,
                "startHour": parseInt(start_hour),
                "endHour": parseInt(end_hour)
            }),
            success: (data) => { 
                alert("Success!");
                window.location.href = "shift";
            },
            error: (data) => {
                alert("Error:" + data.message)
            },
            contentType: "application/json",
            dataType: 'json'
        });
    });
}

const activate_edit_button = () => {
    return $('div.edit-button').off('click').click(function () {
        id = $(this).attr("id");
        $("#overlay-edit").children(".overlay-container").html(edit_overlay_content(id));
        $("#overlay-edit").children(".overlay-container").hide();
        $("#overlay-edit").show();
        $("#overlay-edit").children(".overlay-container").slideToggle("slow");
        activate_update_button();
    });
}

$.ajax({
    type: "GET",
    url: `http://34.101.124.253/api/era/shift/v1/department/${localStorage.getItem("department_id")}`,
    contentType: "application/json",
    dataType: 'json',
    success: (data) => {
        row_content = "";
        for (const shift of data) {
            row_content += get_row_content(shift);
        };
        $("table").append(row_content);
        activate_delete_button();
        activate_edit_button();
    }
});

$('.sign-in-button').off('click').click(function () {
    shift_name = $("#overlay").find('#shift-name').val();
    start_hour = $("#overlay").find('#start-hour').val();
    end_hour = $("#overlay").find('#end-hour').val();
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/era/shift/v1/add",
        xhrFields: {
          withCredentials: true
        },
        data: JSON.stringify({
            "name": shift_name,
            "startHour": parseInt(start_hour),
            "endHour": parseInt(end_hour)
        }),
        error: (data) => {
          alert("Error:" + data.message)
        },
        contentType: "application/json",
        dataType: 'json'
    }).done((shift) => {
        $.ajax({
            method: "POST",
            url: "http://34.101.124.253/api/era/department-shift/v1/add",
            xhrFields: {
              withCredentials: true
            },
            data: JSON.stringify({
                "departmentId": localStorage.getItem("department_id"),
                "shiftId": shift.id
            }),
            success: (data) => { 
              alert("Success!");
              window.location.href = "shift";
            },
            error: (data) => {
              alert("Error:" + data.message)
            },
            contentType: "application/json",
            dataType: 'json'
        })
    });
  });