var roomById = {};

var contractById = {};

const fetchRoomData = () => {
  return $.ajax({
      method: "GET",
      url: `http://34.101.124.253/api/era/room/v1/all/${localStorage.getItem("company_id")}`,
      xhrFields: {
        withCredentials: true
      },
      dataType: 'json',
      success: (data) => {
        for (const room of data) {
            roomById[room.id] = room;
        }
      }
  });
}

const fetchContractData = () => {
  return $.ajax({
      method: "GET",
      url: `http://34.101.124.253/api/era/contract/v1/company/${localStorage.getItem("company_id")}`,
      xhrFields: {
        withCredentials: true
      },
      dataType: 'json',
      success: (data) => {
        for (const contract of data) {
            contractById[contract.id] = contract;
        }
      }
  });
}

const fetchDepartmentPromise = Promise.all([fetchRoomData(), fetchContractData()]);