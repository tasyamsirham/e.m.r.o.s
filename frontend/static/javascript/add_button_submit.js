const click_added_popup = (data) => {
  $(".popup").css("display", "none");
  if (data.availabilityStatus != undefined) {
    popup_div = $(`#${data.day}`).next().find(".availability");
  }else if (data.preferenceStatus != undefined) {
    popup_div = $(`#${data.day}`).next().find(".preference");
  }

  popup_div.click();
}

const remove_add_if_necessary = (data) => {
  if (!can_add(data.day)){
    add_div = $(`#${data.day}`).next().find(".add");
    add_div.remove();
  }
}

const initiate_add_button_submit = () => {
    $('div.submit-button').off('click').click(function () {
      day = $(this).parent().parent().prev().attr('id');
      selector_flex = $(this).prev();

      type_custom_select = selector_flex.prev();
      shift_custom_select = selector_flex.children(":first");
      status_custom_select = shift_custom_select.next();

      type_value = type_custom_select.children(".select-selected").text();
      shift_value = shift_custom_select.children(".select-selected").text();
      status_value = status_custom_select.children(".select-selected").text();
      
      shift_id = shift_value.split(" ")[0];
      status_id = scheduleStatusMapping.indexOf(status_value);
      
      if (type_value === "Availability"){
        create_availability_status(localStorage.getItem("employee_id"), localStorage.getItem("department_id"), 
                                shift_id, day, status_id)
                                .done(initiate_availability_popup)
                                .done(remove_add_if_necessary)
                                .done(activate_popup)
                                .done(activate_delete)
                                .done(click_added_popup);
      }else{
        create_preference_status(localStorage.getItem("employee_id"), localStorage.getItem("department_id"), 
                              shift_id, day, status_id)
                              .done(initiate_preference_popup)
                              .done(remove_add_if_necessary)
                              .done(activate_popup)
                              .done(activate_delete)
                              .done(click_added_popup);
      };
    });
};