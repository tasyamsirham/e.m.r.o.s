const get_option_shift_content = () => {
    option_shift_content = ""

    for(const [shift_id, shift] of Object.entries(shiftById)){
        option_shift_content += `<option>${shift_id} - ${shift['name']}</option>`
    }

    if (option_shift_content.length === 0){
        option_shift_content += `<option>? - No Shift Defined</option>`
    }
    
    return option_shift_content
}

const get_add_popup_content = () => {
    return `
        <div class="add schedule-status">+Add</div>
        <div class="background-white popup">
        <div class="custom-select">
            <select>
            <option selected>Select</option>
            <option value="0">Availability</option>
            <option value="1">Preference</option>
            </select>
        </div>
        <div class="select-flex-row">
            <div class="custom-select">
            <select>
                <option selected>Shift</option>
                ${get_option_shift_content()}
            </select>
            </div>
            <div class="custom-select">
            <select>
                <option selected>Status</option>
                <option value="0">Remote</option>
                <option value="1">On-site</option>
                <option value="1">Not-working</option>
            </select>
            </div>
        </div>
        <div class="submit-button">Submit</div>
        </div>
    `
}

const initiate_schedule_popup_if_necessary = (status, day) => {
    scheduleStatusName = scheduleStatusMapping[status];
    schedule_container = $(`#${day}`).next();

    if (scheduleStatusName === "On-site"){
        popup_div = schedule_container.find(".on-site");
    }else if (scheduleStatusName === "Remote"){
        popup_div = schedule_container.find(".remote");
    }

    if (popup_div.length === 0) {
        content = `
            <div class="${scheduleStatusName.toLowerCase()} schedule-status">${scheduleStatusName}</div>
            <div class="background-white popup"></div>
        `
        schedule_container.append(content);

        return true;
    }

    return false;
}

const get_schedule_popup_content = (status, shift, start_hour, end_hour) => {
    scheduleStatusName = scheduleStatusMapping[status];
    scheduleColor = scheduleColorMapping[status];
    return `
        <p class="shift-name">${shift}</p>
        <div>
            <p class="shift-hours">${start_hour} - ${end_hour}</p>
            <p class="${scheduleColor} shift-schedule">${scheduleStatusName}</p>
        </div>
    `
}

const initiate_pref_avai_popup_if_necessary = (type, day) => {
    schedule_container = $(`#${day}`).next();
    if (type === "availability"){
        popup_div = schedule_container.find(".availability");
    }else if (type === "preference"){
        popup_div = schedule_container.find(".preference");
    }

    if (popup_div.length === 0) {
        content = `
            <div class="${type} schedule-status">${type}</div>
            <div class="background-white popup"></div>
        `
        schedule_container.append(content);

        return true;
    }

    return false;
}

const get_pref_avai_popup_content = (text, status, id, start_hour, end_hour) => {
    scheduleStatusName = scheduleStatusMapping[status];
    scheduleColor = scheduleColorMapping[status];
    return `
        <p class="shift-name">${text}</p>
        <div class="shift-button-flex">
            <div>
                <p class="shift-hours">${start_hour} - ${end_hour}</p>
                <p class="${scheduleColor} shift-schedule">${scheduleStatusName}</p>
            </div>
            <div class="delete-button" id="${id}"></div>
        </div>
    `
}

const initiate_schedule_popup = (schedules) => {
    for (const schedule of schedules) {
        if (schedule.scheduleStatus === 2){
            continue;
        }
        try{
            shiftName = `${schedule.shiftId} - ${shiftById[schedule.shiftId]['name']}`;
            start_time = new Date(1970, 1, 1, shiftById[schedule.shiftId]['startHour']).format("hh:mm tt");
            end_time = new Date(1970, 1, 1, shiftById[schedule.shiftId]['endHour']).format("hh:mm tt");
        } catch (TypeError){
            shiftName = `1 - Pagi`;
            start_time = "NaN";
            end_time = "NaN";
        }
        first_time = initiate_schedule_popup_if_necessary(schedule.scheduleStatus, schedule.day);
        schedule_popup_content = get_schedule_popup_content(schedule.scheduleStatus, shiftName, start_time, end_time);

        schedule_popup_div = $(`#${schedule.day}`).next().find(`.${scheduleStatusMapping[schedule.scheduleStatus].toLowerCase()}`).next();
        if (!first_time) {schedule_popup_div.append("<hr/>")}
        schedule_popup_div.append(schedule_popup_content);
    };       
    activate_popup();
}

const initiate_preference_popup = (preference) => {
    preference_text = `Pref. - ${shiftById[preference.shiftId]['name']}`;

    try{
        start_time = new Date(1970, 1, 1, shiftById[preference.shiftId]['startHour']).format("hh:mm tt");
        end_time = new Date(1970, 1, 1, shiftById[preference.shiftId]['endHour']).format("hh:mm tt");
    } catch (TypeError){
        start_time = "NaN";
        end_time = "NaN";
    }
    
    first_time = initiate_pref_avai_popup_if_necessary("preference", preference.day);
    preference_popup_content = get_pref_avai_popup_content(preference_text, preference.preferenceStatus,  `preference-${preference.id}`, start_time, end_time);
    preference_popup_div = $(`#${preference.day}`).next().find(".preference").next();

    if (!first_time) {preference_popup_div.append("<hr/>")}
    preference_popup_div.append(preference_popup_content);
}

const initiate_bulk_preference_popup = (preferences) => {
    for (const preference of preferences) {
        initiate_preference_popup(preference);
    };
}

const initiate_availability_popup = (availability) => {
    availability_text = `Avai. - ${shiftById[availability.shiftId]['name']}`;

    try{
        start_time = new Date(1970, 1, 1, shiftById[availability.shiftId]['startHour']).format("hh:mm tt");
        end_time = new Date(1970, 1, 1, shiftById[availability.shiftId]['endHour']).format("hh:mm tt");
    } catch (TypeError){
        start_time = "NaN";
        end_time = "NaN";
    }
    
    first_time = initiate_pref_avai_popup_if_necessary("availability", availability.day);
    availability_popup_content = get_pref_avai_popup_content(availability_text, availability.availabilityStatus, `availability-${availability.id}`, start_time, end_time);
    availability_popup_div = $(`#${availability.day}`).next().find(".availability").next();
    
    if (!first_time) {availability_popup_div.append("<hr/>")}
    availability_popup_div.append(availability_popup_content);
}

const initiate_bulk_availability_popup = (availabilities) => {
    for (const availability of availabilities) {
        initiate_availability_popup(availability);
    };
}

const get_latest_scheduled_date = (schedule_arr) => {
    try{
        latest_schedule = schedule_arr[schedule_arr.length - 1];
        latest_scheduled_date = new Date(latest_schedule.day);
    } catch(TypeError){
        return new Date(date.getFullYear(), date.getMonth(), 0);
    }
    return latest_scheduled_date;
}

const can_add = (day) => {
    schedule_container = $(`#${day}`).next();
    preference_div = schedule_container.find(".preference");
    availability_div = schedule_container.find(".availability");
    preference_count = 0;
    availability_count = 0;

    if (preference_div.length != 0){
        popup_div = preference_div.next();
        preference_count = popup_div.children(".shift-button-flex").length;
    }if (availability_div.length != 0){
        popup_div = availability_div.next();
        availability_count = popup_div.children(".shift-button-flex").length;
        is_addable = is_addable && (Object.keys(shiftById).length > availability_count);
    }

    is_addable = Object.keys(shiftById).length > (availability_count + preference_count);

    return is_addable;
}

const get_add_button = (start_date, end_date) => {
    for (let i = new Date(start_date.getFullYear(), start_date.getMonth(), start_date.getDate()); i <= end_date; i.setDate(i.getDate() + 1)) {
        if (can_add(i.format("yyyy-MM-dd")) ) {
            add_popup_content = get_add_popup_content();
            schedule_container = $(`#${i.format("yyyy-MM-dd")}`).next();
            schedule_container.prepend(add_popup_content);
        }
    }
    initiate_custom_select();
    initiate_add_button_submit();
}

const initiate_unscheduled_popup_data = (schedule_arr) => {
    latest_scheduled_date = get_latest_scheduled_date(schedule_arr);
    unscheduled_date = new Date(latest_scheduled_date.getFullYear(), latest_scheduled_date.getMonth(), latest_scheduled_date.getDate() + 1);
    
    if (
        (unscheduled_date.getMonth() >= new Date().getMonth() &&
        unscheduled_date.getFullYear() == new Date().getFullYear())
        ||
        (unscheduled_date.getFullYear() > new Date().getFullYear())
    ) {
        Promise.all(
            [
                get_preference_status(localStorage.getItem("employee_id"), unscheduled_date, end_date).done(initiate_bulk_preference_popup),
                get_availability_status(localStorage.getItem("employee_id"), unscheduled_date, end_date).done(initiate_bulk_availability_popup)
            ]
        ).then(
            (resp) => {get_add_button(unscheduled_date, end_date)}
        ).then(activate_popup).then(activate_delete);
    };
}

const initiate_popup_data = () => {
    start_date = new Date(date.getFullYear(), date.getMonth(), 1);
    end_date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    get_schedule_status(localStorage.getItem("employee_id"), start_date, end_date).done(initiate_schedule_popup).done(initiate_unscheduled_popup_data);
}