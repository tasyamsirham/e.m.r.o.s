company_owner_sidebar_content = `
    <div class="main-link" onclick="sidebar_toggle('#dashboard-select')">
        <div class="dashboard"></div>
        <p>DASHBOARD</p>
        <div class="dropdown-button"></div>
    </div>

    <div class="selection-link" id="dashboard-select">
        <a href="department">
            <div class="dropdown-link" link="">
                <div class="dashboard"></div>
                <p>Department</p>
            </div>
        </a>

        <a href="shift">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Shift</p>
            </div>
        </a>

        <a href="room">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Room</p>
            </div>
        </a>
        
        <a href="contract">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Contract</p>
            </div>
        </a>
    </div>

    <div class="main-link" onclick="sidebar_toggle('#schedule-select')">
        <div class="dashboard"></div>
        <p>SCHEDULE</p>
        <div class="dropdown-button"></div>
    </div>
    
    <div class="selection-link" id="schedule-select">
        <a href="schedule">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Schedule</p>
            </div>
        </a>

        <a href="schedule_request">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Schedule Request</p>
            </div>
        </a>
    </div>

    <div class="main-link" onclick="sidebar_toggle('#employee-select')">
        <div class="dashboard"></div>
        <p>EMPLOYEE</p>
        <div class="dropdown-button"></div>
    </div>

    <div class="selection-link" id="employee-select">
        <a href="employee">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Employee List</p>
            </div>
        </a>
    </div>
    `

department_manager_sidebar_content = `
    <div class="main-link" onclick="sidebar_toggle('#dashboard-select')">
        <div class="dashboard"></div>
        <p>DASHBOARD</p>
        <div class="dropdown-button"></div>
    </div>

    <div class="selection-link" id="dashboard-select">
        <a href="shift">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Shift</p>
            </div>
        </a>
    </div>

    <div class="main-link" onclick="sidebar_toggle('#schedule-select')">
        <div class="dashboard"></div>
        <p>SCHEDULE</p>
        <div class="dropdown-button"></div>
    </div>

    <div class="selection-link" id="schedule-select">
        <a href="schedule">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Schedule</p>
            </div>
        </a>

        <a href="schedule_request">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Schedule Request</p>
            </div>
        </a>
    </div>

    <div class="main-link" onclick="sidebar_toggle('#employee-select')">
        <div class="dashboard"></div>
        <p>EMPLOYEE</p>
        <div class="dropdown-button"></div>
    </div>

    <div class="selection-link" id="employee-select">
        <a href="employee">
            <div class="dropdown-link">
                <div class="dashboard"></div>
                <p>Employee List</p>
            </div>
        </a>
    </div>
    `

employee_sidebar_content = `
<div class="main-link" onclick="sidebar_toggle('#schedule-select')">
    <div class="dashboard"></div>
    <p>SCHEDULE</p>
    <div class="dropdown-button"></div>
</div>

<div class="selection-link" id="schedule-select">
    <a href="schedule">
        <div class="dropdown-link">
            <div class="dashboard"></div>
            <p>Schedule</p>
        </div>
    </a>
</div>
`

role = localStorage.getItem("role");

if (!role){
    $(".sidebar").remove();
}else if (role.toLowerCase() === "employee"){
    sidebar_content = employee_sidebar_content;
    $(".sidebar-link").html(sidebar_content);
    $(".sidebar").css("display", "flex");
}else if (role === "department_manager") {
    sidebar_content = department_manager_sidebar_content;
    $(".sidebar-link").html(sidebar_content);
    $(".sidebar").css("display", "flex");
}else if (role === "company_owner") {
    sidebar_content = company_owner_sidebar_content;
    $(".sidebar-link").html(sidebar_content);
    $(".sidebar").css("display", "flex");
}


function sidebar_toggle(selection_link_id) {
    $(selection_link_id).slideToggle(400);
}