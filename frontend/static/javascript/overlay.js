function on() {
    $("#overlay").children(".overlay-container").hide();
    $("#overlay").show();
    $("#overlay").children(".overlay-container").slideToggle("slow");
}
  
function off() {
    $("#overlay").hide();
}