const auth_company = () => {
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/auth/service-auth",
        xhrFields: {
          withCredentials: true
        },
        success: (data) => { 
            localStorage.setItem("employee_id", data.employee_id);
            localStorage.setItem("department_id", data.department_id);
            localStorage.setItem("company_id", data.company_id);
            localStorage.setItem("role", data.role);
            if (localStorage.getItem("role").toLowerCase() != "company_owner"){
                window.location.href = "/";
            };
        },
        error: (data) => {
          alert("Error:" + data.message)
          window.location.href = "login";
        },
        contentType: "application/json",
        dataType: 'json'
    });
}

const auth_department = () => {
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/auth/service-auth",
        xhrFields: {
          withCredentials: true
        },
        success: (data) => { 
            localStorage.setItem("employee_id", data.employee_id);
            localStorage.setItem("department_id", data.department_id);
            localStorage.setItem("company_id", data.company_id);
            localStorage.setItem("role", data.role);
            if (localStorage.getItem("role").toLowerCase()  === "employee"){
                window.location.href = "/";
            };
        },
        error: (data) => {
          alert("Error:" + data.message)
          window.location.href = "login";
        },
        contentType: "application/json",
        dataType: 'json'
    });
}

const need_authenticated = () => {
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/auth/service-auth",
        xhrFields: {
          withCredentials: true
        },
        success: (data) => { 
            localStorage.setItem("employee_id", data.employee_id);
            localStorage.setItem("department_id", data.department_id);
            localStorage.setItem("company_id", data.company_id);
            localStorage.setItem("role", data.role);
        },
        error: (data) => {
          alert("Error:" + data.message)
          window.location.href = "login";
        },
        contentType: "application/json",
        dataType: 'json'
    });
}

const need_not_authenticated = () => {
    return $.ajax({
        method: "POST",
        url: "http://34.101.124.253/api/auth/service-auth",
        xhrFields: {
          withCredentials: true
        },
        success: (data) => { 
            localStorage.setItem("employee_id", data.employee_id);
            localStorage.setItem("department_id", data.department_id);
            localStorage.setItem("company_id", data.company_id);
            localStorage.setItem("role", data.role);
            window.location.href = "/";
        },
        contentType: "application/json",
        dataType: 'json'
    });
}