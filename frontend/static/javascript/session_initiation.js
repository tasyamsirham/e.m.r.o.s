const get_session_content = (day_class, start_hour, end_hour, status) => {
    scheduleStatusName = scheduleStatusMapping[status];
    scheduleColor = scheduleColorMapping[status];
    return `
        <div class="session" style="grid-column: ${day_class}; grid-row: _${zeroPad(start_hour, 2)}00-start / span _${zeroPad(end_hour, 2)}00-start;">
            <div class="background-${scheduleColor}">${scheduleStatusName}</div>
        </div>
    `
}

const initiate_session = (schedule) => {
    if (schedule.scheduleStatus === 2){
        return;
    }

    current_date = new Date(schedule.day);
    day_class = days_class[current_date.getDay()];
    
    try{
        start_hour = shiftById[schedule.shiftId]['startHour'];
        end_hour = shiftById[schedule.shiftId]['endHour'];
    } catch (TypeError){
        start_hour = 0;
        end_hour = 8;
    }
    
    session_content = get_session_content(day_class, start_hour, end_hour, schedule.scheduleStatus);

    $(".calendar").append(session_content);
}

const initiate_bulk_session = (schedules) => {
    for (const schedule of schedules) {
        initiate_session(schedule);
    };
}

const initiate_session_data = () => {
    sunday_date = new Date(date.getFullYear(), date.getMonth(), date.getDate()- (date.getDay() - 0));
    saturday_date = new Date(date.getFullYear(), date.getMonth(), date.getDate()- (date.getDay() - 6));
    get_schedule_status(localStorage.getItem("employee_id"), sunday_date, saturday_date).done(initiate_bulk_session);
}