const get_column_placeholder_content = (day_class, date) => {
    day = days[date.getDay()];

    if (
        date.getDate() === new Date().getDate() &&
        date.getMonth() === new Date().getMonth() &&
        date.getFullYear() === new Date().getFullYear()
    ){
        return `
        <div class="column-placeholder ${day_class}">
            <div class="date today">
                <p>${day}</p>
                <p>${date.getDate()}</p>
            </div>
        </div>
        `
    }else {
        return `
        <div class="column-placeholder ${day_class}">
            <div class="date">
                <p>${day}</p>
                <p>${date.getDate()}</p>
            </div>
        </div>
        `
    }
}

const get_row_placeholder_content = (start_hour, end_hour) => {
    row_placeholder_content = "";
    
    for (let x=start_hour; x < end_hour; x++){
        row_placeholder_content += `
            <div class='row-placeholder' style='grid-row: _${zeroPad(x, 2)}00'>${zeroPad(x, 2)}.00</div>
            <div class='row-placeholder transparent' style='grid-row: _${zeroPad(x, 2)}30'>${zeroPad(x, 2)}.30</div>
        `
    }

    return row_placeholder_content;
}

const get_session_row_placeholder_content = (start_hour, end_hour) => {
    session_row_placeholder_content = "";
    
    for (let x=start_hour; x < end_hour; x++){
        row_placeholder_content += `
            <div class='session-row-placeholder' style='grid-row: _${zeroPad(x, 2)}00-start / span _${zeroPad(x, 2)}30-end'></div>
        `
    }

    return session_row_placeholder_content;
}

const get_start_end_hour_of_shift = () => {
    min_start_hour = Infinity;
    max_end_hour = 0;

    for(const [shift_id, shift] of Object.entries(shiftById)){
        if (min_start_hour > shift['startHour']){
            min_start_hour = shift['startHour']
        };

        if (max_end_hour < shift['endHour']) {
            max_end_hour = shift['endHour']
        };
    }
    
    return [min_start_hour, max_end_hour];
}

const renderCalendar = () => {
    temp_date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    temp_date.setDate(temp_date.getDate() - (date.getDay() - 0));
    sunday_date = temp_date.getDate();
    sunday_month = temp_date.getMonth();
    temp_date.setDate(temp_date.getDate() - (date.getDay() - 6));
    saturday_date = temp_date.getDate();
    saturday_month = temp_date.getMonth();

    if (sunday_month === saturday_month){
        $(".month").children(":first").text(`${sunday_date} - ${saturday_date} ${months[saturday_month]}, ${date.getFullYear()}`);
    }else {
        $(".month").children(":first").text(`${sunday_date} ${months[sunday_month]} - ${saturday_date} ${months[saturday_month]}, ${date.getFullYear()}`);
    }

    $(".calendar").empty();

    let session_column_placeholder_content = `
        <div class="session-column-placeholder sunday"></div>
        <div class="session-column-placeholder monday"></div>
        <div class="session-column-placeholder tuesday"></div>
        <div class="session-column-placeholder wednesday"></div>
        <div class="session-column-placeholder thursday"></div>
        <div class="session-column-placeholder friday"></div>
        <div class="session-column-placeholder saturday"></div>
    `

    let min_start_hour, max_end_hour = 0

    $(".calendar").append(session_column_placeholder_content);

    fetchShiftPromise.done(
        (ref) => {
            [min_start_hour, max_end_hour] = get_start_end_hour_of_shift();
            row_placeholder_content = get_row_placeholder_content(min_start_hour, max_end_hour);
            session_row_placeholder_content = get_session_row_placeholder_content(min_start_hour, max_end_hour);
            $(".calendar").append(row_placeholder_content + session_row_placeholder_content);
            
        }
    );
    
    let column_placeholder_content = "";
    
    date_iter = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    for (let x=0; x < 7; x++){
        date_diff_between_current = date_iter.getDay() - x;
        date_iter.setDate(date_iter.getDate() - date_diff_between_current);
        column_placeholder_content += get_column_placeholder_content(days_class[x], date_iter);
    }

    $(".calendar").append(column_placeholder_content);
};

document.querySelector(".prev-button").addEventListener("click", () => {
    date.setDate(date.getDate() - 7);
    renderCalendar();
    initiate_session_data()
});
  
document.querySelector(".next-button").addEventListener("click", () => {
    date.setDate(date.getDate() + 7);
    renderCalendar();
    initiate_session_data()
});

renderCalendar();
fetchShiftPromise.done((res) => {initiate_session_data()});