const renderCalendar = () => {
  date.setDate(1);

  const lastDay = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDate();

  const prevLastDay = new Date(
    date.getFullYear(),
    date.getMonth(),
    0
  ).getDate();

  const firstDayIndex = date.getDay();

  const lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();

  const nextDays = 7 - lastDayIndex - 1;

  $(".month").children(":first").text(`${months[date.getMonth()]}, ${date.getFullYear()}`);

  let placeholders = `
    <div class="placeholder sunday"></div>
    <div class="placeholder monday"></div>
    <div class="placeholder tuesday"></div>
    <div class="placeholder wednesday"></div>
    <div class="placeholder thursday"></div>
    <div class="placeholder friday"></div>
    <div class="placeholder saturday"></div>
  `
  let calendar_content = placeholders;

  for (let x = firstDayIndex; x > 0; x--) {
    current_date = new Date(date.getFullYear(), date.getMonth() - 1, prevLastDay - x + 1);
    day_of_week = current_date.getDay();
    calendar_content += `<div class="${days_class[day_of_week]}"><div class="date irrelevant" id="${current_date.toLocaleDateString()}">${days[day_of_week]}, ${zeroPad(prevLastDay - x + 1, 2)}</div></div>`;
    day_of_week++;
  }

  for (let i = 1; i <= lastDay; i++) {
    current_date = new Date(date.getFullYear(), date.getMonth(), i);
    day_of_week = current_date.getDay();
    if (
      i === new Date().getDate() &&
      date.getMonth() === new Date().getMonth() &&
      date.getFullYear() === new Date().getFullYear()
    ) {
      calendar_content += `
        <div class="${days_class[day_of_week]}">
          <div class="date today" id="${current_date.format("yyyy-MM-dd")}">${days[day_of_week]}, ${zeroPad(i, 2)}</div>
          <div class="schedule-container"></div>
        </div>`;
    } else {
      if (day_of_week === 0 || day_of_week === 6) {
        calendar_content += `
          <div class="${days_class[day_of_week]}">
            <div class="date irrelevant" id="${current_date.format("yyyy-MM-dd")}">${days[day_of_week]}, ${zeroPad(i, 2)}</div>
            <div class="schedule-container"></div>
          </div>`;
      }else{
        calendar_content += `
          <div class="${days_class[day_of_week]}">
            <div class="date" id="${current_date.format("yyyy-MM-dd")}">${days[day_of_week]}, ${zeroPad(i, 2)}</div>
            <div class="schedule-container"></div>
          </div>`;
      }
    }
  }

  for (let j = 1; j <= nextDays; j++) {
    current_date = new Date(date.getFullYear(), date.getMonth() + 1, j);
    day_of_week = current_date.getDay();
    calendar_content += `
      <div class="${days_class[day_of_week]}">
        <div class="date irrelevant" id="${current_date.format("yyyy-MM-dd")}">${days[day_of_week]}, ${zeroPad(j, 2)}</div>
        <div class="schedule-container"></div>
      </div>`
  }

  calendar_content += placeholders;

  $(".calendar").html(calendar_content);
};

document.querySelector(".prev-button").addEventListener("click", () => {
  date.setMonth(date.getMonth() - 1);
  renderCalendar();
  initiate_popup_data();
});

document.querySelector(".next-button").addEventListener("click", () => {
  date.setMonth(date.getMonth() + 1);
  renderCalendar();
  initiate_popup_data();
});

renderCalendar();
fetchShiftPromise.done((res) => {initiate_popup_data()});