const remove_div_after_delete = (attr_id_value) => {
  shift_div = $(`#${attr_id_value}`).parent();
  shift_p = shift_div.prev();
  shift_hr = shift_div.next();
  popup_div = shift_div.parent();
  type_div = popup_div.prev();
  shift_div.remove();
  shift_p.remove();
  try{
    if (shift_hr.prop("tagName").toLowerCase() === "hr"){
      shift_hr.remove();
    }
  } catch (TypeError){
  }
  if (popup_div.children().length === 0){
    popup_div.remove();
    type_div.remove();
  }
}

const activate_delete = () => {
  return $('div.delete-button').off('click').click(function () {
      attr_id_value = $(this).attr("id");
      type = attr_id_value.split("-")[0];
      id = attr_id_value.split("-")[1];
      if (type === "preference") {
      delete_preference(id).done(() => {remove_div_after_delete(attr_id_value)});
      } else if (type === "availability") {
      delete_availability(id).done(() => {remove_div_after_delete(attr_id_value)});
      }
  });
};

const activate_popup = () => {
  return $('.schedule-status').off('click').click(function () {
    position = $(this).position();
    popup = $(this).next(".popup");
    if (popup.css("display") == "none") {
      $(".popup").css("display", "none");
      popup.slideToggle(300);
      popup.css('top', position.top + 17);
    } else {
      popup.slideToggle(300);
    }
  });
};