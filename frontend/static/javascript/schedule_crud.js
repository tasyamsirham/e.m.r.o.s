const get_schedule_status = (employee_id, start_date, end_date) => {
    return $.ajax({
        type: "GET",
        url: "http://34.101.124.253/api/era/employeeschedule/v1/all/employee",
        data: {
            "employeeId": employee_id,
            "day1": start_date.format("yyyy-MM-dd"),
            "day2": end_date.format("yyyy-MM-dd"),
        },
        contentType: "application/json",
        dataType: 'json'
    });
}