import jwt
import datetime
import MySQLdb
from cryptography.hazmat.primitives import serialization
from jwt.exceptions import ExpiredSignatureError, InvalidSignatureError
from exceptions import EmailPasswordNotRecognizedException
from config import PRIVATE_KEY, PUBLIC_KEY, DATABASE_USER, DATABASE_HOST, DATABASE_PASSWORD, DATABASE_PORT, DATABASE_NAME

serialized_private_key = serialization.load_ssh_private_key(PRIVATE_KEY.encode(), password=b'')
serialized_public_key = serialization.load_ssh_public_key(PUBLIC_KEY.encode())

def authenticate(email, password):
    mysql_conn = MySQLdb.connect(user=DATABASE_USER, host=DATABASE_HOST, password=DATABASE_PASSWORD, port=int(DATABASE_PORT), database=DATABASE_NAME)
    cursor = mysql_conn.cursor()
    select_query = f"""
    SELECT id, department_id, company_id, role FROM employee WHERE email='{email}' and password='{password}'
    """
    
    try:
        cursor.execute(select_query)
        res = cursor.fetchone()
        payload = {
            "employee_id": res[0],
            "department_id": res[1],
            "company_id": res[2],
            "role": res[3]
        }
        cursor.close()
    except TypeError:
        raise EmailPasswordNotRecognizedException("Email or password is not recognized")
    
    payload['exp'] = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(minutes=5)
        
    access_token = jwt.encode(
        payload=payload,
        key=serialized_private_key,
        algorithm='RS256'
    )
    
    payload['exp'] = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(days=1)
    
    refresh_token = jwt.encode(
        payload=payload,
        key=serialized_private_key,
        algorithm='RS256'
    )
    payload = jwt.decode(jwt=access_token, key=serialized_public_key, algorithms=['RS256', ])
    
    return payload, access_token, refresh_token

def authenticate_with_access_token(access_token):
    try:
        payload = jwt.decode(jwt=access_token, key=serialized_public_key, algorithms=['RS256', ])
    except ExpiredSignatureError as error:
        raise error
    except InvalidSignatureError as error:
        raise error
    
    return payload, access_token
        
def authenticate_with_refresh_token(refresh_token):
    try:
        payload = jwt.decode(jwt=refresh_token, key=serialized_public_key, algorithms=['RS256', ])
        payload['exp'] = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(minutes=5)
        
        new_access_token = jwt.encode(
            payload=payload,
            key=serialized_private_key,
            algorithm='RS256'
        )
        
        payload['exp'] = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(days=1)
        
        new_refresh_token = jwt.encode(
            payload=payload,
            key=serialized_private_key,
            algorithm='RS256'
        )
    except ExpiredSignatureError as error:
        raise error
    except InvalidSignatureError as error:
        raise error
    
    return payload, new_access_token, new_refresh_token