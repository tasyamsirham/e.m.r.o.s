from pydantic import BaseModel

class BaseResponse(BaseModel):
    success: bool

class AuthSuccessResponse(BaseResponse):
    employee_id: int
    department_id: int
    company_id: int
    role: str
    
class AuthFailResponse(BaseResponse):
    message: str
    
class LogoutSuccessResponse(BaseResponse):
    message: str