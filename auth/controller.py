from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse

from response_models import AuthSuccessResponse, AuthFailResponse, LogoutSuccessResponse
from request_models import AuthRequest

from exceptions import EmailPasswordNotRecognizedException
from jwt.exceptions import ExpiredSignatureError, InvalidSignatureError
from utils import authenticate, authenticate_with_access_token, authenticate_with_refresh_token

app = FastAPI()

@app.post("/service-auth", 
        response_model=AuthSuccessResponse, 
        responses={
            500: {"model": AuthFailResponse},
            401: {"model": AuthFailResponse},
        })
async def auth(request: Request):    
    try:
        
        try:
            access_token = request.cookies.get("access_token")[7:]
            refresh_token = request.cookies.get("refresh_token")[7:]
        except TypeError:
            access_token = None
            refresh_token = None
            
        try:
            payload, access_token = authenticate_with_access_token(access_token)
        except ExpiredSignatureError:
            payload, access_token, refresh_token = authenticate_with_refresh_token(refresh_token)
                      
    except ExpiredSignatureError:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Token Expired"
        })
    except InvalidSignatureError:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Token Invalid"
        })
    except Exception as e:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": f"Invalid, {str(e)}"
        })
    
    response = JSONResponse(status_code=200, content={
        "success": True,
        "employee_id": payload["employee_id"],
        "department_id": payload["department_id"],
        "company_id": payload["company_id"],
        "role": payload["role"]
    })
    
    response.set_cookie(key="access_token",value=f"Bearer {access_token}", httponly=True)  #set HttpOnly cookie in response
    response.set_cookie(key="refresh_token",value=f"Bearer {refresh_token}", httponly=True)  #set HttpOnly cookie in response
    
    return response

@app.post("/user-auth", 
        response_model=AuthSuccessResponse, 
        responses={
            500: {"model": AuthFailResponse},
            401: {"model": AuthFailResponse},
        })
async def auth(user_data: AuthRequest):    
    try:
        email, password = user_data.email, user_data.password
        payload, access_token, refresh_token = authenticate(email, password)
                      
    except EmailPasswordNotRecognizedException:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Email or password not recognized"
        })
    except Exception as e:
        return JSONResponse(status_code=500, content={
            "success": False,
            "message": f"Invalid, {str(e)}"
        })
    
    response = JSONResponse(status_code=200, content={
        "success": True,
        "employee_id": payload["employee_id"],
        "department_id": payload["department_id"],
        "company_id": payload["company_id"],
        "role": payload["role"]
    })
    
    response.set_cookie(key="access_token",value=f"Bearer {access_token}", httponly=True)  #set HttpOnly cookie in response
    response.set_cookie(key="refresh_token",value=f"Bearer {refresh_token}", httponly=True)  #set HttpOnly cookie in response
    
    return response

@app.post("/user-logout", 
        response_model=LogoutSuccessResponse, 
        responses={
            500: {"model": AuthFailResponse},
        })
async def auth(request: Request):    
    response = JSONResponse(status_code=200, content={
        "success": True,
        "message": "Purged token cookies. Logged out"
    })
    
    response.set_cookie(key="access_token",value="", httponly=True)  #set HttpOnly cookie in response
    response.set_cookie(key="refresh_token",value="", httponly=True)  #set HttpOnly cookie in response
    
    return response