from pydantic import BaseModel
from typing import Optional

class AuthRequest(BaseModel):
    email: Optional[str]
    password: Optional[str]