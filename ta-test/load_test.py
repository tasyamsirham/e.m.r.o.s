import datetime
from locust import FastHttpUser, task, between

class WebsiteUser(FastHttpUser):
    wait_time = between(30, 30)
    # @task(1)
    # def update_era(self):
    #     with self.client.put("/api/era/shift/v1/update/4", json={
    #         "name": "testUpdate",
    #         "startHour": 10,
    #         "endHour": 30,
    #     }, catch_response=True) as response:
    #         if response.status_code == 500:
    #             response.success()
                
    # @task(1)
    # def get_era(self):
    #     self.client.get("/api/era/health")            
    
    # @task(1)
    # def get_era(self):
    #     self.client.get("/api/era/employeeschedule/v1/all/employee?employeeId=2&day1=2022-07-01&day2=2022-07-31")
        
    # @task(1)
    # def user_auth(self):
    #     random_number = random.choice(range(1,40))
    #     with self.client.post("/api/auth/user-auth", json={"email": f"placeholder@{random_number}", "password": "placeholder_password"}, catch_response=True) as response:
    #         if response.status_code == 500:
    #             response.success()
        
    # @task(1)
    # def service_auth(self):
    #     self.client.post("/api/auth/service-auth")
    
    @task(1)
    def schedule(self):
        self.client.post("/api/schedule-sender/schedule", json={
            "department_id": 1,
            "start_date": self.day_iter.strftime("%Y-%m-%d"),
            "end_date": self.day_iter.strftime("%Y-%m-%d"),
            "minimum_work_hour_weight": 0.2,
            "maximum_work_hour_weight": 0.2,
            "minimum_onsite_weight": 0.1,
            "room_capacity_weight": 0.1,
            "preference_weight": 0.2,
            "different_employee_per_shift_weight": 0.1,
            "different_employee_total_weight": 0.1
        })
        self.day_iter += datetime.timedelta(days=1)
        
    def on_start(self):
        self.day_iter = datetime.datetime(2022,8,1)
        self.client.post("/api/auth/user-auth", json={"email": "placeholder@1", "password": "placeholder_password"})