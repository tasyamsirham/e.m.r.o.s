DROP DATABASE IF EXISTS gamer_db;
CREATE DATABASE IF NOT EXISTS gamer_db;
USE gamer_db;

#
# TABLE STRUCTURE FOR: company
#

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `company` (`id`, `name`) VALUES (1, 'Rippin, Kertzmann and Carter');
INSERT INTO `company` (`id`, `name`) VALUES (2, 'Kerluke, Robel and Koelpin');
INSERT INTO `company` (`id`, `name`) VALUES (3, 'Koelpin-Dare');
INSERT INTO `company` (`id`, `name`) VALUES (4, 'Quitzon-Mertz');
INSERT INTO `company` (`id`, `name`) VALUES (5, 'Stanton-Stoltenberg');

#
# TABLE STRUCTURE FOR: room
#

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (1, 1, 'tenetur', 14);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (2, 2, 'non', 13);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (3, 3, 'excepturi', 19);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (4, 4, 'tempore', 27);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (5, 5, 'saepe', 19);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (6, 1, 'ut', 19);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (7, 2, 'id', 16);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (8, 3, 'tempora', 20);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (9, 4, 'ducimus', 11);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (10, 5, 'eum', 26);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (11, 1, 'ducimus', 15);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (12, 2, 'et', 28);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (13, 3, 'asperiores', 14);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (14, 4, 'non', 16);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (15, 5, 'ut', 26);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (16, 1, 'eum', 15);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (17, 2, 'est', 28);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (18, 3, 'sed', 29);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (19, 4, 'optio', 27);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (20, 5, 'neque', 11);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (21, 1, 'iusto', 23);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (22, 2, 'esse', 24);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (23, 3, 'eum', 30);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (24, 4, 'qui', 13);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (25, 5, 'aliquam', 10);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (26, 1, 'qui', 14);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (27, 2, 'illo', 23);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (28, 3, 'voluptas', 29);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (29, 4, 'modi', 29);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (30, 5, 'rerum', 29);

#
# TABLE STRUCTURE FOR: department
#

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `minimum_employee_onsite` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `department_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `department_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (1, 'Quam qui.', 1, 1, 2);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (2, 'Mollitia nostrum.', 2, 2, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (3, 'Impedit velit.', 3, 3, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (4, 'Qui nisi expedita.', 4, 4, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (5, 'Quod optio magni.', 5, 5, 1);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (6, 'Consequatur unde repellendus.', 1, 6, 5);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (7, 'Ipsum autem.', 2, 7, 0);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (8, 'Ab et assumenda.', 3, 8, 2);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (9, 'Saepe nulla repellat.', 4, 9, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (10, 'Id quibusdam error.', 5, 10, 0);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (11, 'Aperiam fuga quibusdam.', 1, 11, 3);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (12, 'Magnam quod.', 2, 12, 3);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (13, 'Voluptatem quia.', 3, 13, 3);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (14, 'Qui consequatur ut.', 4, 14, 0);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (15, 'Distinctio repellat enim.', 5, 15, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (16, 'Excepturi quaerat.', 1, 16, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (17, 'Voluptatibus occaecati optio.', 2, 17, 3);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (18, 'Accusantium explicabo recusandae.', 3, 18, 2);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (19, 'Enim quos ullam.', 4, 19, 1);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (20, 'Temporibus quibusdam reprehenderit.', 5, 20, 1);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (21, 'Sunt maxime.', 1, 21, 0);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (22, 'Non voluptatum.', 2, 22, 3);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (23, 'Iure nesciunt quidem.', 3, 23, 3);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (24, 'Non ducimus esse.', 4, 24, 1);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (25, 'Minima quam.', 5, 25, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (26, 'Veritatis perspiciatis.', 1, 26, 5);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (27, 'Quia quasi voluptas.', 2, 27, 2);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (28, 'Ut explicabo.', 3, 28, 0);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (29, 'Occaecati quia labore.', 4, 29, 0);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (30, 'Corporis dolorum eos.', 5, 30, 4);

#
# TABLE STRUCTURE FOR: employee
#

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `contract_minimum_work_hours` int(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '8',
  `contract_minimum_work_hours_interval` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DAY',
  `contract_maximum_work_hours` int(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '12',
  `contract_maximum_work_hours_interval` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DAY',
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (1, 'Margaretta Toy', 18, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (2, 'Mr. Rudy Spencer Jr.', 18, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (3, 'Ashlynn Walter', 10, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (4, 'Lilly Grimes Sr.', 21, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (5, 'Ms. Libbie Rolfson', 4, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (6, 'Joey Kutch', 28, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (7, 'Reymundo Cormier PhD', 4, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (8, 'Dr. Nelda Terry Sr.', 23, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (9, 'June Mueller Jr.', 21, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (10, 'Katheryn Sawayn', 28, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (11, 'Noble Olson', 12, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (12, 'Eleonore White', 7, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (13, 'Prof. Karson Becker', 3, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (14, 'Mr. Darron Streich', 22, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (15, 'Evan Kuhic III', 7, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (16, 'Delta Hoeger V', 8, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (17, 'Dr. Noel Daugherty', 5, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (18, 'Mrs. Elnora Reinger MD', 30, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (19, 'Sherwood Schimmel', 2, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (20, 'Prof. Woodrow Becker', 22, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (21, 'Caroline Lindgren', 15, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (22, 'Ms. Felicity Graham', 13, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (23, 'Clement Lind', 2, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (24, 'Mrs. Pascale Gusikowski MD', 7, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (25, 'Jerel Feest', 9, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (26, 'Laney O\'Reilly', 16, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (27, 'Yessenia Champlin', 9, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (28, 'Jedidiah Moen', 27, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (29, 'Buck Grant', 19, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (30, 'Kareem Aufderhar', 30, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (31, 'Yazmin Klocko', 14, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (32, 'Mina Schmidt', 6, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (33, 'Rafael Gibson', 17, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (34, 'Aisha Kuvalis', 23, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (35, 'Mr. Taylor Bogisich', 26, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (36, 'Sydnee Gottlieb', 21, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (37, 'Theo Kautzer V', 21, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (38, 'Prof. Christop Gorczany', 30, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (39, 'Dr. Lukas Hauck', 13, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (40, 'Shaylee Jacobi', 11, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (41, 'Mozell Crona III', 27, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (42, 'Rita Cruickshank', 25, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (43, 'Winnifred Veum', 17, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (44, 'Mr. Regan Schroeder', 29, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (45, 'Delmer Hettinger', 16, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (46, 'Christa Emmerich PhD', 24, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (47, 'Waldo Wehner MD', 7, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (48, 'Nigel Will', 20, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (49, 'Alejandra Hyatt', 23, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (50, 'Mr. Jalen Yundt DVM', 8, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (51, 'Maryjane Green', 11, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (52, 'Mr. Jonas Hagenes', 8, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (53, 'Kole Aufderhar', 20, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (54, 'Casimir Mills', 13, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (55, 'Dr. Adolphus Koepp V', 14, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (56, 'Jimmie Ruecker', 29, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (57, 'Maggie Funk', 29, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (58, 'Pattie Kshlerin', 23, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (59, 'Brad Greenfelder I', 25, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (60, 'Trystan Yundt III', 17, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (61, 'Bethel Emmerich', 22, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (62, 'Maryse White', 9, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (63, 'Damon O\'Connell', 22, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (64, 'Mr. Jamie Rogahn MD', 8, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (65, 'Mr. Weldon Hoeger Sr.', 1, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (66, 'Dr. Carley Willms PhD', 18, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (67, 'Kassandra Hand V', 28, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (68, 'Nestor Kovacek', 22, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (69, 'Carmel Green', 18, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (70, 'Christ Kuhic', 11, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (71, 'Reynold Gaylord', 2, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (72, 'Dr. Arielle Von', 15, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (73, 'Mr. Jonatan Will', 5, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (74, 'Marlon Cruickshank II', 19, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (75, 'Prof. Aurore Prosacco', 13, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (76, 'Glennie Hane Sr.', 21, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (77, 'Juston Waters', 12, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (78, 'Destini Blick', 19, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (79, 'Nestor Rowe DVM', 10, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (80, 'Herta Franecki', 4, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (81, 'Felix Rolfson MD', 27, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (82, 'Herman Anderson', 21, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (83, 'Dr. Clovis Labadie PhD', 11, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (84, 'Prof. Ashton Wisozk Sr.', 16, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (85, 'Dr. Virginia Bauch', 4, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (86, 'Carolyne Ondricka', 25, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (87, 'Dr. Kaden Casper', 14, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (88, 'Prof. Adah Kub V', 2, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (89, 'Reanna Feest', 17, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (90, 'Augustus Nienow', 9, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (91, 'Tom Altenwerth', 19, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (92, 'Guy Gerhold', 8, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (93, 'Prof. Keyon Kris', 17, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (94, 'Sim Collins IV', 11, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (95, 'Keshaun Bartell', 16, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (96, 'Abigayle Klein IV', 18, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (97, 'Ms. Tara Kemmer', 28, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (98, 'Ms. Lexi Emmerich', 14, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (99, 'Orie Schmeler', 9, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (100, 'Lelia Ward III', 16, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (101, 'Sherman Dietrich', 24, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (102, 'Lewis Boyle', 11, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (103, 'Marianne West', 30, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (104, 'Aidan Tillman', 29, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (105, 'Alfredo Paucek', 29, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (106, 'Mr. Ladarius Maggio Jr.', 13, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (107, 'Alejandrin Strosin', 19, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (108, 'Mac Padberg', 10, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (109, 'Austin O\'Conner', 2, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (110, 'Cathy Waelchi', 29, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (111, 'Domenick Kovacek', 13, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (112, 'Dr. Unique Murray', 28, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (113, 'Pietro Grimes PhD', 20, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (114, 'Brycen Grimes', 24, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (115, 'Dr. Daisha Pouros DVM', 13, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (116, 'Desiree Schowalter V', 24, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (117, 'Devyn Murphy', 18, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (118, 'Graham Quitzon', 27, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (119, 'Ms. Amara Fay', 25, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (120, 'Clifton Braun', 4, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (121, 'Grant Quigley', 6, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (122, 'Willard Rau', 14, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (123, 'Marietta Stiedemann', 12, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (124, 'Zion Orn', 22, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (125, 'Beau Vandervort', 24, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (126, 'Stewart Dickinson', 27, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (127, 'Prof. Joaquin Yundt V', 10, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (128, 'Kole Kovacek', 22, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (129, 'Shaniya Connelly', 10, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (130, 'Bernita Denesik', 18, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (131, 'Dr. Seamus Hoeger', 7, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (132, 'Javon Mertz', 4, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (133, 'Willow Kuhn', 29, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (134, 'Malinda Johns', 6, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (135, 'Mr. Aidan Barton', 2, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (136, 'Prof. Genesis Runte', 27, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (137, 'Mrs. Kenya Lubowitz V', 18, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (138, 'Carole Becker', 21, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (139, 'Mrs. Melyssa Greenfelder DVM', 6, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (140, 'Jake Kessler', 20, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (141, 'Mr. Branson Jacobi PhD', 19, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (142, 'Dora Mante III', 19, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (143, 'Meagan O\'Kon', 17, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (144, 'Ali Moen', 9, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (145, 'Meghan Herzog', 12, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (146, 'Cullen Vandervort', 30, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (147, 'Edgardo Lehner', 2, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (148, 'Lacey Weber', 30, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (149, 'Miracle Glover', 26, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (150, 'Justen Raynor DDS', 27, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (151, 'Prof. Halle Kshlerin PhD', 3, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (152, 'Hunter Braun I', 1, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (153, 'Shany Effertz Jr.', 10, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (154, 'Marcus Bednar', 14, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (155, 'Kurtis Gulgowski Jr.', 23, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (156, 'Mr. Isadore Kirlin', 4, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (157, 'Joelle Gibson', 11, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (158, 'Garnett Hoppe', 2, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (159, 'Arne Miller', 25, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (160, 'Mr. Titus Rolfson', 20, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (161, 'Mavis Douglas', 20, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (162, 'Joany Nienow', 1, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (163, 'Leland Langworth DDS', 23, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (164, 'Tristian Jaskolski IV', 18, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (165, 'Prof. Kenyatta Hodkiewicz DDS', 7, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (166, 'Scotty Prosacco', 25, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (167, 'Toney Johnston', 14, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (168, 'Mr. Mike Baumbach MD', 25, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (169, 'Karelle Ebert', 15, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (170, 'Keyshawn Lowe', 20, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (171, 'Alene Beer', 14, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (172, 'Shawna Dickens', 4, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (173, 'Jack Carroll', 9, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (174, 'Eric Tillman', 30, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (175, 'Jameson Nader', 12, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (176, 'Stephania Ebert', 21, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (177, 'Mrs. Aubree Boehm', 29, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (178, 'Danyka Fisher', 14, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (179, 'Sven Kling', 20, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (180, 'Gay Mohr', 25, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (181, 'Salvador Breitenberg', 11, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (182, 'Autumn Hickle', 23, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (183, 'Giovanna Lindgren', 26, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (184, 'Zechariah Gleichner', 20, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (185, 'Elmo Miller DVM', 7, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (186, 'Prof. Ken Hilpert III', 18, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (187, 'Everardo Hessel', 23, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (188, 'Mrs. Demetris Marks I', 17, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (189, 'Imelda Raynor', 20, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (190, 'Glen Hodkiewicz', 18, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (191, 'Prof. Eulah Hilll', 6, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (192, 'Margaret Lehner I', 9, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (193, 'Gavin Kemmer', 18, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (194, 'Rhea Schamberger', 29, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (195, 'Haley Cremin', 27, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (196, 'Albert Grady', 25, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (197, 'Taryn Lesch', 23, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (198, 'Ariel Runte MD', 10, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (199, 'Elisabeth Renner', 19, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (200, 'Alyce Becker', 7, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (201, 'Darrion Schiller', 30, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (202, 'Katherine Stroman Jr.', 3, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (203, 'Mrs. Madeline Rohan', 10, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (204, 'Tiana Reinger', 8, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (205, 'Karen Cartwright', 2, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (206, 'Rowena Kessler', 22, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (207, 'Vivianne Bosco IV', 28, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (208, 'Jermain Jakubowski', 1, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (209, 'Jeremie Osinski IV', 5, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (210, 'Miller Zieme IV', 17, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (211, 'Miss Mable Torp', 26, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (212, 'Nola Pagac DDS', 15, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (213, 'Keagan Parker', 9, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (214, 'Leonie Swift', 21, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (215, 'Mrs. Vena Johns', 5, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (216, 'Ludie Cruickshank', 15, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (217, 'Lottie Terry', 9, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (218, 'Mary Reynolds', 28, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (219, 'Dee Price', 2, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (220, 'Esta Jast III', 28, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (221, 'Paula Fisher', 15, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (222, 'Sigrid Beier', 7, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (223, 'Marcos McDermott', 7, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (224, 'Eldred Hauck', 3, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (225, 'Verla Fadel III', 6, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (226, 'Dr. Clifford Leuschke DDS', 4, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (227, 'Marlee Deckow', 27, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (228, 'Ozella Welch PhD', 28, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (229, 'Dr. Kory Stanton Jr.', 13, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (230, 'Breanna Gorczany', 15, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (231, 'Ben Mertz Jr.', 5, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (232, 'Golden Senger', 13, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (233, 'Prof. Amir Lindgren', 17, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (234, 'Mr. Lionel Stiedemann', 15, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (235, 'Zena Mann', 20, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (236, 'Prof. Edwina Murray', 19, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (237, 'Dahlia Osinski', 6, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (238, 'Davon Walker', 18, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (239, 'Jasmin Langworth', 20, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (240, 'Felicita Roob Sr.', 11, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (241, 'Trystan Kuphal', 5, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (242, 'Breanna Quigley', 15, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (243, 'Madeline Konopelski', 26, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (244, 'Lyda Kohler Jr.', 13, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (245, 'Mr. Dangelo Gleason', 6, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (246, 'Johnpaul Walter', 30, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (247, 'Lafayette Lemke Jr.', 28, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (248, 'Mckenzie Berge', 14, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (249, 'Christa Lesch', 28, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (250, 'Prof. Jo Predovic', 29, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (251, 'Seth Rutherford', 12, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (252, 'Mrs. Sierra Kunze Sr.', 13, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (253, 'Dr. Thaddeus Wilkinson PhD', 6, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (254, 'Dr. Stanton Champlin IV', 19, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (255, 'Royal Bode', 15, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (256, 'Katrine Nolan II', 12, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (257, 'Kenya Nolan', 22, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (258, 'Ms. Vicky Larkin', 11, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (259, 'Devon Heaney', 9, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (260, 'Clarissa Grimes', 5, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (261, 'Delbert Halvorson MD', 26, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (262, 'Lelah Osinski', 14, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (263, 'Mr. Fritz Wiegand DVM', 17, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (264, 'Mr. Aiden Steuber V', 13, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (265, 'Drake Kub', 28, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (266, 'Houston Yost', 7, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (267, 'Willa Herman', 2, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (268, 'Kristina Gislason', 3, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (269, 'Lacy Kohler', 25, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (270, 'Audra Fahey', 21, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (271, 'Ms. Verna Denesik', 14, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (272, 'Mariano Ratke', 29, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (273, 'Cali Smitham', 6, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (274, 'Kameron Jenkins', 9, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (275, 'Heath Crona', 12, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (276, 'Ms. Dandre Schoen V', 11, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (277, 'Mr. Larue Rogahn', 9, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (278, 'Joanie Auer', 10, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (279, 'Miss Fae Hartmann PhD', 25, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (280, 'Mr. Felton Muller', 6, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (281, 'Tina Greenfelder', 9, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (282, 'Verdie Farrell', 7, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (283, 'Ahmed Kling', 18, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (284, 'Dr. Eldon Hintz IV', 14, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (285, 'Mr. Wyatt Schiller', 26, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (286, 'Kallie Purdy', 3, '7', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (287, 'Henry Zieme', 25, '8', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (288, 'Dr. Jackeline Ondricka DDS', 17, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (289, 'Darrick Kozey', 14, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (290, 'Miss Sadye Schumm', 4, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (291, 'Alvina Lebsack', 22, '8', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (292, 'Deron Schaefer', 10, '6', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (293, 'Diamond Strosin', 18, '8', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (294, 'Addie Harber', 9, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (295, 'Dashawn Lind', 22, '6', '12', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (296, 'Irwin Orn', 15, '7', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (297, 'Carmella Kovacek', 16, '7', '11', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (298, 'Prof. Jeramy Walker PhD', 24, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (299, 'Dr. Rosario Goldner III', 18, '6', '10', 'WEEK', 'DAY');
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`, `contract_minimum_work_hours_interval`,  `contract_maximum_work_hours_interval`) VALUES (300, 'Adam Paucek II', 10, '8', '11', 'WEEK', 'DAY');


#
# TABLE STRUCTURE FOR: shift
#

DROP TABLE IF EXISTS `shift`;

CREATE TABLE `shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_hour` int(11) NOT NULL,
  `end_hour` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `shift_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (1, 1, 'Magni non.', 8, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (2, 2, 'Dolor ipsum asperiores.', 9, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (3, 3, 'Quis qui.', 8, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (4, 4, 'Ad ad et.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (5, 5, 'Accusamus autem dolor.', 9, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (6, 6, 'Commodi dolores.', 9, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (7, 7, 'Doloremque in.', 9, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (8, 8, 'Placeat fugit voluptate.', 9, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (9, 9, 'Laborum vitae.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (10, 10, 'Est sint esse.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (11, 11, 'Et rerum.', 8, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (12, 12, 'Delectus quia ducimus.', 8, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (13, 13, 'Animi ex fuga.', 9, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (14, 14, 'Voluptas voluptates.', 9, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (15, 15, 'Explicabo autem alias.', 8, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (16, 16, 'Quibusdam et mollitia.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (17, 17, 'Distinctio eos et.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (18, 18, 'Officiis consequatur magnam.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (19, 19, 'Voluptas placeat nemo.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (20, 20, 'Magnam quidem.', 9, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (21, 21, 'Et non tempore.', 9, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (22, 22, 'Ipsa mollitia.', 9, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (23, 23, 'Pariatur iste debitis.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (24, 24, 'Natus deserunt.', 8, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (25, 25, 'Non ipsam.', 9, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (26, 26, 'Et suscipit.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (27, 27, 'Consequatur non.', 9, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (28, 28, 'Sit voluptatibus facilis.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (29, 29, 'Aliquam ex impedit.', 8, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (30, 30, 'Dolorem culpa et.', 8, 17);

#
# TABLE STRUCTURE FOR: availability
#

DROP TABLE IF EXISTS `availability`;

CREATE TABLE `availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `availability_status` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `availability_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `availability_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `availability` (`id`, `employee_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (1, 3, 10, '2022-02-20', 1, 'Dont need to know it');

#
# TABLE STRUCTURE FOR: employee_schedule
#

DROP TABLE IF EXISTS `employee_schedule`;

CREATE TABLE `employee_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `schedule_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `employee_schedule_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `employee_schedule_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `employee_schedule` (`id`, `employee_id`, `shift_id`, `day`, `schedule_status`) VALUES (1, 3, 10, '2022-02-20', 0);

#
# TABLE STRUCTURE FOR: preference
#

DROP TABLE IF EXISTS `preference`;

CREATE TABLE `preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `preference_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `preference_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `preference_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `preference` (`id`, `employee_id`, `shift_id`, `day`, `preference_status`) VALUES (1, 85, 4, '2022-02-20', 1);
