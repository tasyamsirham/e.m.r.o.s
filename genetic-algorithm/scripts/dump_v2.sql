DROP DATABASE IF EXISTS gamer_db;
CREATE DATABASE IF NOT EXISTS gamer_db;
USE gamer_db;

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `company` (`id`, `name`) VALUES (1, 'Qomunis Inc.');
INSERT INTO `company` (`id`, `name`) VALUES (2, 'Wild Rose Group');


DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (1, 1, 'Room-Mar', 20);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (2, 2, 'Room-En', 8);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (3, 1, 'Room-HR', 10);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (4, 1, 'Room-Fin', 12);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (5, 1, 'Room-RD', 12);

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `minimum_employee_onsite` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `department_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `department_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (1, 'Marketing', 1, 1, 10);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (2, 'Engineering', 1, 2, 4);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (3, 'Human Resources', 1, 3, 5);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (4, 'Finance', 1, 4, 6);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `minimum_employee_onsite`) VALUES (5, 'Research & Development', 1, 5, 6);


DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `contract_minimum_work_hours` int(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '8',
  `contract_minimum_work_hours_interval` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DAY',
  `contract_maximum_work_hours` int(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '12',
  `contract_maximum_work_hours_interval` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DAY',
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (1,"Francis Webb",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (2,"Dahlia Rowland",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (3,"Brynne Dillon",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (4,"Cain Boyle",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (5,"Julie Nolan",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (6,"Samuel Mcclure",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (7,"Michelle Pitts",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (8,"Bert Macias",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (9,"Barrett Odom",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (10,"Dalton Willis",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (11,"Thane Dejesus",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (12,"Rooney Wood",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (13,"Anika Maxwell",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (14,"Cassidy Morton",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (15,"Jane Carrillo",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (16,"Yuri David",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (17,"Amity Vasquez",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (18,"Vincent Mcintyre",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (19,"Harper Reid",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (20,"Ronan Sellers",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (21,"Merrill Velez",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (22,"Belle Spencer",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (23,"Howard Snow",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (24,"Demetria Kramer",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (25,"Molly Barlow",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (26,"Kyle Vang",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (27,"Clinton Clemons",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (28,"Kadeem Randolph",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (29,"Ifeoma Petty",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (30,"Calvin Huber",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (31,"Velma Benson",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (32,"Damian Montgomery",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (33,"Quamar Garrett",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (34,"Todd Decker",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (35,"Danielle Collier",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (36,"Nero Cardenas",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (37,"Nadine Porter",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (38,"Tamara Bentley",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (39,"Ora Duke",1,40,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (40,"Pearl Ellison",1,40,12);

INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (41,"Murphy",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (42,"Amal",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (43,"Gil",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (44,"Darius",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (6,"Griffin",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (46,"Calvin",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (47,"Elliott",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (48,"Kasimir",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (49,"Eagan",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (50,"Gray",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (51,"Craig",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (52,"Alan",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (53,"Tyler",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (54,"Joel",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (55,"Merrill",2,6,15);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (56,"Solomon",2,6,15);

INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (57,"Amelia",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (58,"Desiree",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (59,"Ima",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (60,"Tatum",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (61,"Jaime",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (62,"Emily",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (63,"Jael",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (64,"Danielle",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (65,"Latifah",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (66,"Stacey",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (67,"Sylvia",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (68,"Adria",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (69,"Mona",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (70,"Whitney",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (71,"Maryam",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (72,"Marcia",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (73,"Quail",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (74,"Kylee",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (75,"Kevyn",3,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (76,"Kim",3,8,12);

INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (77,"Hughes",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (78,"Alexander",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (79,"Hahn",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (80,"Carney",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (81,"Maldonado",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (82,"Church",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (83,"Moran",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (84,"Vincent",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (85,"Cherry",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (86,"Jordan",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (87,"Stark",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (88,"Hogan",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (89,"Bishop",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (90,"Pugh",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (91,"Miranda",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (92,"Spencer",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (93,"Lambert",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (94,"Haney",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (95,"Mcmahon",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (96,"Dunn",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (97,"Santiago",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (98,"Crawford",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (99,"Wiley",4,8,12);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (100,"Cohen",4,8,12);

INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (101,"Iliana E. Dotson",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (102,"Nathan K. Grant",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (103,"Dillon W. Cervantes",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (104,"Tobias P. Bray",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (105,"Marsden Z. Ellison",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (106,"Julie L. Travis",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (107,"MacKenzie F. Lynn",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (108,"Basil H. Payne",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (109,"Emily T. Perry",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (110,"Dorian R. Rhodes",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (111,"Astra O. Ewing",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (112,"Madonna I. Mckay",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (113,"Graham Y. Middleton",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (114,"Fletcher B. Mccarty",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (115,"Roary P. Whitaker",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (116,"Tatiana Q. Hill",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (117,"Driscoll T. Murphy",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (118,"Leila B. Hammond",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (119,"Phyllis J. Booth",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (120,"Jesse P. Garza",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (121,"Victor H. Goff",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (122,"Kai Y. Ferguson",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (123,"Kirk I. Simmons",5,50,18);
INSERT INTO `employee` (`id`, `name`, `department_id`, `contract_minimum_work_hours`, `contract_maximum_work_hours`) VALUES (124,"Kermit J. Tate",5,50,18);




DROP TABLE IF EXISTS `shift`;

CREATE TABLE `shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_hour` int(11) NOT NULL,
  `end_hour` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `shift_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (1, 1, 'Pagi', 5, 11);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (2, 1, 'Sore', 12, 18);

INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (3, 2, 'Shift 1', 0, 3);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (4, 2, 'Shift 2', 3, 6);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (5, 2, 'Shift 3', 6, 9);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (6, 2, 'Shift 4', 9, 12);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (7, 2, 'Shift 5', 12, 15);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (8, 2, 'Shift 6', 15, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (9, 2, 'Shift 7', 18, 21);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (10, 2, 'Shift 8', 21, 24);

INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (11, 3, 'Pagi', 8, 12);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (12, 3, 'Pagi', 13, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (13, 3, 'Sore', 17, 21);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (14, 3, 'Sore', 21, 24);

INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (15, 4, 'Pagi', 8, 12);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (16, 4, 'Pagi', 13, 17);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (17, 4, 'Sore', 17, 21);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (18, 4, 'Sore', 21, 24);

INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (19, 5, 'Pagi', 5, 11);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (20, 5, 'Sore', 12, 18);
INSERT INTO `shift` (`id`, `department_id`, `name`, `start_hour`, `end_hour`) VALUES (21, 5, 'Malam', 18, 24);


DROP TABLE IF EXISTS `availability`;

CREATE TABLE `availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `availability_status` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `department_id` (`department_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `availability_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `availability_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE,
  CONSTRAINT `availability_ibfk_3` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (1,1,1,1,"2022-03-16",0,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (2,5,1,2,"2022-03-17",0,"depressed");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (3,9,1,1,"2022-03-17",1,"lost the will to live");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (4,13,1,2,"2022-03-19",1,"game night lmao");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (5,17,1,2,"2022-03-08",2,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (6,21,1,2,"2022-03-19",2,"depressed");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (7,25,1,1,"2022-03-30",0,"lost the will to live");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (8,29,1,2,"2022-03-09",0,"game night lmao");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (9,33,1,2,"2022-03-19",1,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (10,37,1,2,"2022-03-17",1,"depressed");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (11,41,2,3,"2022-03-12",2,"lost the will to live");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (12,45,2,4,"2022-03-06",2,"game night lmao");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (13,49,2,5,"2022-03-27",0,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (14,53,2,5,"2022-03-08",0,"depressed");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (15,57,3,6,"2022-03-26",1,"lost the will to live");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (16,61,3,7,"2022-03-24",1,"game night lmao");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (17,65,3,6,"2022-03-09",2,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (18,69,3,7,"2022-03-28",2,"depressed");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (19,73,3,7,"2022-03-26",0,"lost the will to live");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (20,77,4,8,"2022-03-05",0,"game night lmao");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (21,81,4,9,"2022-03-02",1,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (22,85,4,9,"2022-03-13",1,"depressed");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (23,89,4,8,"2022-03-28",2,"lost the will to live");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (24,93,4,9,"2022-03-24",2,"game night lmao");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (25,97,4,8,"2022-03-21",0,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (26,105,5,10,"2022-03-13",1,"depressed");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (27,106,5,11,"2022-03-28",2,"lost the will to live");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (28,115,5,12,"2022-03-24",2,"game night lmao");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (29,120,5,10,"2022-03-21",0,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (30,121,5,10,"2022-03-10",0,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (31,121,5,11,"2022-03-10",0,"too tired");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (32,121,5,12,"2022-03-10",0,"too tired");


DROP TABLE IF EXISTS `employee_schedule`;

CREATE TABLE `employee_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `schedule_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `employee_schedule_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `employee_schedule_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `employee_schedule` (`id`, `employee_id`, `shift_id`, `day`, `schedule_status`) VALUES (1, 3, 10, '2022-02-20', 0);


DROP TABLE IF EXISTS `preference`;

CREATE TABLE `preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `preference_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `department_id` (`department_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `preference_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `preference_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE,
  CONSTRAINT `preference_ibfk_3` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (1,1,1,1,"2022-03-06",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (2,5,1,2,"2022-03-30",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (3,9,1,2,"2022-03-03",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (4,13,1,2,"2022-03-24",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (5,17,1,1,"2022-03-21",2);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (6,21,1,1,"2022-03-15",2);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (7,25,1,2,"2022-03-10",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (8,29,1,1,"2022-03-08",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (9,33,1,2,"2022-03-31",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (10,37,1,1,"2022-03-11",1);

INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (12,54,2,3,"2022-04-12",2);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (13,51,2,10,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (14,51,2,4,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (15,55,2,4,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (16,54,2,7,"2022-04-14",2);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (17,47,2,9,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (18,42,2,10,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (19,45,2,8,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (20,48,2,5,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (21,50,2,8,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (22,52,2,10,"2022-04-12",2);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (23,44,2,7,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (24,44,2,9,"2022-04-14",2);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (25,46,2,6,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (26,49,2,7,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (27,46,2,9,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (28,56,2,7,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (29,48,2,3,"2022-04-11",2);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (30,52,2,7,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (31,54,2,9,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (32,43,2,8,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (33,55,2,5,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (34,42,2,7,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (35,49,2,8,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (36,44,2,5,"2022-04-14",2);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (37,52,2,10,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (38,42,2,9,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (39,46,2,5,"2022-04-12",2);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (40,46,2,8,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (41,46,2,8,"2022-04-13",1);

INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (42,61,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (43,61,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (44,74,3,12,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (45,58,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (46,68,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (47,58,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (48,71,3,11,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (49,69,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (50,59,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (51,67,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (52,70,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (53,58,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (54,73,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (55,75,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (56,73,3,12,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (57,57,3,13,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (58,61,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (59,72,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (60,57,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (61,64,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (62,68,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (63,61,3,13,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (64,61,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (65,67,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (66,72,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (67,63,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (68,68,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (69,74,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (70,70,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (71,59,3,11,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (72,75,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (73,69,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (74,75,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (75,63,3,14,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (76,68,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (77,73,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (78,63,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (80,65,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (82,71,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (84,61,3,14,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (85,73,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (88,58,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (89,66,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (90,58,3,11,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (91,58,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (93,67,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (94,75,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (95,67,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (96,76,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (97,59,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (99,59,3,11,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (101,61,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (103,70,3,14,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (105,75,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (107,64,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (108,68,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (110,69,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (113,70,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (114,57,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (115,74,3,11,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (116,71,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (117,58,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (118,66,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (119,72,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (120,62,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (121,63,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (125,66,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (126,70,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (127,74,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (128,63,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (129,74,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (130,72,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (132,60,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (133,61,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (136,59,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (137,58,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (138,59,3,14,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (139,57,3,12,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (140,62,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (141,70,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (142,68,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (143,75,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (144,69,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (145,70,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (146,70,3,13,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (147,62,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (148,64,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (149,76,3,12,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (150,61,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (151,71,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (152,73,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (153,73,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (154,66,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (155,59,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (156,65,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (157,72,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (158,63,3,14,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (159,59,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (160,59,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (161,61,3,11,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (162,64,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (163,63,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (164,68,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (167,66,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (168,61,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (169,76,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (170,62,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (171,63,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (172,73,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (173,60,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (174,68,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (175,70,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (176,71,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (177,67,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (178,70,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (179,59,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (180,74,3,14,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (181,70,3,11,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (182,71,3,11,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (183,62,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (184,62,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (185,74,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (186,65,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (187,74,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (188,62,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (189,58,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (190,67,3,13,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (191,76,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (192,60,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (193,66,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (194,68,3,11,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (195,68,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (196,66,3,11,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (197,59,3,13,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (198,64,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (199,75,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (201,69,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (202,74,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (203,66,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (204,71,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (205,65,3,14,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (206,71,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (207,73,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (208,63,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (209,67,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (210,59,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (211,73,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (212,64,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (213,63,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (215,73,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (216,59,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (217,68,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (218,59,3,11,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (219,67,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (220,73,3,14,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (221,58,3,11,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (222,67,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (223,65,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (224,74,3,14,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (225,69,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (226,63,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (227,76,3,13,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (228,66,3,11,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (229,61,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (230,67,3,13,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (231,72,3,13,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (232,74,3,12,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (233,71,3,14,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (234,60,3,12,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (235,73,3,14,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (236,57,3,11,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (237,70,3,13,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (238,68,3,13,"2022-04-11",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (239,60,3,12,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (240,73,3,12,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (241,64,3,13,"2022-04-12",0);

INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (242,94,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (243,95,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (244,82,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (245,83,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (246,87,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (247,99,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (248,95,4,15,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (249,90,4,15,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (250,81,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (251,90,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (252,79,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (252,79,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (253,86,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (254,93,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (255,89,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (257,88,4,18,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (258,82,4,15,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (259,88,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (260,88,4,18,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (262,99,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (263,84,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (264,98,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (265,91,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (266,97,4,15,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (267,85,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (268,91,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (269,91,4,15,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (271,79,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (272,89,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (274,93,4,15,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (276,84,4,15,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (277,80,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (278,83,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (279,91,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (280,87,4,16,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (281,97,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (283,91,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (284,84,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (285,87,4,18,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (286,81,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (287,83,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (288,99,4,18,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (289,83,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (290,79,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (291,93,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (292,94,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (293,99,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (294,80,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (295,88,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (296,91,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (297,94,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (298,78,4,15,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (299,98,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (300,96,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (301,86,4,18,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (302,84,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (303,95,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (304,80,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (305,85,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (306,93,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (307,98,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (309,95,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (310,100,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (311,96,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (312,85,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (313,95,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (314,95,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (316,88,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (317,77,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (318,82,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (319,87,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (320,79,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (321,80,4,16,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (322,92,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (323,84,4,16,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (324,95,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (325,100,4,18,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (326,92,4,15,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (327,89,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (328,95,4,15,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (329,77,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (330,85,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (331,85,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (332,85,4,16,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (333,82,4,15,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (334,97,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (335,98,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (336,92,4,18,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (337,96,4,18,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (338,84,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (339,77,4,18,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (340,97,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (341,84,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (342,86,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (343,82,4,15,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (344,89,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (345,82,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (346,82,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (347,81,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (348,86,4,17,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (349,80,4,16,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (350,86,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (351,98,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (353,100,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (354,82,4,15,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (355,83,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (356,79,4,17,"2022-04-13",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (357,99,4,15,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (358,86,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (359,84,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (360,83,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (361,93,4,17,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (362,85,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (363,92,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (364,91,4,16,"2022-04-14",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (365,98,4,15,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (366,87,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (367,78,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (368,97,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (369,91,4,17,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (370,78,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (371,94,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (372,86,4,16,"2022-04-12",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (373,89,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (374,96,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (375,85,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (376,93,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (377,90,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (378,84,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (379,98,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (380,91,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (381,98,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (382,92,4,16,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (383,96,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (384,99,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (385,89,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (386,83,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (387,99,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (388,91,4,18,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (389,91,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (390,86,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (391,79,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (392,87,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (393,81,4,16,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (394,81,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (395,84,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (396,95,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (397,90,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (398,96,4,18,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (399,96,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (400,96,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (401,86,4,18,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (402,90,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (403,90,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (404,88,4,15,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (405,93,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (406,80,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (407,98,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (408,97,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (409,99,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (410,82,4,17,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (411,87,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (412,80,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (413,77,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (414,80,4,15,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (415,86,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (416,98,4,18,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (417,87,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (418,92,4,15,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (419,91,4,16,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (420,82,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (421,90,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (422,90,4,16,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (423,84,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (424,80,4,17,"2022-04-15",0);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (425,83,4,15,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (426,79,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (427,81,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (428,95,4,17,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (429,79,4,15,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (430,82,4,15,"2022-04-13",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (431,98,4,15,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (432,81,4,16,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (433,78,4,16,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (434,89,4,17,"2022-04-15",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (435,85,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (436,95,4,17,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (437,81,4,18,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (438,99,4,16,"2022-04-12",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (439,94,4,17,"2022-04-14",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (440,83,4,18,"2022-04-11",1);
INSERT INTO `preference` (`id`,`employee_id`,`department_id`,`shift_id`,`day`,`preference_status`) VALUES (441,87,4,16,"2022-04-13",1);
