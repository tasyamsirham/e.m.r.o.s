class EmployeeScheduleRepresentative():
    def __init__(self, employee_id=None, day=None, shift_id=None, schedule_status=None):
        self.employee_id = employee_id
        self.day = day
        self.shift_id = shift_id
        self.schedule_status = schedule_status

    def __str__(self):
        return f"Employee ID: {self.employee_id} -- Shift ID: {self.shift_id} -- Day: {self.day} -- Schedule Status: {self.schedule_status}"

    def clone(self):
        clone_schedule = EmployeeScheduleRepresentative()
        clone_schedule.employee_id = self.employee_id
        clone_schedule.schedule_status = self.schedule_status
        clone_schedule.shift_id = self.shift_id
        clone_schedule.day = self.day

        return clone_schedule