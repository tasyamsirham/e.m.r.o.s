from . import EmployeeScheduleRepresentative
from random import random, choices
from datetime import timedelta


class Initializer():

    def __init__(self, global_variable, total_days, start_date):
        self.total_days = total_days
        self.start_date = start_date
        self.employee_list = global_variable.employee_id_list
        self.shift_dict = global_variable.shift_dict
        self.availabilities = global_variable.availability_dict
        self.capacity = global_variable.capacity
        self.minimum_work_hour = global_variable.minimum_work_hour
        self.maximum_work_hour = global_variable.maximum_work_hour
        self.minimum_employee_onsite = global_variable.minimum_employee_onsite

    def initialize_gen_zero(self, alpha=0.5):
        if random() < alpha:
            employee_schedule_list = self.initialize_pseudo_random_gen()
        else:
            employee_schedule_list = self.initialize_perfect_gen()
        
        return employee_schedule_list

    def initialize_pseudo_random_gen(self):
        genome_length = len(self.employee_list) * self.total_days * len(self.shift_dict)
        return choices(range(0,3), weights=(40, 40, 20), k=genome_length) #40% to generate on-site and remote, and 20% to generate not working

    def initialize_perfect_gen(self):
        genome_length = len(self.employee_list) * self.total_days * len(self.shift_dict)
        employee_schedule_list = choices(range(0,3), weights=(40, 40, 20), k=genome_length) #40% to generate on-site and remote, and 20% to generate not working
        employee_schedule_list = self.check_availability(employee_schedule_list)
        
        return employee_schedule_list

    def check_availability(self, employee_schedule_list):
        for key in self.availabilities:
            shift_id = key[1]
            shift_index = self.shift_dict[shift_id]['index']
            day_from_start_date = (key[2] - self.start_date).days
            employee_index = self.employee_list.index(key[0])
            schedule_index = (employee_index * len(self.shift_dict) * self.total_days) + shift_index + (len(self.shift_dict) * day_from_start_date)
            employee_schedule_list[schedule_index] = self.availabilities[key]
        
        return employee_schedule_list
    
    def __str__(self):
        return "\n".join([ 
            f"Employee ID: {employee_schedule.employee_id} -- Shift ID: {employee_schedule.shift_id} -- Day: {employee_schedule.day} -- Schedule Status: {employee_schedule.schedule_status}" 
            for employee_schedule in self.gen_zero 
            ])

