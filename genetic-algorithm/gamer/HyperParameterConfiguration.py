class HyperParameterConfiguration:
    def __init__(self, elitism_ratio, initializer_alpha, selector_alpha, reproductor_alpha, reproductor_max_multi_point_number, mutation_chance, mutation_alpha):
        self.elitism_ratio = elitism_ratio
        self.initializer_alpha = initializer_alpha
        self.selector_alpha = selector_alpha
        self.reproductor_alpha = reproductor_alpha
        self.reproductor_max_multi_point_number = reproductor_max_multi_point_number
        self.mutation_chance = mutation_chance
        self.mutation_alpha = mutation_alpha