from datetime import timedelta

from gamer.Evaluator.Evaluator import Evaluator

class HardConstraintViolationException(Exception):
    pass

class DefaultEvaluator(Evaluator):
    def __init__(self, genome, global_variable):
        super().__init__(genome)
        self.start_date = global_variable.start_date
        self.total_days = global_variable.total_days
        self.employee_list = global_variable.employee_id_list
        self.total_employee = len(self.employee_list)
        self.shift_dict = global_variable.shift_dict
        self.total_shift = len(self.shift_dict)
        self.preferences = global_variable.preference_dict
        self.availabilities = global_variable.availability_dict
        self.capacity = global_variable.capacity
        self.minimum_work_hour = global_variable.minimum_work_hour
        self.maximum_work_hour = global_variable.maximum_work_hour
        self.minimum_employee_onsite = global_variable.minimum_employee_onsite
        self.preference_violation_max_value = global_variable.preference_violation_max_value
        self.employee_is_previously_violated_flag = {}
        self.employee_total_work_hour_by_day = {}
        self.planning_duration_employee_on_site = set()
        self.shift_employee_on_site = {}
    
    def evaluate(self, weight_configuration):
        self.min_work_hour_ff_value = 0
        self.max_work_hour_ff_value = 0
        self.min_onsite_ff_value = 0
        self.room_capacity_ff_value = 0
        self.preference_ff_value = 0
        self.different_employee_shift_ff_value = 0
        self.different_employee_planning_duration_value = 0
        
        minimum_work_hour_weight= weight_configuration.minimum_work_hour_weight
        maximum_work_hour_weight= weight_configuration.maximum_work_hour_weight
        minimum_onsite_weight= weight_configuration.minimum_onsite_weight
        room_capacity_weight= weight_configuration.room_capacity_weight
        preference_weight= weight_configuration.preference_weight
        different_employee_per_shift_weight= weight_configuration.different_employee_per_shift_weight
        different_employee_total_weight= weight_configuration.different_employee_total_weight
        
        try:
            self.check_hard_constraints_violation()
            self.check_soft_constraints_violation()
        
        except HardConstraintViolationException:
            return 0

        return ((self.max_work_hour_ff_value * maximum_work_hour_weight) + \
            (self.preference_ff_value * preference_weight) + \
            (self.different_employee_shift_ff_value * different_employee_per_shift_weight) + \
            (self.different_employee_planning_duration_value * different_employee_total_weight) + \
            (self.min_work_hour_ff_value * minimum_work_hour_weight) + \
            (self.min_onsite_ff_value * minimum_onsite_weight) + \
            (self.room_capacity_ff_value * room_capacity_weight)) / 1

    def check_hard_constraints_violation(self):
        for idx in range(self.total_shift * self.total_days): #iterate per shift
            day_from_start_date = idx // self.total_shift
            day = self.start_date + timedelta(days=day_from_start_date)
            shift_index = idx % self.total_shift
            shift_id = [*self.shift_dict][shift_index]
            
            shift_employee_on_site_key = (shift_id, day)
            if shift_employee_on_site_key not in self.shift_employee_on_site:
                self.shift_employee_on_site[shift_employee_on_site_key] = set()
            
            for employee_idx, schedule_status_idx in enumerate(range(idx, len(self.genome), self.total_shift * self.total_days)): #iterate per employee in shift on day
                employee_id = self.employee_list[employee_idx]
                schedule_status = self.genome[schedule_status_idx]
                employee_total_work_hour_key = (employee_id, day)
                
                if employee_total_work_hour_key not in self.employee_total_work_hour_by_day:
                    self.employee_total_work_hour_by_day[employee_total_work_hour_key] = 0
                
                if schedule_status == 0 or schedule_status == 1:
                    self.employee_total_work_hour_by_day[employee_total_work_hour_key] += self.shift_dict[shift_id]["total_hour"]
                    
                if schedule_status == 0:
                    self.planning_duration_employee_on_site.add(employee_id)
                    self.shift_employee_on_site[shift_employee_on_site_key].add(employee_id)
                
                self.check_availability_violation(schedule_status, employee_id, shift_id, day)
        
    def check_availability_violation(self, schedule_status, employee_id, shift_id, day):
        try:
            availability = self.availabilities[(employee_id, shift_id, day)]
            
            if availability != schedule_status:
                raise HardConstraintViolationException("Violated HC-5")
        
        except KeyError:
            pass

    def check_soft_constraints_violation(self):
        self.check_minimum_employee_onsite_violation()
        self.check_room_capacity_per_shift_violation()
        self.check_min_max_work_hour_violation()
        self.check_preference_violation()
        self.check_shift_different_employee_ratio()
        self.check_planning_duration_different_employee_ratio()

    def check_minimum_employee_onsite_violation(self):
        local_fitness_function = 0
        for _, different_employee_set in self.shift_employee_on_site.items():
            employee_on_site = len(different_employee_set)
        
            if employee_on_site < self.minimum_employee_onsite:
                local_fitness_function += (employee_on_site / self.minimum_employee_onsite)
            else:
                local_fitness_function += 1
        
        self.min_onsite_ff_value = local_fitness_function / len(self.shift_employee_on_site)

    def check_room_capacity_per_shift_violation(self):
        local_fitness_function = 0
        for _, different_employee_set in self.shift_employee_on_site.items():
            employee_on_site = len(different_employee_set)
        
            if employee_on_site > self.capacity:
                local_fitness_function += (self.capacity / employee_on_site)
            else:
                local_fitness_function += 1
        
        self.room_capacity_ff_value = local_fitness_function / len(self.shift_employee_on_site)

    def check_min_max_work_hour_violation(self):
        local_min_hour_fitness_function = 0
        local_max_hour_fitness_function = 0
        
        for employee_id in self.employee_list:
            
            for day in range(self.total_days):
                current_date = self.start_date + timedelta(days=day)

                current_date_work_hours = self.employee_total_work_hour_by_day[(employee_id, current_date)]
                
                if current_date_work_hours < self.minimum_work_hour:
                    local_min_hour_fitness_function += (current_date_work_hours**(2) / self.minimum_work_hour**(2))
                    local_max_hour_fitness_function += 1
                elif self.minimum_work_hour <= current_date_work_hours <= self.maximum_work_hour:
                    local_min_hour_fitness_function += 1
                    local_max_hour_fitness_function += 1
                else:
                    local_min_hour_fitness_function += 1
                    local_max_hour_fitness_function += (self.maximum_work_hour**(2) / current_date_work_hours**(2))
                    
        self.min_work_hour_ff_value = local_min_hour_fitness_function / (self.total_employee * self.total_days)
        self.max_work_hour_ff_value = local_max_hour_fitness_function / (self.total_employee * self.total_days)
    
    def check_preference_violation(self):
        preference_fitness_function = 0
        preference_fitness_function = self.check_employee_preference_violation(self.genome)
        
        self.preference_ff_value = (1 - preference_fitness_function)
        

    def check_employee_preference_violation(self, employee_schedule_list):
        preference_fitness_function = 0
        
        for key in self.preferences:
            shift_id = key[1]
            shift_index = self.shift_dict[shift_id]['index']
            day_from_start_date = (key[2] - self.start_date).days
            employee_index = self.employee_list.index(key[0])
            schedule_index = (employee_index * len(self.shift_dict) * self.total_days) + shift_index + (len(self.shift_dict) * day_from_start_date)
            schedule_status = employee_schedule_list[schedule_index]
            
            if key[0] not in self.employee_is_previously_violated_flag:
                self.employee_is_previously_violated_flag[key[0]] = 0
                
            if self.preferences[key] != schedule_status and self.employee_is_previously_violated_flag[key[0]] == 1:
                preference_fitness_function += (2 / self.preference_violation_max_value)
            elif self.preferences[key] != schedule_status:
                preference_fitness_function += (1 / self.preference_violation_max_value)
                self.employee_is_previously_violated_flag[key[0]] = 1
            else:
                self.employee_is_previously_violated_flag[key[0]] = 0
        
        return preference_fitness_function

    def check_planning_duration_different_employee_ratio(self):
        planning_duration_employee_count = len(self.planning_duration_employee_on_site)
        self.different_employee_planning_duration_value = (1 - (planning_duration_employee_count / self.total_employee))

    def check_shift_different_employee_ratio(self):
        shift_different_employee_ratio_fitness_function = 0
        
        for _, different_employee_set in self.shift_employee_on_site.items():
            shift_different_employee_count = len(different_employee_set)
            shift_different_employee_ratio_fitness_function += (1 - (shift_different_employee_count / self.capacity))
        
        self.different_employee_shift_ff_value = shift_different_employee_ratio_fitness_function / len(self.shift_employee_on_site.items())