from abc import ABC, abstractmethod

class Evaluator(ABC):
    def __init__(self, genome):
        self.genome = genome
        self.value = 0

    @abstractmethod
    def evaluate(self):
        pass

    def get_fitness_function(self):
        return self.value

    @abstractmethod
    def check_hard_constraints_violation(self):
        pass

    @abstractmethod
    def check_soft_constraints_violation(self):
        pass
