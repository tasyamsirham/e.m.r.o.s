class WeightConfiguration:
    def __init__(self, minimum_work_hour_weight, maximum_work_hour_weight, 
                 minimum_onsite_weight, room_capacity_weight, preference_weight, 
                 different_employee_per_shift_weight, different_employee_total_weight):
        self.minimum_work_hour_weight = minimum_work_hour_weight
        self.maximum_work_hour_weight = maximum_work_hour_weight
        self.minimum_onsite_weight = minimum_onsite_weight
        self.room_capacity_weight = room_capacity_weight
        self.preference_weight = preference_weight
        self.different_employee_per_shift_weight = different_employee_per_shift_weight
        self.different_employee_total_weight = different_employee_total_weight