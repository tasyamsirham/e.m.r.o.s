import random
import numpy as np

class Selector():
    def __init__(self, evaluation_list, generation):
        self.evaluation_list = evaluation_list
        self.generation = generation

    def get_parent(self, alpha=0.5):
        # alpha is the percentage of picking tournament method as its selection method
        # Performs a selection process on all of the individuals
        # Should return an index of evaluation_list
        if random.random() < alpha:
            parent = self.tournament_selection()
        else:
            parent = self.biased_random_selection()

        return parent

    def tournament_selection(self):
        candidate_one_idx, candidate_two_idx = random.sample(range(len(self.evaluation_list)), 2)
        candidate_one,  candidate_two = self.evaluation_list[candidate_one_idx], self.evaluation_list[candidate_two_idx]
        selected_genome_index = candidate_one_idx if candidate_one > candidate_two else candidate_two_idx
        
        return self.generation[selected_genome_index]

    def biased_random_selection(self):
        evaluation_np = np.array(self.evaluation_list)
        sum_of_evaluation = np.sum(evaluation_np)

        if sum_of_evaluation == 0 or (evaluation_np[0] == sum_of_evaluation):# in case the generation is filled with 0 fitness function as to not return the same index over and over again
            selected_genome_index = random.choice(range(len(evaluation_np)))
            return self.generation[selected_genome_index]

        inverse_proportions = sum_of_evaluation / evaluation_np
        inverse_proportions[np.isinf(inverse_proportions)] = 0
        normalized_evaluation = inverse_proportions / np.sum(inverse_proportions)
        cumulative_normalized_evaluation = np.cumsum(normalized_evaluation)

        random_value = random.random()
        selected_genome_index = np.searchsorted(cumulative_normalized_evaluation, [random_value,], side='right')[0]
        return self.generation[selected_genome_index]
