import MySQLdb
from config import MYSQL_DB_USER, MYSQL_DB_HOST, MYSQL_DB_PASSWORD, MYSQL_DB_PORT, MYSQL_DB_NAME

class GlobalVariable:
    def __init__(self, start_date, end_date, department_id):
        self.mysql_conn = MySQLdb.connect(user=MYSQL_DB_USER, host=MYSQL_DB_HOST, password=MYSQL_DB_PASSWORD, port=int(MYSQL_DB_PORT), database=MYSQL_DB_NAME)
        self.cursor = self.mysql_conn.cursor()
        
        self.schedule_request_id = self.get_schedule_request_id(department_id, start_date)
        self.department_id = department_id
        self.end_date = end_date
        self.start_date = start_date
        self.total_days = self.get_total_days()
        self.preference_dict, self.preference_violation_max_value = self.get_preference_dict(start_date, end_date, department_id)
        self.availability_dict = self.get_availability_dict(start_date, end_date, department_id)
        self.employee_id_list = self.get_employee_id_list(department_id)
        self.shift_dict = self.get_shift_dict(department_id)
        
        self.capacity, \
        self.minimum_work_hour, \
        self.maximum_work_hour, \
        self.minimum_employee_onsite = self.get_contract_details_and_room_capacity(department_id)
        
        self.cursor.close()
    
    def get_total_days(self):
        return (self.end_date - self.start_date).days + 1
    
    def get_schedule_request_id(self, department_id, start_date):
        query = f"""
            SELECT 
                id
            FROM 
                schedule_request
            WHERE 
                start_date = '{start_date}' 
                and 
                department_id = {department_id}
        """
        self.cursor.execute(query)
        res = self.cursor.fetchone()
        return res[0]
    
    def get_preference_dict(self, start_date, end_date, department_id):
        query = f"""
            SELECT 
                employee_id, 
                shift_id, 
                day, 
                preference_status 
            FROM 
                preference 
            WHERE 
                day >= '{start_date}' 
                and 
                day <= '{end_date}'
                and 
                department_id={department_id}
        """
        self.cursor.execute(query)
        res = self.cursor.fetchall()
        employee_id_set = set()
        ff_max_value = 0
        result = {}
        for r in res:
            if r[0] not in employee_id_set:
                ff_max_value += 1
                employee_id_set.add(r[0])
            else:
                ff_max_value += 2
            
            result[(r[0], r[1], r[2])] = r[3]
        
        return result, ff_max_value
    
    def get_availability_dict(self, start_date, end_date, department_id):
        query = f"""
            SELECT 
                employee_id, 
                shift_id, 
                day, 
                availability_status 
            FROM 
                availability 
            WHERE 
                day >= '{start_date}' 
                and 
                day <= '{end_date}'
                and 
                department_id={department_id}
        """
        self.cursor.execute(query)
        res = self.cursor.fetchall()
        result = {}
        for idx, r in enumerate(res):
            result[(r[0], r[1], r[2])] = r[3]
        return result
    
    def get_employee_id_list(self, department_id):
        query = f"""
            SELECT 
                id
            FROM 
                employee 
            WHERE 
                department_id={department_id}
        """
        self.cursor.execute(query)
        res = self.cursor.fetchall()
        return [r[0] for r in res]
    
    def get_shift_dict(self, department_id):
        query = f"""
            SELECT
                shift_id, (end_hour - start_hour) as hour_duration
            FROM (
                SELECT 
                    shift_id
                FROM 
                    department_shift 
                WHERE 
                    department_id={department_id}
                ) department_shift
            INNER JOIN shift ON department_shift.shift_id=shift.id
        """
        self.cursor.execute(query)
        res = self.cursor.fetchall()
        result = {}
        for idx, r in enumerate(res):
            result[r[0]] = {"index": idx, "total_hour": r[1]}
        return result
    
    def get_contract_details_and_room_capacity(self, department_id):
        query = f"""
            SELECT 
                capacity, minimum_work_hour, maximum_work_hour, minimum_employee_onsite
            FROM 
                (
                    SELECT id, room_id, contract_id, minimum_employee_onsite FROM department WHERE id={department_id}
                ) department
            LEFT JOIN room ON department.room_id=room.id
            LEFT JOIN contract ON department.contract_id=contract.id
        """
        self.cursor.execute(query)
        res = self.cursor.fetchone()
        return [res[0], res[1], res[2], res[3]]