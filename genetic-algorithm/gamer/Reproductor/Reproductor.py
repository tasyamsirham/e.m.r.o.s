import random

class Reproductor():
    def __init__(self, mother, father):
        self.mother = mother
        self.father = father
        self.individual_length = len(mother)
    
    def get_offsprings(self, alpha = 0.5, max_multi_point_number = 10):
        # alpha is the percentage of picking tournament method as its selection method
        # Performs a selection process on all of the individuals
        # Should return an index of evaluation_list
        if random.random() < alpha:
            offsprings = self.single_point_crossover()
        else:
            random_number_of_point = random.randrange(2, max_multi_point_number+1)
            offsprings = self.multi_point_crossover(random_number_of_point)

        return offsprings

    def single_point_crossover(self):
        random_point = random.randrange(self.individual_length)
        offspring_one = self.mother[0:random_point] + self.father[random_point:]
        offspring_two = self.father[0:random_point] + self.mother[random_point:]
        
        return [offspring_one, offspring_two]

    def multi_point_crossover(self, number_of_point):
        try:
            random_points = random.sample(range(1, self.individual_length-1), number_of_point)
        except ValueError:
            random_points = random.sample(range(1, self.individual_length-1), 2)
        sorted_random_points = sorted(random_points)
        sorted_random_points.insert(0, 0)
        sorted_random_points.append(self.individual_length)
        offspring_one, offspring_two = [], []
        
        for idx, point in enumerate(sorted_random_points[:-1]):
            if idx % 2 == 1:
                offspring_one += self.father[point:sorted_random_points[idx+1]]
                offspring_two += self.mother[point:sorted_random_points[idx+1]]
            else:
                offspring_one += self.mother[point:sorted_random_points[idx+1]]
                offspring_two += self.father[point:sorted_random_points[idx+1]]
        
        return [offspring_one, offspring_two]
    
    def uniform_crossover(self):
        offspring_one, offspring_two = [], []
        
        for i in range(self.individual_length):
            random_chance = random.random()
            if random_chance >= 0.5:
                offspring_one.append(self.mother[i])
                offspring_two.append(self.father[i])
            else:
                offspring_one.append(self.father[i])
                offspring_two.append(self.mother[i])
                
        return [offspring_one, offspring_two]