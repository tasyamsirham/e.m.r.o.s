import random

class Mutator():
    def __init__(self, offspring):
        self.offspring = offspring
    
    def mutate(self, mutation_chance = 0.05, alpha = 0.5):
        # alpha is the chance of the offspring undergoing mutation
        if random.random() < mutation_chance:
            if random.random() < alpha:
                self.swap_mutate()
            else:
                self.rotation_mutate()

        return self.offspring

    def swap_mutate(self):
        random_point_a, random_point_b = random.sample(range(len(self.offspring)), 2)
        cell_a, cell_b = self.offspring[random_point_a], self.offspring[random_point_b]
        self.offspring[random_point_a], self.offspring[random_point_b] = cell_b, cell_a


    def rotation_mutate(self):
        random_points = random.sample(range(len(self.offspring)), 2)
        sorted_random_points = sorted(random_points)
        random_point_a, random_point_b = sorted_random_points

        for idx in range(0, random_point_a, random_point_b + 1):
            cell_a, cell_b = self.offspring[random_point_a + idx], self.offspring[random_point_b - idx]
            self.offspring[random_point_a + idx], self.offspring[random_point_b - idx] = cell_b, cell_a