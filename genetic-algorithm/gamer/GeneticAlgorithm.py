import heapq
import math
import time
import json
import iso8601
import datetime
from gamer import GlobalVariable

class GeneticAlgorithm:
    def __init__(self, initializer, evaluator, selector, reproductor, mutator, individual_count, department_id, start_date, end_date, hyper_parameter_config, weight_config):
        self.evaluator = evaluator
        self.selector = selector
        self.reproductor = reproductor
        self.mutator = mutator
        self.hyper_parameter_config = hyper_parameter_config
        self.weight_config = weight_config
        
        start_date = iso8601.parse_date(start_date).date()
        end_date = iso8601.parse_date(end_date).date()
        self.global_variable = GlobalVariable(start_date, end_date, department_id)
        self.evaluation_list = []

        #  Create the first generation
        self.individual_count = individual_count
        
        self.generation = [
            initializer(self.global_variable, (end_date - start_date).days + 1, start_date).initialize_gen_zero(self.hyper_parameter_config.initializer_alpha)
            for _ in range(self.individual_count)
        ]
        
    def run(self, max_time_in_seconds):
        last_fitness_function = 0
        counter = 1
        convergence_counter = 0
        timeout_end = time.time() + max_time_in_seconds
        
        while time.time() < timeout_end:
            self.iterate()
            fitness_function = self.get_fittest_genome_fitness_function()
            is_close = math.isclose(last_fitness_function, fitness_function)
            if convergence_counter < 5 and is_close:
                convergence_counter += 1
            elif convergence_counter == 5 and is_close:
                break
            else:
                convergence_counter = 0
            last_fitness_function = fitness_function
            counter += 1

    def iterate(self):
        next_generation = []

        #  Get the fitness function for each individuals
        #start_time = time.time()
        self.evaluation_list = [
            self.evaluator(genome, self.global_variable).evaluate(self.weight_config)
            for genome in self.generation
        ]
        #print("--- %s seconds ---" % (time.time() - start_time))

        current_generation_selector = self.selector(self.evaluation_list, self.generation)
        for _ in range(self.individual_count):
            #  Select two fittest individuals
            mother = current_generation_selector.get_parent(self.hyper_parameter_config.selector_alpha)
            father = current_generation_selector.get_parent(self.hyper_parameter_config.selector_alpha)
            is_same_counter = 0
            while mother == father and is_same_counter <= 10:
                father = current_generation_selector.get_parent(self.hyper_parameter_config.selector_alpha)
                is_same_counter += 1
            
            offsprings = self.reproductor(mother, father).get_offsprings(self.hyper_parameter_config.reproductor_alpha, self.hyper_parameter_config.reproductor_max_multi_point_number)
            offsprings = [self.mutator(offspring).mutate(self.hyper_parameter_config.mutation_alpha) for offspring in offsprings]
            next_generation += offsprings

        #elitism implementation based on elitism ratio
        elitism_ratio = self.hyper_parameter_config.elitism_ratio
        elite_count = round(elitism_ratio * self.individual_count)
        next_gen_evaluation_list = [
            self.evaluator(genome, self.global_variable).evaluate(self.weight_config)
            for genome in next_generation
        ]
        
        n_fittest_next_gen_genomes = self.get_n_fittest_genome(next_generation, next_gen_evaluation_list, self.individual_count - elite_count)
        elite_genomes = self.get_n_fittest_genome(self.generation, self.evaluation_list, elite_count)

        self.generation = elite_genomes + n_fittest_next_gen_genomes

    def get_n_fittest_genome(self, generation, evaluation_list, n):
        n_fittest_genome_indexes = heapq.nlargest(n, range(len(evaluation_list)), key=evaluation_list.__getitem__)
        return [generation[index] for index in n_fittest_genome_indexes]

    def get_fittest_genome(self):
        return self.get_n_fittest_genome(self.generation, self.evaluation_list, 1)[0]

    def get_fittest_genome_fitness_function(self):
        return self.evaluator(self.get_n_fittest_genome(self.generation, self.evaluation_list, 1)[0], self.global_variable).evaluate(self.weight_config)
    
    def get_serialized_json_result(self):
        fittest_genome = self.get_fittest_genome()
        employee_schedule_list = []
        total_shift = len(self.global_variable.shift_dict)
        list_shift_dict_keys = [*self.global_variable.shift_dict]
        
        for idx in range(total_shift * self.global_variable.total_days): #iterate per shift
            day_from_start_date = idx // total_shift
            day = self.global_variable.start_date + datetime.timedelta(days=day_from_start_date)
            shift_index = idx % total_shift
            shift_id = list_shift_dict_keys[shift_index]
            
            for employee_idx, schedule_status_idx in enumerate(range(idx, len(fittest_genome), total_shift * self.global_variable.total_days)): #iterate per employee in shift on day
                employee_id = self.global_variable.employee_id_list[employee_idx]
                schedule_status = fittest_genome[schedule_status_idx]
                employee_schedule_dict = {
                    "schedule_request_id": self.global_variable.schedule_request_id,
                    "employee_id": employee_id,
                    "shift_id": shift_id,
                    "day": day.strftime("%Y-%m-%d"),
                    "schedule_status": schedule_status
                }
                
                employee_schedule_list.append(employee_schedule_dict)
                
        
        return json.dumps(employee_schedule_list)