from gamer import GeneticAlgorithm, HyperParameterConfiguration, WeightConfiguration, Initializer, Metrics, DefaultEvaluator, Mutator, Reproductor, Selector, Metrics
from utils.redis import add_schedule_request_result

hyper_parameter_config = HyperParameterConfiguration(elitism_ratio=0.1, initializer_alpha=0.5, 
                                                            selector_alpha=0.5, reproductor_alpha=0.5, 
                                                            reproductor_max_multi_point_number=10, 
                                                            mutation_alpha=0.5, mutation_chance=0.05)

weight_config = WeightConfiguration(minimum_work_hour_weight=0.2, maximum_work_hour_weight=0, minimum_onsite_weight=0.4,
                                    room_capacity_weight=0.3, preference_weight=0.1, different_employee_per_shift_weight=0,
                                    different_employee_total_weight=0)

genetic_algorithm = GeneticAlgorithm(Initializer, DefaultEvaluator, Selector, 
                                     Reproductor, Mutator, 500, 2, '2022-03-16', 
                                     '2022-03-17', hyper_parameter_config, weight_config)

genetic_algorithm.run(300)

print("FITNESS FUNCTION: ", genetic_algorithm.get_fittest_genome_fitness_function())

Metrics(genetic_algorithm.get_fittest_genome(), genetic_algorithm.global_variable).get_metrics()

add_schedule_request_result(genetic_algorithm.global_variable.schedule_request_id, genetic_algorithm.get_serialized_json_result())