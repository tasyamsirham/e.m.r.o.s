import redis
from config import REDIS_DB_HOST

def add_schedule_request_result(schedule_request_id, result_json_serialized):
    r = redis.Redis(host=REDIS_DB_HOST)
    r.rpush("finished_schedule_request_queue", schedule_request_id)
    r.set(f"schedule_request_result:{schedule_request_id}", result_json_serialized)

def add_schedule_request_fitness_function(schedule_request_id, fitness_function):
    r = redis.Redis(host=REDIS_DB_HOST)
    r.set(f"schedule_request_fitness_function:{schedule_request_id}", fitness_function)
    