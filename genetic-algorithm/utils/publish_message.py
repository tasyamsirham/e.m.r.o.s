
import json
from google.cloud import pubsub_v1
from gamer import GeneticAlgorithm, HyperParameterConfiguration, WeightConfiguration, Initializer, DefaultEvaluator, Mutator, Reproductor, Selector
from utils.redis import add_schedule_request_result, add_schedule_request_fitness_function

def generate_schedule_callback(message: pubsub_v1.subscriber.message.Message) -> None:
    data_str = message.data.decode('utf-8')
    data = json.loads(data_str)
    hyper_parameter_config = HyperParameterConfiguration(elitism_ratio=0.1, initializer_alpha=0.5, 
                                                            selector_alpha=0.5, reproductor_alpha=0.5, 
                                                            reproductor_max_multi_point_number=10, 
                                                            mutation_alpha=0.5, mutation_chance=0.05)

    weight_config = WeightConfiguration(minimum_work_hour_weight=data["minimum_work_hour_weight"], maximum_work_hour_weight=data["maximum_work_hour_weight"], 
                                        minimum_onsite_weight=data["minimum_onsite_weight"], room_capacity_weight=data["room_capacity_weight"], 
                                        preference_weight=data["preference_weight"], different_employee_per_shift_weight=data["different_employee_per_shift_weight"],
                                        different_employee_total_weight=data["different_employee_total_weight"])

    genetic_algorithm = GeneticAlgorithm(Initializer, DefaultEvaluator, Selector, 
                                        Reproductor, Mutator, 500, data["department_id"], data["start_date"], 
                                        data["end_date"], hyper_parameter_config, weight_config)

    genetic_algorithm.run(300)

    add_schedule_request_result(genetic_algorithm.global_variable.schedule_request_id, genetic_algorithm.get_serialized_json_result())
    add_schedule_request_fitness_function(genetic_algorithm.global_variable.schedule_request_id, genetic_algorithm.get_fittest_genome_fitness_function())
    
    message.ack()
    
    print(f"Schedule #{genetic_algorithm.global_variable.schedule_request_id} created for department #{data['department_id']} on {data['start_date']} until {data['end_date']}")