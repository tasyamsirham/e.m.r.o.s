import datetime
from pydantic import BaseModel

class ScheduleRequest(BaseModel):
    department_id: int
    start_date: datetime.datetime
    planning_period: int
    individual_count: int = 10
    iterations: int = 10
    elitism_ratio: int = 0.1
    initializer_alpha: int = 0.5
    selector_alpha: int = 0.5
    reproductor_alpha: int = 0.5
    reproductor_max_multi_point_number: int = 10
    mutation_alpha: int = 0.5
    mutation_chance: int = 0.05