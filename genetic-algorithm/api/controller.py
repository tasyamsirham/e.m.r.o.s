from fastapi import FastAPI, Header
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware

from config import GAMER_ACCESS_TOKEN
from api.models import ScheduleRequest, ScheduleResponse
from api.service import ScheduleService

AUTH_HEADER = Header(None, description="Bearer token that contains the access token. Must be in 'Bearer ...' format")

app = FastAPI(root_path="/gamer/v1")

app.add_middleware(CORSMiddleware, 
    allow_origins = ["*"],
    allow_methods=["*"],
    allow_headers=["*"]
)

def _authenticate(function):
    def authenticate_api_call(*args, authentication, **kwargs):
        stripped_access_token = authentication[7:]
        
        if stripped_access_token == GAMER_ACCESS_TOKEN:
            return function(*args, authentication, **kwargs)
        else:
            raise ValueError("Invalid API Access Token")

    return authenticate_api_call

@_authenticate
@app.post("/schedule", response_model=ScheduleResponse, responses={400: {"model": ScheduleResponse}})
async def schedule(request: ScheduleRequest, authentication=AUTH_HEADER):
    ScheduleService.schedule(department_id=request.department_id, start_date=request.start_date, planning_period=request.planning_period, 
                        individual_count=request.individual_count, iterations=request.iterations, elitism_ratio=request.elitism_ratio, 
                        initializer_alpha=request.initializer_alpha, selector_alpha=request.selector_alpha, reproductor_alpha=request.reproductor_alpha, 
                        reproductor_max_multi_point_number=request.reproductor_max_multi_point_number, mutation_alpha=request.mutation_alpha, 
                        mutation_chance=request.mutation_chance)

    return JSONResponse(status_code=200, content={
        "success": True,
        "message": "Job is successful, Your department schedules have been assigned"
    })