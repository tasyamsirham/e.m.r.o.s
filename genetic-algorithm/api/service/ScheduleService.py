from gamer import GeneticAlgorithm, HyperParameterConfiguration, Initializer, Metrics, DefaultEvaluator, Mutator, Reproductor, Selector
from models import SESSION, EmployeeSchedule

class ScheduleService:
    @staticmethod
    def schedule(department_id, start_date, planning_period, 
                individual_count, iterations, elitism_ratio, 
                initializer_alpha, selector_alpha, reproductor_alpha, 
                reproductor_max_multi_point_number, mutation_alpha, mutation_chance):
        hyper_parameter_config = HyperParameterConfiguration(elitism_ratio=elitism_ratio, initializer_alpha=initializer_alpha, 
                                                            selector_alpha=selector_alpha, reproductor_alpha=reproductor_alpha, 
                                                            reproductor_max_multi_point_number=reproductor_max_multi_point_number, 
                                                            mutation_alpha=mutation_alpha, mutation_chance=mutation_chance)
        genetic_algorithm = GeneticAlgorithm(Initializer, DefaultEvaluator, Selector, Reproductor, Mutator, individual_count, department_id, start_date, planning_period, hyper_parameter_config)

        for _ in range(iterations):
            genetic_algorithm.iterate()
            print(f"ITERATION {_} DONE")

        for employee_schedule in genetic_algorithm.get_fittest_genome():
            orm_employee_schedule = EmployeeSchedule()
            orm_employee_schedule.employee = employee_schedule.employee
            orm_employee_schedule.employee_id = employee_schedule.employee_id
            orm_employee_schedule.schedule_status = employee_schedule.schedule_status
            orm_employee_schedule.shift = employee_schedule.shift
            orm_employee_schedule.shift_id = employee_schedule.shift_id
            orm_employee_schedule.day = employee_schedule.day
            SESSION.add(orm_employee_schedule)

        print("FITNESS FUNCTION: ", genetic_algorithm.get_fittest_genome_fitness_function())

        metrics = Metrics(genetic_algorithm.get_fittest_genome())
        metrics.get_metrics()
        SESSION.commit()