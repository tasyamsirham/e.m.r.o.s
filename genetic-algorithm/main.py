import os
from google.cloud import pubsub_v1
from concurrent.futures import TimeoutError
from utils.publish_message import generate_schedule_callback
from config import PROJECT_ID, SUBSCRIBER_ID

timeout = None
print("starting...")
subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path(PROJECT_ID, SUBSCRIBER_ID)

streaming_pull_future = subscriber.subscribe(subscription_path, callback=generate_schedule_callback)
print(f"Listening for messages on {subscription_path}..\n")

# Wrap subscriber in a 'with' block to automatically call close() when done.
with subscriber:
    try:
        streaming_pull_future.result(timeout=timeout)
    except TimeoutError:
        streaming_pull_future.cancel()  # Trigger the shutdown.
        streaming_pull_future.result()  # Block until the shutdown is complete.