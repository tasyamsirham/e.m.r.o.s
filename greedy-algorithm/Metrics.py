from datetime import timedelta

class HardConstraintViolationException(Exception):
    pass

class Metrics():
    def __init__(self, genome, global_variable):
        self.genome = genome
        self.start_date = global_variable.start_date
        self.total_days = global_variable.total_days
        self.employee_list = global_variable.employee_id_list
        self.total_employee = len(self.employee_list)
        self.shift_dict = global_variable.shift_dict
        self.total_shift = len(self.shift_dict)
        self.preferences = global_variable.preference_dict
        self.availabilities = global_variable.availability_dict
        self.capacity = global_variable.capacity
        self.minimum_work_hour = global_variable.minimum_work_hour
        self.maximum_work_hour = global_variable.maximum_work_hour
        self.minimum_employee_onsite = global_variable.minimum_employee_onsite
        self.employee_is_previously_violated_flag = {}
        self.employee_total_work_hour_by_day = {}
        self.planning_duration_employee_on_site = set()
        self.shift_employee_on_site = {}
        self.initiate_attributes()

    def get_total_days(self):
        return (self.genome[-1].day - self.genome[0].day).days + 1

    def initiate_attributes(self):
        for idx in range(self.total_shift * self.total_days):
            day_from_start_date = idx // self.total_shift
            day = self.start_date + timedelta(days=day_from_start_date)
            shift_index = idx % self.total_shift
            shift_id = [*self.shift_dict][shift_index]
            
            shift_employee_on_site_key = (shift_id, day)
            if shift_employee_on_site_key not in self.shift_employee_on_site:
                self.shift_employee_on_site[shift_employee_on_site_key] = set()
            
            for employee_idx, schedule_status_idx in enumerate(range(idx, len(self.genome), self.total_shift * self.total_days)): #iterate per employee in shift on day
                employee_id = self.employee_list[employee_idx]
                schedule_status = self.genome[schedule_status_idx]
                employee_total_work_hour_key = (employee_id, day)
                
                if employee_total_work_hour_key not in self.employee_total_work_hour_by_day:
                    self.employee_total_work_hour_by_day[employee_total_work_hour_key] = 0
                
                if schedule_status == 0 or schedule_status == 1:
                    self.employee_total_work_hour_by_day[employee_total_work_hour_key] += self.shift_dict[shift_id]["total_hour"]
                    
                if schedule_status == 0:
                    self.planning_duration_employee_on_site.add(employee_id)
                    self.shift_employee_on_site[shift_employee_on_site_key].add(employee_id)

    def get_metrics(self):
        availability_violated = self.get_availability_violation()
        minimum_work_hour_violated, maximum_work_hour_violation = self.get_work_hour_violated()
        minimum_employee_onsite_violated = self.get_minimum_employee_onsite_violated()
        room_capacity_per_shift_violated = self.get_room_capacity_per_shift_violated()
        preference_violated, subsequent_preference_violated = self.get_preference_violated()
        shift_different_employee_ratio = self.get_shift_different_employee_ratio()
        planning_duration_different_employee_ratio = self.get_planning_duration_different_employee_ratio()

        print("\n=============== % METRICS % ===============\n")

        print("\n===== % HARD CONSTRAINTS % =====\n")

        print("Availability Violated: ", availability_violated)

        print("\n===== % SOFT CONSTRAINTS % =====\n")
        
        print("Minimum Work Hour Violated: ", minimum_work_hour_violated, "\n")
        print("Maximum Work Hour Violated: ", maximum_work_hour_violation, "\n")
        print("Minimum Employee On Site Violated: ", minimum_employee_onsite_violated, "\n")
        print("Room Capacity Violated: ", room_capacity_per_shift_violated, "\n")
        print("Preference Violated: ", preference_violated, "\n")
        print("Subsequent Preference Violated: ", subsequent_preference_violated, "\n")
        print("Planning Duration Different Employee Ratio: ", planning_duration_different_employee_ratio, "\n")
        print("Shift Different Employee Ratio: ", shift_different_employee_ratio, "\n")

    def get_availability_violation(self):
        availability_violated_count = 0
        for key in self.availabilities:
            shift_id = key[1]
            shift_index = self.shift_dict[shift_id]['index']
            day_from_start_date = (key[2] - self.start_date).days
            employee_index = self.employee_list.index(key[0])
            schedule_index = (employee_index * len(self.shift_dict) * self.total_days) + shift_index + (len(self.shift_dict) * day_from_start_date)
            schedule_status = self.genome[schedule_index]
            
            if self.availabilities[key] != schedule_status:
                print(schedule_status, schedule_index, self.availabilities[key], key[0], key[1], key[2])
                availability_violated_count += 1
        
        return f"{availability_violated_count}/{len(self.availabilities)}"

    def get_work_hour_violated(self):
        contract_minimum_work_hours_violated_cnt = 0
        contract_maximum_work_hours_violated_cnt = 0
        
        for employee_id in self.employee_list:
            
            for day in range(self.total_days):
                current_date = self.start_date + timedelta(days=day)

                try:
                    current_date_work_hours = self.employee_total_work_hour_by_day[(employee_id, current_date)]
                except KeyError:
                    current_date_work_hours = 0
                
                if current_date_work_hours < self.minimum_work_hour:
                    contract_minimum_work_hours_violated_cnt += 1
                
                if current_date_work_hours > self.maximum_work_hour:
                    contract_maximum_work_hours_violated_cnt += 1

        return f"{contract_minimum_work_hours_violated_cnt}/{self.total_employee * self.total_days}", \
                f"{contract_maximum_work_hours_violated_cnt}/{self.total_employee * self.total_days}"

    def get_minimum_employee_onsite_violated(self):
        details = ""
        minimum_employee_onsite_violated_cnt = 0
        for key in self.shift_employee_on_site:
            try:
                employee_on_site = len(self.shift_employee_on_site[key])
            except KeyError:
                employee_on_site = 0
            
            if employee_on_site < self.minimum_employee_onsite:
                minimum_employee_onsite_violated_cnt += 1
                details += "\n" + f"{key[1]} {key[0]}: {employee_on_site}/{self.minimum_employee_onsite}"

        return f"{minimum_employee_onsite_violated_cnt}/{len(self.shift_employee_on_site)}" + details

    def get_room_capacity_per_shift_violated(self):
        details = ""
        room_capacity_violated_cnt = 0
        for key in self.shift_employee_on_site:
            try:
                employee_on_site = len(self.shift_employee_on_site[key])
            except KeyError:
                employee_on_site = 0
        
            if employee_on_site > self.capacity:
                room_capacity_violated_cnt += 1
                details +=  "\n" + f"{key[1]} {key[0]}: {employee_on_site}/{self.capacity}"

        return f"{room_capacity_violated_cnt}/{len(self.shift_employee_on_site)}" + details

    def get_preference_violated(self):
        preference_violated_cnt = 0
        subsequent_preference_violated_cnt = 0
        
        for key in self.preferences:
            shift_id = key[1]
            shift_index = self.shift_dict[shift_id]['index']
            day_from_start_date = (key[2] - self.start_date).days
            employee_index = self.employee_list.index(key[0])
            schedule_index = (employee_index * len(self.shift_dict) * self.total_days) + shift_index + (len(self.shift_dict) * day_from_start_date)
            schedule_status = self.genome[schedule_index]

            if key[0] not in self.employee_is_previously_violated_flag:
                self.employee_is_previously_violated_flag[key[0]] = 0

            if self.preferences[key] != schedule_status and self.employee_is_previously_violated_flag[key[0]] == 1:
                subsequent_preference_violated_cnt += 1
            elif self.preferences[key] != schedule_status:
                preference_violated_cnt += 1
                self.employee_is_previously_violated_flag[key[0]] = 1
            else:
                self.employee_is_previously_violated_flag[key[0]] = 0
        
        return f"{preference_violated_cnt}/{len(self.preferences)}", f"{subsequent_preference_violated_cnt}/{len(self.preferences)}"

    def get_planning_duration_different_employee_ratio(self):
        planning_duration_employee_count = len(self.planning_duration_employee_on_site)
        return f"{planning_duration_employee_count}/{self.total_employee}"

    def get_shift_different_employee_ratio(self):
        different_employee_ratio_per_shift = "\n"
        for key, different_employee_set in self.shift_employee_on_site.items():
            shift_different_employee_count = len(different_employee_set)
            normalized_different_employee_count = shift_different_employee_count
            normalized_room_capacity = self.capacity
            different_employee_ratio_per_shift += f"{key[1]} {key[0]}: {normalized_different_employee_count}/{normalized_room_capacity}\n"
        
        return different_employee_ratio_per_shift