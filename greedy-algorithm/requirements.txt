click==8.0.4
greenlet==1.1.2
importlib-metadata==4.11.1
iso8601==1.0.2
mysql==0.0.3
mysqlclient==2.1.0
numpy==1.21.5
typing_extensions==4.1.1
zipp==3.7.0