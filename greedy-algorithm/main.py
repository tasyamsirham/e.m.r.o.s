import sys
import iso8601
import datetime
import time

from GlobalVariable import GlobalVariable
from Metrics import Metrics

start_date = iso8601.parse_date(sys.argv[1]).date()
end_date = iso8601.parse_date(sys.argv[2]).date()
global_variable = GlobalVariable(start_date, end_date, int(sys.argv[3]))
weight = ['availability', 'min_onsite', 'max_onsite', 'min_work_hour', 'preference', 'max_work_hour', 'end']

current_onsite = {}
current_work_hour = {}
result = []

def recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index):
    if weight[weight_index] == 'availability':
        availability = global_variable.availability_dict.get((employee_id, shift, day))
        if availability is not None:
            
            if availability != 2:
                current_work_hour[(employee_id, day)] += global_variable.shift_dict[shift]['total_hour']
            
            if availability == 0:
                current_onsite[(shift, start_date)] += 1
            
            return availability
        
        else:
            return recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
    
    elif weight[weight_index] == 'min_work_hour':
        if current_work_hour[(employee_id, day)] < global_variable.minimum_work_hour:
            schedule = recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
            if schedule == 2:
                current_work_hour[(employee_id, day)] += global_variable.shift_dict[shift]['total_hour']
                return 1
            else:
                return schedule
            
        else:
            schedule = recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
            return schedule
            
    elif weight[weight_index] == 'max_work_hour':
        if current_work_hour[(employee_id, day)] + global_variable.shift_dict[shift]['total_hour'] <= global_variable.maximum_work_hour:
            schedule = recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
            return schedule
            
        else:
            schedule = recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
            if schedule != 2:
                current_work_hour[(employee_id, day)] -= global_variable.shift_dict[shift]['total_hour']
                
                if schedule == 0:
                    current_onsite[(shift, start_date)] -= 1

                return 2
            
            else:
                return schedule
        
    elif weight[weight_index] == 'min_onsite':
        if current_onsite[(shift, start_date)] < global_variable.minimum_employee_onsite:
            current_onsite[(shift, start_date)] += 1
            current_work_hour[(employee_id, day)] += global_variable.shift_dict[shift]['total_hour']
            return 0
        else:
            schedule = recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
            return schedule
            
    elif weight[weight_index] == 'max_onsite':
        if current_onsite[(shift, start_date)] + 1 <= global_variable.capacity:
            schedule = recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
            return schedule
        else:
            schedule = recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
            if schedule == 0:
                current_onsite[(shift, start_date)] -= 1
                current_work_hour[(employee_id, day)] -= global_variable.shift_dict[shift]['total_hour']
                return 2
            else:
                return schedule
            
    elif weight[weight_index] == 'preference':
        preference = global_variable.preference_dict.get((employee_id, shift, day))
        if preference is not None:
            
            if preference != 2:
                current_work_hour[(employee_id, day)] += global_variable.shift_dict[shift]['total_hour']
            
            if preference == 0:
                current_onsite[(shift, start_date)] += 1
                
            return preference
        
        else:
            return recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, day, weight, weight_index+1)
        
    else:
        return 2

#  Get the fitness function for each individuals
start_time = time.time()
for employee_id in global_variable.employee_id_list:
    for day in range(global_variable.total_days):
        try:
            current_work_hour[(employee_id, start_date)] += 0
        except:
            current_work_hour[(employee_id, start_date)] = 0
        for shift in global_variable.shift_dict:
            try:
                current_onsite[(shift, start_date)] += 0
            except:
                current_onsite[(shift, start_date)] = 0
                
            result.append(recursive_greedy_weighted(current_onsite, current_work_hour, employee_id, shift, start_date, weight, 0))

print("--- %s seconds ---" % (time.time() - start_time))

         
metrics = Metrics(result, global_variable)
metrics.get_metrics()
