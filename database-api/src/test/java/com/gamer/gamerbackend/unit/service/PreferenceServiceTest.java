package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.entity.Preference;
import com.gamer.gamerbackend.repository.PreferenceRepository;
import com.gamer.gamerbackend.service.PreferenceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class PreferenceServiceTest {
    @InjectMocks
    private PreferenceServiceImpl preferenceService;

    @Mock
    private PreferenceRepository preferenceRepository;

    private Preference preference;
    private List<Preference> preferenceList;

    @BeforeEach
    public void init(){
        preference = new Preference();
        preference.setId(1);
        preference.setDepartmentId(1);
        preference.setEmployeeId(1);
        preference.setShiftId(1);
        preference.setDay(LocalDate.of(12,6,15));
        preference.setPreferenceStatus(1);

        preferenceList = new ArrayList<Preference>();
        preferenceList.add(preference);
    }

    @DisplayName("given preference id then return preference")
    @Test
    public void givenPreferenceId_thenReturnPreference(){
        Mockito.when(preferenceRepository.findPreferenceById(Mockito.anyInt()))
                .thenReturn(Mono.just(preference));

        StepVerifier.create(preferenceService.findPreferenceById(1))
                .consumeNextWith(response -> {
                    assertEquals(1, response.getPreferenceStatus());
                });

        Mockito.verify(preferenceRepository, Mockito.times(1)).findPreferenceById(1);
    }

    @DisplayName("given employee id then return list of preference")
    @Test
    public void givenEmployeeId_thenReturnPreferenceList(){
        Mockito.when(preferenceRepository.findByEmployeeId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(preferenceList));

        StepVerifier.create(preferenceService.findPreferenceByEmployeeId(1))
                .expectNext(preference)
                .verifyComplete();

        Mockito.verify(preferenceRepository, Mockito.times(1)).findByEmployeeId(1);
    }

    @DisplayName("given preference then return new prefence")
    @Test
    public void givenPreference_thenReturnNewPreference(){
        Preference newPreference = new Preference();
        newPreference.setId(2);
        newPreference.setDepartmentId(2);
        newPreference.setEmployeeId(2);
        newPreference.setShiftId(2);
        newPreference.setDay(LocalDate.of(12,5,12));
        newPreference.setPreferenceStatus(0);

        Mockito.when(preferenceRepository.findPreferenceById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(preferenceRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newPreference));

        StepVerifier.create(preferenceService.insertPreference(newPreference))
                .consumeNextWith(response -> {
                    assertEquals(newPreference.getPreferenceStatus(), response.getPreferenceStatus());
                })
                .verifyComplete();

        Mockito.verify(preferenceRepository, Mockito.times(1)).save(newPreference);
        Mockito.verify(preferenceRepository, Mockito.times(1)).findPreferenceById(2);
    }

    @DisplayName("given existed preference then return nothing")
    @Test
    public void givenExistedPreference_thenReturnNothing(){
        Preference newPreference = new Preference();
        newPreference.setId(1);
        newPreference.setDepartmentId(1);
        newPreference.setEmployeeId(1);
        newPreference.setShiftId(1);
        newPreference.setDay(LocalDate.of(12,6,15));
        newPreference.setPreferenceStatus(1);

        Mockito.when(preferenceRepository.findPreferenceById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(preferenceRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newPreference));

        StepVerifier.create(preferenceService.insertPreference(newPreference))
                .consumeNextWith(response -> preferenceService.insertPreference(preference))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given preference with updated value then return updated preference")
    @Test
    public void givenPreferenceWithUpdatedValue_thenReturnUpdatedPreference(){
        Preference newPreference = new Preference();
        newPreference.setId(1);
        newPreference.setDepartmentId(1);
        newPreference.setEmployeeId(1);
        newPreference.setShiftId(1);
        newPreference.setDay(LocalDate.of(12,6,15));
        newPreference.setPreferenceStatus(2);

        Mockito.when(preferenceRepository.findPreferenceById(Mockito.anyInt()))
                .thenReturn(Mono.just(preference));
        Mockito.when(preferenceRepository.save(Mockito.any()))
                .thenReturn(Mono.just(preference));

        StepVerifier.create(preferenceService.updatePreference(newPreference,preference.getId()))
                .consumeNextWith(response -> {
                    assertEquals(newPreference.getId(), response.getId());
                    assertEquals(newPreference.getPreferenceStatus(), response.getPreferenceStatus());
                })
                .verifyComplete();
    }


}
