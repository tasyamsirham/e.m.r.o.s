package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.dto.converter.DepartmentConverter;
import com.gamer.gamerbackend.dto.response.DepartmentResponse;
import com.gamer.gamerbackend.entity.Company;
import com.gamer.gamerbackend.entity.Department;
import com.gamer.gamerbackend.entity.Room;
import com.gamer.gamerbackend.repository.DepartmentRepository;
import com.gamer.gamerbackend.repository.RoomRepository;
import com.gamer.gamerbackend.service.RoomServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class RoomServiceTest {
    @InjectMocks
    private RoomServiceImpl roomService;

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private DepartmentRepository departmentRepository;

    private Room room;
    private Room roomTwo;
    private Room roomRequest;
    private Department department;
    private Department departmentTwo;
    private DepartmentResponse departmentResponse;
    private Company company;
    private List<Department> departmentList;
    private List<DepartmentResponse> departmentResponseList;

    @BeforeEach
    public void init(){
        department = new Department();
        departmentList = new ArrayList<Department>();
        departmentList.add(department);

        room = new Room();
        room.setId(1);
        room.setCompanyId(1);
        room.setName("Room One");
        room.setCapacity(15);
        room.setDepartment(department);

        List<Room> roomList = new ArrayList<Room>();
        roomList.add(room);

        roomRequest = new Room();
        roomRequest.setId(1);
        roomRequest.setCompanyId(1);
        roomRequest.setName("Room Two");
        roomRequest.setCapacity(16);
        roomRequest.setDepartment(department);

        departmentResponse = new DepartmentResponse();
        departmentResponseList = new ArrayList<DepartmentResponse>();
        departmentResponseList.add(departmentResponse);

        company = new Company();
        company.setId(1);
        company.setName("Company A");
        company.setRooms(roomList);
        company.setDepartments(departmentResponseList);
    }

    @DisplayName("given room id then return room with given id")
    @Test
    public void givenRoomId_thenReturnRoomWithGivenId(){
        Mockito.when(roomRepository.findRoomById(Mockito.anyInt()))
                .thenReturn(Mono.just(room));

        StepVerifier.create(roomService.findRoomById(1))
                .consumeNextWith(response ->{
                    assertEquals("Room One", response.getName());
                });

        Mockito.verify(roomRepository, Mockito.times(1)).findRoomById(1);
    }

    @DisplayName("given false room id then return no room")
    @Test
    public void givenFalseRoomId_thenReturnNoRoom(){
        Mockito.when(roomRepository.findRoomById(Mockito.anyInt()))
                .thenReturn(Mono.just(room));

        StepVerifier.create(roomService.findRoomById(12))
                .verifyError();

        Mockito.verify(roomRepository, Mockito.times(1)).findRoomById(12);
    }

    @DisplayName("given request then return room list")
    @Test
    public void givenRequest_thenReturnRoomList(){
        Mockito.when(roomRepository.findByCompanyId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(company.getRooms()));

        Mockito.when(departmentRepository.findDepartmentByRoomId(room.getId()))
                .thenReturn(Mono.just(room.getDepartment()));

        StepVerifier.create(roomService.findAllRoomByCompanyId(1))
                .expectNext(room)
                .verifyComplete();

        Mockito.verify(roomRepository, Mockito.times(1)).findByCompanyId(1);
    }

    @DisplayName("given invalid request then return empty room")
    @Test
    public void givenInvalidRequest_thenReturnEmptyRoom(){
        Mockito.when(roomRepository.findByCompanyId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(company.getRooms()));

        StepVerifier.create(roomService.findAllRoomByCompanyId(100))
                .verifyError();

        Mockito.verify(roomRepository, Mockito.times(1)).findByCompanyId(100);
    }
}
