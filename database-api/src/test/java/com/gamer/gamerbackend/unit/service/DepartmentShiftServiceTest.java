package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.entity.DepartmentShift;
import com.gamer.gamerbackend.repository.DepartmentShiftRepository;
import com.gamer.gamerbackend.service.DepartmentShiftServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class DepartmentShiftServiceTest {
    @InjectMocks
    private DepartmentShiftServiceImpl departmentShiftService;

    @Mock
    private DepartmentShiftRepository departmentShiftRepository;

    private DepartmentShift departmentShift;
    private List<DepartmentShift> departmentShiftList;

    @BeforeEach
    public void init(){
        departmentShift = new DepartmentShift();
        departmentShift.setId(1);
        departmentShift.setShiftId(1);
        departmentShift.setDepartmentId(1);

        departmentShiftList = new ArrayList<DepartmentShift>();
        departmentShiftList.add(departmentShift);
    }

    @DisplayName("given departmentshift id then return department shift")
    @Test
    public void givenDepartmentshiftId_thenReturnDepartmentshift(){
        Mockito.when(departmentShiftRepository.findDepartmentShiftById(Mockito.anyInt()))
                .thenReturn(Mono.just(departmentShift));

        StepVerifier.create(departmentShiftService.findDepartmentShiftById(1))
                .consumeNextWith(response -> {
                    assertEquals(1, response.getDepartmentId());
                });

        Mockito.verify(departmentShiftRepository, Mockito.times(1)).findDepartmentShiftById(1);
    }

    @DisplayName("given department id then return list of departmentshift")
    @Test
    public void givenDepartmentId_thenReturnListOfDepartmentshift(){
        Mockito.when(departmentShiftRepository.findDepartmentShiftByDepartmentId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(departmentShiftList));

        StepVerifier.create(departmentShiftService.findDepartmentShiftByDepartmentId(1))
                .expectNext(departmentShift)
                .verifyComplete();

        Mockito.verify(departmentShiftRepository, Mockito.times(1)).findDepartmentShiftByDepartmentId(1);
    }

    @DisplayName("given shift id then return list of departmentshift")
    @Test
    public void givenShiftId_thenReturnListOfDepartmentshift(){
        Mockito.when(departmentShiftRepository.findDepartmentShiftByShiftId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(departmentShiftList));

        StepVerifier.create(departmentShiftService.findDepartmentShiftByShiftId(1))
                .expectNext(departmentShift)
                .verifyComplete();

        Mockito.verify(departmentShiftRepository, Mockito.times(1)).findDepartmentShiftByShiftId(1);
    }

    @DisplayName("given departmentshift then return new departmentshift")
    @Test
    public void givenDepartmentshift_thenReturnNewDepartmentshift(){
        DepartmentShift newDepartmentShift = new DepartmentShift();
        newDepartmentShift.setId(2);
        newDepartmentShift.setShiftId(2);
        newDepartmentShift.setDepartmentId(2);

        Mockito.when(departmentShiftRepository.findDepartmentShiftById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(departmentShiftRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newDepartmentShift));

        StepVerifier.create(departmentShiftService.insertDepartmentShift(newDepartmentShift))
                .consumeNextWith(response -> {
                    assertEquals(newDepartmentShift.getDepartmentId(), response.getDepartmentId());
                })
                .verifyComplete();

        Mockito.verify(departmentShiftRepository, Mockito.times(1)).save(newDepartmentShift);
        Mockito.verify(departmentShiftRepository, Mockito.times(1)).findDepartmentShiftById(newDepartmentShift.getId());
    }

    @DisplayName("given existed departmentshift then return nothing")
    @Test
    public void givenExistedDepartmentshift_thenReturnNothing(){
        DepartmentShift newDepartmentShift = new DepartmentShift();
        newDepartmentShift.setId(1);
        newDepartmentShift.setShiftId(1);
        newDepartmentShift.setDepartmentId(1);

        Mockito.when(departmentShiftRepository.findDepartmentShiftById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(departmentShiftRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newDepartmentShift));

        StepVerifier.create(departmentShiftService.insertDepartmentShift(newDepartmentShift))
                .consumeNextWith(response -> departmentShiftService.insertDepartmentShift(response))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given departmentshift with updated value then return updated departmentshift")
    @Test
    public void givenDepartmentshiftWithUpdatedValue_thenReturnUpdatedDepartmentshift(){
        DepartmentShift newDepartmentShift = new DepartmentShift();
        newDepartmentShift.setId(1);
        newDepartmentShift.setShiftId(1);
        newDepartmentShift.setDepartmentId(2);

        Mockito.when(departmentShiftRepository.findDepartmentShiftById(Mockito.anyInt()))
                .thenReturn(Mono.just(departmentShift));
        Mockito.when(departmentShiftRepository.save(Mockito.any()))
                .thenReturn(Mono.just(departmentShift));

        StepVerifier.create(departmentShiftService.updateDepartmentShift(newDepartmentShift, newDepartmentShift.getDepartmentId()))
                .consumeNextWith(response -> {
                    assertEquals(newDepartmentShift.getId(), response.getId());
                    assertEquals(newDepartmentShift.getShiftId(), response.getShiftId());
                })
                .verifyComplete();
    }
}
