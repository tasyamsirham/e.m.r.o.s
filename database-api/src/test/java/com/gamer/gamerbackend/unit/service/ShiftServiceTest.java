package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.entity.Department;
import com.gamer.gamerbackend.entity.Employee;
import com.gamer.gamerbackend.entity.Shift;
import com.gamer.gamerbackend.repository.ShiftRepository;
import com.gamer.gamerbackend.service.ShiftServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class ShiftServiceTest {
    @InjectMocks
    private ShiftServiceImpl shiftService;

    @Mock
    private ShiftRepository shiftRepository;

    private Department department;
    private Shift shift;
    private Employee employee;
    private List<Shift> shifts;

    @BeforeEach
    public void init(){
        shift = new Shift();
        shift.setId(1);
        shift.setName("Pagi");
        shift.setStartHour(12);
        shift.setEndHour(18);
        shift.setStartMinute(0);
        shift.setEndMinute(0);

        shifts = new ArrayList<Shift>();
        shifts.add(shift);

        employee = new Employee();
        List<Employee> employees = new ArrayList<Employee>();
        employees.add(employee);

//        department = new Department();
//        department.setId(1);
//        department.setName("Department One");
//        department.setCompanyId(1);
//        department.setRoomId(1);
//        department.setEmployees(employees);
//        department.setShifts(shifts);

    }

    @DisplayName("given shift id then return shift with given id")
    @Test
    public void givenShiftId_thenReturnShiftWithShiftId(){
        Mockito.when(shiftRepository.findShiftById(Mockito.anyInt()))
                .thenReturn(Mono.just(shift));

        StepVerifier.create(shiftService.findShiftById(1))
                .consumeNextWith(response ->{
                    assertEquals("Pagi", response.getName());
                });

        Mockito.verify(shiftRepository, Mockito.times(1)).findShiftById(1);
    }

    @DisplayName("given invalid shift id then return empty shift")
    @Test
    public void givenInvalidShiftId_thenReturnEmptyShift(){
        Mockito.when(shiftRepository.findShiftById(Mockito.anyInt()))
                .thenReturn(Mono.error(Exception::new));

        StepVerifier.create(shiftService.findShiftById(100))
                .verifyError();

        Mockito.verify(shiftRepository, Mockito.times(1)).findShiftById(100);
    }

    @DisplayName("given department id then return list of shifts")
    @Test
    public void givenDepartmentId_thenReturnShifts(){
        Mockito.when(shiftRepository.findAll())
                .thenReturn(Flux.fromIterable(shifts));

        StepVerifier.create(shiftService.findAllShift())
                .expectNext(shift)
                .verifyComplete();

        Mockito.verify(shiftRepository, Mockito.times(1)).findAll();
    }

    @DisplayName("given request all shift then return no shift")
    @Test
    public void givenRequestAllShift_thenReturnNoShift(){
        Mockito.when(shiftRepository.findAll())
                .thenReturn(Flux.error(Exception::new));

        StepVerifier.create(shiftService.findAllShift())
                .verifyError();

        Mockito.verify(shiftRepository, Mockito.times(1)).findAll();
    }

    @DisplayName("given shift then return new shift")
    @Test
    public void givenShift_thenReturnNewShift(){
        Shift newShift = new Shift();
        newShift.setId(2);
        newShift.setName("Siang");
        newShift.setStartHour(14);
        newShift.setEndHour(20);
        newShift.setStartMinute(0);
        newShift.setEndMinute(0);

        Mockito.when(shiftRepository.findShiftById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(shiftRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newShift));

        StepVerifier.create(shiftService.insertShift(newShift))
                .consumeNextWith(response -> {
                    assertEquals(newShift.getName(), response.getName());
                })
                .verifyComplete();

        Mockito.verify(shiftRepository, Mockito.times(1)).save(newShift);
        Mockito.verify(shiftRepository, Mockito.times(1)).findShiftById(newShift.getId());
    }

    @DisplayName("given existed shift then return nothing")
    @Test
    public void givenExistedShift_thenReturnNothing(){
        Shift newShift = new Shift();
        newShift.setId(1);
        newShift.setName("Pagi");
        newShift.setStartHour(12);
        newShift.setEndHour(18);
        newShift.setStartMinute(0);
        newShift.setEndMinute(0);

        Mockito.when(shiftRepository.findShiftById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(shiftRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newShift));

        StepVerifier.create(shiftService.insertShift(newShift))
                .consumeNextWith(response -> shiftService.insertShift(response))
                .expectNextCount(0)
                .verifyComplete();

    }

    @DisplayName("given shift with updated value then return updated shift")
    @Test
    public void givenShiftWithUpdatedValue_thenReturnUpdatedShift(){
        Shift newShift = new Shift();
        newShift.setId(1);
        newShift.setName("Siang");
        newShift.setStartHour(12);
        newShift.setEndHour(18);
        newShift.setStartMinute(0);
        newShift.setEndMinute(0);

        Mockito.when(shiftRepository.findShiftById(Mockito.anyInt()))
                .thenReturn(Mono.just(shift));
        Mockito.when(shiftRepository.save(Mockito.any()))
                .thenReturn(Mono.just(shift));

        StepVerifier.create(shiftService.updateShift(newShift, newShift.getId()))
                .consumeNextWith(response -> {
                    assertEquals(newShift.getId(), response.getId());
                    assertEquals(newShift.getName(), response.getName());
                })
                .verifyComplete();
    }
}
