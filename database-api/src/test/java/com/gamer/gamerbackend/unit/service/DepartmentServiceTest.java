package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.dto.converter.DepartmentConverter;
import com.gamer.gamerbackend.dto.response.DepartmentResponse;
import com.gamer.gamerbackend.entity.*;
import com.gamer.gamerbackend.repository.DepartmentRepository;
import com.gamer.gamerbackend.repository.EmployeeRepository;
import com.gamer.gamerbackend.repository.ShiftRepository;
import com.gamer.gamerbackend.service.DepartmentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class DepartmentServiceTest {
    @InjectMocks
    private DepartmentServiceImpl departmentService;

    @Mock
    private DepartmentRepository departmentRepository;

//    @Mock
//    private EmployeeRepository employeeRepository;

    private Department department;
    private Employee employee;
    private Shift shift;
    private Room room;
    private Company company;
    private DepartmentResponse departmentResponse;
    private List<Department> departmentList;
    private List<DepartmentResponse> departmentResponseList;

    @BeforeEach
    public void init(){
        employee = new Employee();
        employee.setId(1);
        employee.setFirstName("James");

        List<Employee> employees = new ArrayList<Employee>();
        employees.add(employee);

        List<Shift> shifts = new ArrayList<Shift>();
        shifts.add(shift);

        department = new Department();
        department.setId(1);
        department.setName("Department One");
        department.setCompanyId(1);
        department.setContractId(1);
        department.setRoomId(1);
        department.setMinimumEmployeeOnsite(15);
//        department.setEmployees(employees);

        departmentList = new ArrayList<Department>();
        departmentList.add(department);

        room = new Room();
        room.setId(1);
        room.setCompanyId(1);
        room.setName("Room One");
        room.setCapacity(15);
        room.setDepartment(department);

        List<Room> roomList = new ArrayList<Room>();
        roomList.add(room);

        departmentResponse = new DepartmentResponse();
        departmentResponseList = new ArrayList<DepartmentResponse>();
        departmentResponseList.add(departmentResponse);

        company = new Company();
        company.setId(1);
        company.setName("Company A");
        company.setRooms(roomList);
        company.setDepartments(departmentResponseList);

    }

    @DisplayName("given department id then return department with the given department id")
    @Test
    public void givenDepartmentId_thenReturnDepartmentWithDepartmentId(){
        Mockito.when(departmentRepository.findDepartmentById(Mockito.anyInt()))
                .thenReturn(Mono.just(department));

        StepVerifier.create(departmentService.findDepartmentById(1))
                .consumeNextWith(response ->{
                    assertEquals("Department One", response.getName());
                });

        Mockito.verify(departmentRepository, Mockito.times(1)).findDepartmentById(1);
    }

    @DisplayName("given false company id then return no department")
    @Test
    public void givenFalseCompanyId_thenReturnNoDepartment(){
        Mockito.when(departmentRepository.findDepartmentById(Mockito.anyInt()))
                .thenReturn(Mono.empty());

        StepVerifier.create(departmentService.findDepartmentById(100))
                .verifyComplete();

        Mockito.verify(departmentRepository, Mockito.times(1)).findDepartmentById(100);

    }

    @DisplayName("given company id then return list of departments")
    @Test
    public void givenCompanyId_thenReturnDepartmentList(){
        Mockito.when(departmentRepository.findByCompanyId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(departmentList));

        StepVerifier.create(departmentService.findDepartmentsByCompanyId(1))
                .expectNext(department)
                .verifyComplete();

        Mockito.verify(departmentRepository, Mockito.times(1)).findByCompanyId(1);
    }

    @DisplayName("given invalid company id then return empty department")
    @Test
    public void givenInvalidCompany_thenReturnEmptyDepartment(){
        Mockito.when(departmentRepository.findByCompanyId(Mockito.anyInt()))
                .thenReturn(Flux.empty());

        StepVerifier.create(departmentService.findDepartmentsByCompanyId(100))
                .verifyComplete();

        Mockito.verify(departmentRepository, Mockito.times(1)).findByCompanyId(100);


    }

    @DisplayName("given department then return new department")
    @Test
    public void givenDepartment_thenReturnInsertedDepartment(){
        Department newDepartment = new Department();
        newDepartment.setId(2);
        newDepartment.setName("Department Two");
        newDepartment.setCompanyId(2);
        newDepartment.setContractId(2);
        newDepartment.setRoomId(2);
        newDepartment.setMinimumEmployeeOnsite(12);

        Mockito.when(departmentRepository.findDepartmentById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(departmentRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newDepartment));

        StepVerifier.create(departmentService.insertDepartment(newDepartment))
                .consumeNextWith(response -> {
                    assertEquals(newDepartment.getName(),  response.getName());
                })
                .verifyComplete();

        Mockito.verify(departmentRepository, Mockito.times(1)).save(newDepartment);
        Mockito.verify(departmentRepository, Mockito.times(1)).findDepartmentById(newDepartment.getId());

    }

    @DisplayName("given existed department then return nothing")
    @Test
    public void givenExistedDepartment_thenReturnNothing(){
        Department newDepartment = new Department();
        newDepartment.setId(1);
        newDepartment.setName("Department One");
        newDepartment.setCompanyId(1);
        newDepartment.setContractId(1);
        newDepartment.setRoomId(1);
        newDepartment.setMinimumEmployeeOnsite(15);

        Mockito.when(departmentRepository.findDepartmentById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(departmentRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newDepartment));

        StepVerifier.create(departmentService.insertDepartment(newDepartment))
                .consumeNextWith(response -> departmentService.insertDepartment(response))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given department with updated value then return updated department")
    @Test
    public void givenDepartmentWithUpdatedValue_thenReturnUpdatedDepartment(){
        Department newDepartment = new Department();
        newDepartment.setId(1);
        newDepartment.setName("Department One NEW");
        newDepartment.setCompanyId(1);
        newDepartment.setContractId(1);
        newDepartment.setRoomId(1);
        newDepartment.setMinimumEmployeeOnsite(15);

        Mockito.when(departmentRepository.findDepartmentById(Mockito.anyInt()))
                .thenReturn(Mono.just(department));
        Mockito.when(departmentRepository.save(Mockito.any()))
                .thenReturn(Mono.just(department));

        StepVerifier.create(departmentService.updateDepartment(newDepartment, department.getId()))
                .consumeNextWith(response -> {
                    assertEquals(newDepartment.getId(), response.getId());
                    assertEquals(newDepartment.getName(), response.getName());
                })
                .verifyComplete();
    }
}

