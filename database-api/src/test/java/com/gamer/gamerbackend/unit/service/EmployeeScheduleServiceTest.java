package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.entity.Availability;
import com.gamer.gamerbackend.entity.Employee;
import com.gamer.gamerbackend.entity.EmployeeSchedule;
import com.gamer.gamerbackend.entity.Preference;
import com.gamer.gamerbackend.repository.EmployeeScheduleRepository;
import com.gamer.gamerbackend.service.EmployeeScheduleServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class EmployeeScheduleServiceTest {
    @InjectMocks
    private EmployeeScheduleServiceImpl employeeScheduleService;

    @Mock
    private EmployeeScheduleRepository employeeScheduleRepository;

    private Employee employee;
    private EmployeeSchedule employeeSchedule;
    private Availability availability;
    private Preference preference;

    @BeforeEach
    public void init(){
        employeeSchedule = new EmployeeSchedule();
        employeeSchedule.setId(1);
        employeeSchedule.setEmployeeId(1);
        employeeSchedule.setShiftId(1);
        employeeSchedule.setDay(LocalDate.of(2022,04,28));
        employeeSchedule.setScheduleStatus(1);

        List<EmployeeSchedule> employeeSchedules= new ArrayList<EmployeeSchedule>();
        employeeSchedules.add(employeeSchedule);

        availability = new Availability();
        preference = new Preference();

        List<Availability> availabilities = new ArrayList<Availability>();
        availabilities.add(availability);

        List<Preference> preferences = new ArrayList<Preference>();
        preferences.add(preference);

        employee = new Employee();
        employee.setId(1);
        employee.setDepartmentId(1);
        employee.setCompanyId(1);
        employee.setEmail("guy@gmail.com");
        employee.setUsername("guy123");
        employee.setPassword("password");
        employee.setFirstName("Zonny");
        employee.setLastName("Davius");
        employee.setRole("Employee");
        employee.setAvailabilities(availabilities);
        employee.setPreferences(preferences);
        employee.setEmployeeSchedules(employeeSchedules);
    }

    @DisplayName("given employee schedule id then return employee schedule with given id")
    @Test
    public void givenEmployeeScheduleId_thenReturnEmployeescheduleWithEmployeeScheduleId(){
        Mockito.when(employeeScheduleRepository.findEmployeeScheduleById(Mockito.anyInt()))
                .thenReturn(Mono.just(employeeSchedule));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleById(1))
                .consumeNextWith(response ->{
                    assertEquals(1,response.getScheduleStatus());
                });

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findEmployeeScheduleById(1);
    }

    @DisplayName("given invalid id then return empty employee schedule")
    @Test
    public void givenInvalidId_thenReturnEmptyEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findEmployeeScheduleById(Mockito.anyInt()))
                .thenReturn(Mono.error(Exception::new));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleById(100))
                .verifyError();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findEmployeeScheduleById(100);
    }

    @DisplayName("given employee id then return list of employee schedule")
    @Test
    public void givenEmployeeId_thenReturnListOfEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByEmployeeId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(employee.getEmployeeSchedules()));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByEmployeeId(employee.getId()))
                .expectNext(employeeSchedule)
                .verifyComplete();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByEmployeeId(employee.getId());
    }

    @DisplayName("given invalid employee id then return empty employee schedule")
    @Test
    public void givenInvalidEmployeeId_thenReturnEmptyEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByEmployeeId(Mockito.anyInt()))
                .thenReturn(Flux.error(Exception::new));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByEmployeeId(100))
                .verifyError();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByEmployeeId(100);
    }

    @DisplayName("given employee id and day then return list of employee schedule")
    @Test
    public void givenEmployeeIdAndDay_thenReturnListOfEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByEmployeeIdAndDay(Mockito.anyInt(), Mockito.any()))
                .thenReturn(Flux.fromIterable(employee.getEmployeeSchedules()));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByEmployeeIdAndDay(employee.getId(), employeeSchedule.getDay()))
                .expectNext(employeeSchedule)
                .verifyComplete();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByEmployeeIdAndDay(employee.getId(), employeeSchedule.getDay());
    }

    @DisplayName("given invalid employee id and day then return empty employee schedule")
    @Test
    public void givenInvalidEmployeeIdAndDay_thenReturnEmptyEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByEmployeeIdAndDay(Mockito.anyInt(), Mockito.any()))
                .thenReturn(Flux.error(Exception::new));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByEmployeeIdAndDay(100, LocalDate.of(2021,02,01)))
                .verifyError();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByEmployeeIdAndDay(100, LocalDate.of(2021,02,01));
    }

    @DisplayName("given employee id and between two days then return list of employee schedule")
    @Test
    public void givenEmployeeIdAndBetweenTwoDays_thenReturnListOfEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByEmployeeIdAndDayBetween(Mockito.anyInt(), Mockito.any(), Mockito.any()))
                .thenReturn(Flux.fromIterable(employee.getEmployeeSchedules()));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByEmployeeAndBetweenDays(employee.getId(), employeeSchedule.getDay().toString(), employeeSchedule.getDay().toString()))
                .expectNext(employeeSchedule)
                .verifyComplete();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByEmployeeIdAndDayBetween(employee.getId(), employeeSchedule.getDay().toString(), employeeSchedule.getDay().toString());
    }

    @DisplayName("given invalid employee id and between two days then return empty employee schedule")
    @Test
    public void givenInvalidEmployeeIdAndBetweenTwoDays_thenReturnEmptyEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByEmployeeIdAndDayBetween(Mockito.anyInt(), Mockito.any(), Mockito.any()))
                .thenReturn(Flux.error(Exception::new));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByEmployeeAndBetweenDays(100, "2020-02-02", "2020-02-03"))
                .verifyError();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByEmployeeIdAndDayBetween(100, "2020-02-02", "2020-02-03");
    }

    @DisplayName("given two days then return list of employee schedule")
    @Test
    public void givenTwoDays_thenReturnListOfEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByDayBetween(Mockito.any(), Mockito.any()))
                .thenReturn(Flux.fromIterable(employee.getEmployeeSchedules()));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByBetweenDays(employeeSchedule.getDay().toString(), employeeSchedule.getDay().toString()))
                .expectNext(employeeSchedule)
                .verifyComplete();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByDayBetween(employeeSchedule.getDay().toString(), employeeSchedule.getDay().toString());
    }

    @DisplayName("given invalid two days then return empty employee schedule")
    @Test
    public void givenInvalidTwoDays_thenReturnEmptyEmployeeSchedule(){
        Mockito.when(employeeScheduleRepository.findByDayBetween(Mockito.any(), Mockito.any()))
                .thenReturn(Flux.error(Exception::new));

        StepVerifier.create(employeeScheduleService.findEmployeeScheduleByBetweenDays("2020-02-02", "2020-01-02"))
                .verifyError();

        Mockito.verify(employeeScheduleRepository, Mockito.times(1)).findByDayBetween("2020-02-02", "2020-01-02");
    }
}
