package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.entity.*;
import com.gamer.gamerbackend.repository.AvailabilityRepository;
import com.gamer.gamerbackend.repository.EmployeeRepository;
import com.gamer.gamerbackend.repository.EmployeeScheduleRepository;
import com.gamer.gamerbackend.repository.PreferenceRepository;
import com.gamer.gamerbackend.service.EmployeeServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class EmployeeServiceTest {
    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private AvailabilityRepository availabilityRepository;

    @Mock
    private PreferenceRepository preferenceRepository;

    @Mock
    private EmployeeScheduleRepository employeeScheduleRepository;

    private Department department;
    private Employee employee;
    private Shift shift;
    private Availability availability;
    private Preference preference;
    private EmployeeSchedule employeeSchedule;
    private List<Employee> employees;
    private List<Availability> availabilities;
    private List<Preference> preferences;
    List<EmployeeSchedule> employeeSchedules;

    @BeforeEach
    public void init(){
        availability = new Availability();
        preference = new Preference();
        employeeSchedule = new EmployeeSchedule();

        availabilities = new ArrayList<Availability>();
        availabilities.add(availability);

        preferences = new ArrayList<Preference>();
        preferences.add(preference);

        employeeSchedules= new ArrayList<EmployeeSchedule>();
        employeeSchedules.add(employeeSchedule);

        employee = new Employee();
        employee.setId(1);
        employee.setDepartmentId(1);
        employee.setCompanyId(1);
        employee.setEmail("guy@gmail.com");
        employee.setUsername("guy123");
        employee.setPassword("password");
        employee.setFirstName("Zonny");
        employee.setLastName("Davius");
        employee.setRole("Employee");
        employee.setAvailabilities(availabilities);
        employee.setPreferences(preferences);
        employee.setEmployeeSchedules(employeeSchedules);

        employees = new ArrayList<Employee>();
        employees.add(employee);

        department = new Department();
        department.setId(1);
        department.setName("Department One");
        department.setCompanyId(1);
        department.setRoomId(1);
//        department.setEmployees(employees);

    }

    @DisplayName("given employee id then return employee with given id")
    @Test
    public void givenEmployeeId_thenReturnEmployeeWithEmployeeId(){
        Mockito.when(employeeRepository.findEmployeeById(Mockito.anyInt()))
                .thenReturn(Mono.just(employee));

        StepVerifier.create(employeeService.findEmployeeById(1))
                .consumeNextWith(response ->{
                    assertEquals("Zonny", response.getFirstName());
                });

        Mockito.verify(employeeRepository, Mockito.times(1)).findEmployeeById(1);
    }

    @DisplayName("given invalid employee id then return no employee")
    @Test
    public void givenInvalidEmployee_thenReturnNoEmployee(){
        Mockito.when(employeeRepository.findEmployeeById(Mockito.anyInt()))
                .thenReturn(Mono.just(employee));

        StepVerifier.create(employeeService.findEmployeeById(100))
                 .verifyError();

        Mockito.verify(employeeRepository, Mockito.times(1)).findEmployeeById(100);
    }

    @DisplayName("given department id then return list of employees")
    @Test
    public void givenDepartmentId_thenReturnEmployees(){
        Mockito.when(employeeRepository.findByDepartmentId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(employees));

        Mockito.when(availabilityRepository.findByEmployeeId(employee.getId()))
                .thenReturn(Flux.fromIterable(employee.getAvailabilities()));

        Mockito.when(preferenceRepository.findByEmployeeId(employee.getId()))
                .thenReturn(Flux.fromIterable(employee.getPreferences()));

        Mockito.when(employeeScheduleRepository.findByEmployeeId(employee.getId()))
                .thenReturn(Flux.fromIterable(employee.getEmployeeSchedules()));

        StepVerifier.create(employeeService.findAllEmployeeByDepartmentId(employee.getDepartmentId()))
                .expectNext(employee)
                .verifyComplete();

        Mockito.verify(employeeRepository, Mockito.times(1)).findByDepartmentId(1);
    }

    @DisplayName("given invalid department id then return empty employee")
    @Test
    public void givenInvalidDepartmentId_thenReturnEmptyEmployee(){
        Mockito.when(employeeRepository.findByDepartmentId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(employees));

        StepVerifier.create(employeeService.findAllEmployeeByDepartmentId(100))
                        .verifyError();

        Mockito.verify(employeeRepository, Mockito.times(1)).findByDepartmentId(100);
    }

    @DisplayName("given invalid employee then return new employee")
    @Test
    public void givenInvalidEmployee_thenReturnNewEmployee(){
        Employee newEmployee = new Employee();
        newEmployee.setId(2);
        newEmployee.setDepartmentId(2);
        newEmployee.setCompanyId(2);
        newEmployee.setEmail("jay@gmail.com");
        newEmployee.setUsername("jay123");
        newEmployee.setPassword("password");
        newEmployee.setFirstName("Luca");
        newEmployee.setLastName("Raly");
        newEmployee.setRole("Employee");
        newEmployee.setAvailabilities(availabilities);
        newEmployee.setPreferences(preferences);
        newEmployee.setEmployeeSchedules(employeeSchedules);

        Mockito.when(employeeRepository.findEmployeeById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(employeeRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newEmployee));

        StepVerifier.create(employeeService.insertEmployee(newEmployee))
                .consumeNextWith(response -> {
                    assertEquals(newEmployee.getFirstName(), response.getFirstName());
                })
                .verifyComplete();

        Mockito.verify(employeeRepository, Mockito.times(1)).save(newEmployee);
        Mockito.verify(employeeRepository, Mockito.times(1)).findEmployeeById(newEmployee.getId());
    }

    @DisplayName("given existed employee then return nothing")
    @Test
    public void givenExistedEmployee_thenReturnNothing(){
        Employee newEmployee = new Employee();
        newEmployee.setId(1);
        newEmployee.setDepartmentId(1);
        newEmployee.setCompanyId(1);
        newEmployee.setEmail("guy@gmail.com");
        newEmployee.setUsername("guy123");
        newEmployee.setPassword("password");
        newEmployee.setFirstName("Zonny");
        newEmployee.setLastName("Davius");
        newEmployee.setRole("Employee");
        newEmployee.setAvailabilities(availabilities);
        newEmployee.setPreferences(preferences);
        newEmployee.setEmployeeSchedules(employeeSchedules);

        Mockito.when(employeeRepository.findEmployeeById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(employeeRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newEmployee));

        StepVerifier.create(employeeService.insertEmployee(newEmployee))
                .consumeNextWith(response -> employeeService.insertEmployee(response))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given employee with updated value then return updated employee")
    @Test
    public void givenEmployeeWithUpdatedValue_thenReturnUpdatedEmployee(){
        Employee newEmployee = new Employee();
        newEmployee.setId(employee.getId());
        newEmployee.setDepartmentId(employee.getDepartmentId());
        newEmployee.setCompanyId(employee.getCompanyId());
        newEmployee.setEmail(employee.getEmail());
        newEmployee.setUsername(employee.getUsername());
        newEmployee.setPassword(employee.getPassword());
        newEmployee.setFirstName("Zehaha");
        newEmployee.setLastName("Davy");
        newEmployee.setRole(employee.getRole());
        newEmployee.setAvailabilities(employee.getAvailabilities());
        newEmployee.setPreferences(employee.getPreferences());
        newEmployee.setEmployeeSchedules(employee.getEmployeeSchedules());

        Mockito.when(employeeRepository.findEmployeeById(Mockito.anyInt()))
                .thenReturn(Mono.just(employee));
        Mockito.when(employeeRepository.save(Mockito.any()))
                .thenReturn(Mono.just(employee));

        StepVerifier.create(employeeService.updateEmployee(newEmployee, newEmployee.getId()))
                .consumeNextWith(response -> {
                    assertEquals(newEmployee.getId(), response.getId());
                    assertEquals(newEmployee.getFirstName(), response.getFirstName());
                })
                .verifyComplete();


    }

}
