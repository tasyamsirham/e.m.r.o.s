package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.entity.Availability;
import com.gamer.gamerbackend.repository.AvailabilityRepository;
import com.gamer.gamerbackend.service.AvailabilityServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class AvailabilityServiceTest {
    @InjectMocks
    private AvailabilityServiceImpl availabilityService;

    @Mock
    private AvailabilityRepository availabilityRepository;

    private Availability availability;
    private List<Availability> availabilityList;

    @BeforeEach
    public void init(){
        availability = new Availability();
        availability.setId(1);
        availability.setAvailabilityStatus(1);
        availability.setEmployeeId(1);
        availability.setShiftId(1);
        availability.setDay(LocalDate.of(2022,6,12));
        availability.setReason("wakwaw");

        availabilityList = new ArrayList<Availability>();
        availabilityList.add(availability);
    }

    @DisplayName("given availability id then return availability")
    @Test
    public void givenAvailabilityId_thenReturnAvailability(){
        Mockito.when(availabilityRepository.findAvailabilityById(Mockito.anyInt()))
                .thenReturn(Mono.just(availability));

        StepVerifier.create(availabilityService.findAvailabilityById(1))
                .consumeNextWith(response -> {
                    assertEquals("wakwaw", response.getReason());
                });

        Mockito.verify(availabilityRepository, Mockito.times(1)).findAvailabilityById(1);
    }

    @DisplayName("given employee id then return list of availability")
    @Test
    public void givenEmployeeId_thenReturnAvailabilityList(){
        Mockito.when(availabilityRepository.findByEmployeeId(Mockito.anyInt()))
                .thenReturn(Flux.fromIterable(availabilityList));

        StepVerifier.create(availabilityService.findAvailabilityByEmployeeId(1))
                .expectNext(availability)
                .verifyComplete();

        Mockito.verify(availabilityRepository, Mockito.times(1)).findByEmployeeId(1);
    }

    @DisplayName("given availability then return new availability")
    @Test
    public void givenAvailability_thenReturnInsertedAvailability(){
        Availability newAvailability = new Availability();
        newAvailability.setId(2);
        newAvailability.setAvailabilityStatus(2);
        newAvailability.setEmployeeId(2);
        newAvailability.setShiftId(2);
        newAvailability.setDay(LocalDate.of(2022,7,12));
        newAvailability.setReason("huehue");

        Mockito.when(availabilityRepository.findAvailabilityById(Mockito.anyInt()))
                .thenReturn(Mono.empty());

        Mockito.when(availabilityRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newAvailability));

        StepVerifier.create(availabilityService.insertAvailability(newAvailability))
                .consumeNextWith(response -> {
                    assertEquals(newAvailability.getAvailabilityStatus(), response.getAvailabilityStatus());
                })
                .verifyComplete();

        Mockito.verify(availabilityRepository, Mockito.times(1)).save(newAvailability);
        Mockito.verify(availabilityRepository, Mockito.times(1)).findAvailabilityById(newAvailability.getId());
    }

    @DisplayName("given existed availability then return nothing")
    @Test
    public void givenExistedAvailability_thenReturnNothing(){
        Availability newAvailability = new Availability();
        newAvailability.setId(1);
        newAvailability.setAvailabilityStatus(1);
        newAvailability.setEmployeeId(1);
        newAvailability.setShiftId(1);
        newAvailability.setDay(LocalDate.of(2022,6,12));
        newAvailability.setReason("wakwaw");

        Mockito.when(availabilityRepository.findAvailabilityById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(availabilityRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newAvailability));

        StepVerifier.create(availabilityService.insertAvailability(newAvailability))
                .consumeNextWith(response -> availabilityService.insertAvailability(response))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given availability with updated value then return updated availability")
    @Test
    public void givenAvailabilityWithUpdatedValue_thenReturnUpdatedAvailability(){
        Availability newAvailability = new Availability();
        newAvailability.setId(1);
        newAvailability.setAvailabilityStatus(1);
        newAvailability.setEmployeeId(1);
        newAvailability.setShiftId(1);
        newAvailability.setDay(LocalDate.of(2022,6,12));
        newAvailability.setReason("wakwaw NEW");

        Mockito.when(availabilityRepository.findAvailabilityById(Mockito.anyInt()))
                .thenReturn(Mono.just(availability));
        Mockito.when(availabilityRepository.save(Mockito.any()))
                .thenReturn(Mono.just(availability));

        StepVerifier.create(availabilityService.updateAvailability(newAvailability, newAvailability.getId()))
                .consumeNextWith(response -> {
                    assertEquals(newAvailability.getId(), response.getId());
                    assertEquals(newAvailability.getAvailabilityStatus(), response.getAvailabilityStatus());
                })
                .verifyComplete();
    }
}
