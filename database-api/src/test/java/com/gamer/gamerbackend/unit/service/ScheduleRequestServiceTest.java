package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.entity.ScheduleRequest;
import com.gamer.gamerbackend.repository.ScheduleRequestRepository;
import com.gamer.gamerbackend.service.ScheduleRequestServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class ScheduleRequestServiceTest {
    @InjectMocks
    private ScheduleRequestServiceImpl scheduleRequestService;

    @Mock
    private ScheduleRequestRepository scheduleRequestRepository;

    private ScheduleRequest scheduleRequest;
    private List<ScheduleRequest> scheduleRequestList;

    @BeforeEach
    public void init(){
        scheduleRequest = new ScheduleRequest();
        scheduleRequest.setId(1);
        scheduleRequest.setDepartmentId(1);
        scheduleRequest.setStartDate(LocalDate.of(2022,6,10));
        scheduleRequest.setEndDate(LocalDate.of(2022,6,15));
        scheduleRequest.setStatus("wow");
        scheduleRequest.setMinimumWorkHourWeight(0.1F);
        scheduleRequest.setMaximumWorkHourWeight(0.1F);
        scheduleRequest.setMinimumOnsiteWeight(0.1F);
        scheduleRequest.setRoomCapacityWeight(0.1F);
        scheduleRequest.setPreferenceWeight(0.1F);
        scheduleRequest.setDifferentEmployeePerShiftWeight(0.25F);
        scheduleRequest.setDifferentEmployeeTotalWeight(0.25F);

        scheduleRequestList = new ArrayList<ScheduleRequest>();
        scheduleRequestList.add(scheduleRequest);
    }

    @DisplayName("given schedulerequest id then return schedulerequest")
    @Test
    public void givenSchedulerequestId_thenReturnSchedulerequest(){
        Mockito.when(scheduleRequestRepository.findScheduleRequestById(Mockito.anyInt()))
                .thenReturn(Mono.just(scheduleRequest));

        StepVerifier.create(scheduleRequestService.findScheduleRequestById(1))
                .consumeNextWith(response -> {
                    assertEquals("wow", response.getStatus());
                });

        Mockito.verify(scheduleRequestRepository, Mockito.times(1)).findScheduleRequestById(1);
    }

    @DisplayName("given schedulerequest then return new schedulerequest")
    @Test
    public void givenScheduleRequest_thenReturnNewScheduleRequest(){
        ScheduleRequest newScheduleRequest = new ScheduleRequest();
        newScheduleRequest.setId(2);
        newScheduleRequest.setDepartmentId(2);
        newScheduleRequest.setStartDate(LocalDate.of(2022,6,11));
        newScheduleRequest.setEndDate(LocalDate.of(2022,6,16));
        newScheduleRequest.setStatus("wowow");
        newScheduleRequest.setMinimumWorkHourWeight(0.2F);
        newScheduleRequest.setMaximumWorkHourWeight(0.2F);
        newScheduleRequest.setMinimumOnsiteWeight(0.1F);
        newScheduleRequest.setRoomCapacityWeight(0.1F);
        newScheduleRequest.setPreferenceWeight(0.1F);
        newScheduleRequest.setDifferentEmployeePerShiftWeight(0.15F);
        newScheduleRequest.setDifferentEmployeeTotalWeight(0.15F);

        Mockito.when(scheduleRequestRepository.findScheduleRequestById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(scheduleRequestRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newScheduleRequest));

        StepVerifier.create(scheduleRequestService.insertScheduleRequest(newScheduleRequest))
                .consumeNextWith(response ->{
                    assertEquals(newScheduleRequest.getStatus(), response.getStatus());
                })
                .verifyComplete();

        Mockito.verify(scheduleRequestRepository, Mockito.times(1)).save(newScheduleRequest);
        Mockito.verify(scheduleRequestRepository, Mockito.times(1)).findScheduleRequestById(newScheduleRequest.getId());
    }

    @DisplayName("given schedulerequest already in database then return nothing")
    @Test
    public void givenExistedSchedulerequest_thenReturnNothing(){
        ScheduleRequest newScheduleRequest = new ScheduleRequest();
        newScheduleRequest.setId(1);
        newScheduleRequest.setDepartmentId(1);
        newScheduleRequest.setStartDate(LocalDate.of(2022,6,10));
        newScheduleRequest.setEndDate(LocalDate.of(2022,6,15));
        newScheduleRequest.setStatus("wow");
        newScheduleRequest.setMinimumWorkHourWeight(0.1F);
        newScheduleRequest.setMaximumWorkHourWeight(0.1F);
        newScheduleRequest.setMinimumOnsiteWeight(0.1F);
        newScheduleRequest.setRoomCapacityWeight(0.1F);
        newScheduleRequest.setPreferenceWeight(0.1F);
        newScheduleRequest.setDifferentEmployeePerShiftWeight(0.25F);
        newScheduleRequest.setDifferentEmployeeTotalWeight(0.25F);

        Mockito.when(scheduleRequestRepository.findScheduleRequestById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(scheduleRequestRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newScheduleRequest));

        StepVerifier.create(scheduleRequestService.insertScheduleRequest(newScheduleRequest))
                .consumeNextWith(response -> scheduleRequestService.insertScheduleRequest(newScheduleRequest))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given schedulerequest with updated value then return updated schedulerequest")
    @Test
    public void givenSchedulerRequestWithUpdatedValue_thenReturnUpdatedSchedule(){
        ScheduleRequest newScheduleRequest = new ScheduleRequest();
        newScheduleRequest.setId(1);
        newScheduleRequest.setDepartmentId(1);
        newScheduleRequest.setStartDate(LocalDate.of(2022,6,10));
        newScheduleRequest.setEndDate(LocalDate.of(2022,6,15));
        newScheduleRequest.setStatus("wow");
        newScheduleRequest.setMinimumWorkHourWeight(0.1F);
        newScheduleRequest.setMaximumWorkHourWeight(0.1F);
        newScheduleRequest.setMinimumOnsiteWeight(0.0F);
        newScheduleRequest.setRoomCapacityWeight(0.0F);
        newScheduleRequest.setPreferenceWeight(0.0F);
        newScheduleRequest.setDifferentEmployeePerShiftWeight(0.40F);
        newScheduleRequest.setDifferentEmployeeTotalWeight(0.40F);

        Mockito.when(scheduleRequestRepository.findScheduleRequestById(Mockito.anyInt()))
                .thenReturn(Mono.just(scheduleRequest));
        Mockito.when(scheduleRequestRepository.save(Mockito.any()))
                .thenReturn(Mono.just(scheduleRequest));

        StepVerifier.create(scheduleRequestService.updateScheduleRequest(newScheduleRequest, scheduleRequest.getId()))
                .consumeNextWith(response -> {
                    assertEquals(newScheduleRequest.getId(), response.getId());
                    assertEquals(newScheduleRequest.getDepartmentId(), response.getDepartmentId());
                })
                .verifyComplete();
    }
}
