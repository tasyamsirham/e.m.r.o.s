package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.dto.converter.DepartmentConverter;
import com.gamer.gamerbackend.dto.response.DepartmentResponse;
import com.gamer.gamerbackend.entity.Company;
import com.gamer.gamerbackend.entity.Department;
import com.gamer.gamerbackend.entity.Room;
import com.gamer.gamerbackend.repository.CompanyRepository;
import com.gamer.gamerbackend.service.CompanyService;
import com.gamer.gamerbackend.service.CompanyServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class CompanyServiceTest {
    @InjectMocks
    private CompanyServiceImpl companyService;

    @Mock
    private CompanyRepository companyRepository;

    private Company company;
    private Company requestCompany;
    private Room room;
    private DepartmentResponse departmentResponse;

    @BeforeEach
    public void init(){
        room = new Room();
        departmentResponse = new DepartmentResponse();

        List<Room> roomList = new ArrayList<Room>();
        roomList.add(room);

        List<DepartmentResponse> departmentList = new ArrayList<DepartmentResponse>();
        departmentList.add(departmentResponse);

        company = new Company();
        company.setId(1);
        company.setName("Company A");
        company.setRooms(roomList);
        company.setDepartments(departmentList);

        requestCompany = new Company();
        requestCompany.setId(1);
        requestCompany.setName("Updated Company A");
        requestCompany.setRooms(roomList);
        requestCompany.setDepartments(departmentList);
    }

    @DisplayName("given company id then return company with the given company id")
    @Test
    public void givenCompanyId_thenReturnCompanyWithCompanyId(){
        Mockito.when(companyRepository.findCompanyById(Mockito.anyInt()))
                .thenReturn(Mono.just(company));

        StepVerifier.create(companyService.findCompanyById(1))
                .consumeNextWith(response -> {
                    assertEquals("Company A",response.getName());
                });

        Mockito.verify(companyRepository, Mockito.times(1)).findCompanyById(1);
    }

    @DisplayName("given a false company id then return no company")
    @Test
    public void givenFalseCompanyId_thenReturnNoCompany(){
        Mockito.when(companyRepository.findCompanyById(Mockito.anyInt()))
                .thenReturn(Mono.just(company));

        StepVerifier.create(companyService.findCompanyById(100))
                .verifyError();

        Mockito.verify(companyRepository, Mockito.times(1)).findCompanyById(100);
    }

    @DisplayName("given company then return new company")
    @Test
    public void givenCompany_thenReturnInsertedCompany(){

        Company newCompany = new Company();
        newCompany.setName("New Company");
        newCompany.setId(2);

        Mockito.when(companyRepository.findCompanyById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(companyRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newCompany));

        StepVerifier.create(companyService.addCompany(newCompany))
                .consumeNextWith(response ->{
                    assertEquals(newCompany.getName(), response.getName());
                })
                .verifyComplete();

        Mockito.verify(companyRepository, Mockito.times(1)).save(newCompany);
        Mockito.verify(companyRepository, Mockito.times(1)).findCompanyById(newCompany.getId());
    }

    @DisplayName("given company already in database then return nothing")
    @Test
    public void givenExistedCompany_thenReturnNothing(){
        Company newCompany = new Company();
        newCompany.setName("Company A");
        newCompany.setId(1);

        Mockito.when(companyRepository.findCompanyById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(companyRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newCompany));

        StepVerifier.create(companyService.addCompany(newCompany))
                .consumeNextWith(response -> companyService.addCompany(response))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given company with updated value then return updated company")
    @Test
    public void givenCompanyWithUpdatedValue_thenReturnUpdatedCompany(){
        Company updatedCompany = new Company();
        updatedCompany.setId(company.getId());
        updatedCompany.setDepartments(company.getDepartments());
        updatedCompany.setRooms(company.getRooms());
        updatedCompany.setName("Updated Company A");

        Mockito.when(companyRepository.findCompanyById(Mockito.anyInt()))
                .thenReturn(Mono.just(company));
        Mockito.when(companyRepository.save(Mockito.any()))
                .thenReturn(Mono.just(company));


        StepVerifier.create(companyService.updateCompany(requestCompany, company.getId()))
                .consumeNextWith(response -> {
                    assertEquals(updatedCompany.getId(), response.getId());
                    assertEquals(updatedCompany.getName(), response.getName());
                })
                .verifyComplete();

    }
}
