package com.gamer.gamerbackend.unit.service;

import com.gamer.gamerbackend.dto.response.DepartmentResponse;
import com.gamer.gamerbackend.entity.Contract;
import com.gamer.gamerbackend.repository.ContractRepository;
import com.gamer.gamerbackend.service.ContractServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
@Slf4j
public class ContractServiceTest {
    @InjectMocks
    private ContractServiceImpl contractService;

    @Mock
    private ContractRepository contractRepository;

    private Contract contract;

    @BeforeEach
    private void init(){
        contract = new Contract();
        contract.setId(1);
        contract.setCompanyId(1);
        contract.setName("HR Contract");
        contract.setMinimumWorkHour(10);
        contract.setMaximumWorkHour(12);
    }

    @DisplayName("given contract id then return contract with id")
    @Test
    public void givenContractId_thenReturnContractWithContractId(){
        Mockito.when(contractRepository.findContractById(Mockito.anyInt()))
                .thenReturn(Mono.just(contract));

        StepVerifier.create(contractService.findContractById(1))
                .consumeNextWith(response ->{
                   assertEquals("HR Contract", response.getName());
                });

        Mockito.verify(contractRepository, Mockito.times(1)).findContractById(1);
    }

    @DisplayName("given a false contract id then return no contract")
    @Test
    public void givenFalseContractId_thenReturnNoContract(){
        Mockito.when(contractRepository.findContractById(Mockito.anyInt()))
                .thenReturn(Mono.empty());

        StepVerifier.create(contractService.findContractById(25))
                .verifyComplete();

        Mockito.verify(contractRepository, Mockito.times(1)).findContractById(25);
    }

    @DisplayName("given contract then return new contract")
    @Test
    public void givenContract_thenReturnInsertedContract(){
        Contract newContract = new Contract();
        newContract.setId(5);
        newContract.setCompanyId(1);
        newContract.setName("Finance Contract");
        newContract.setMinimumWorkHour(9);
        newContract.setMaximumWorkHour(10);

        Mockito.when(contractRepository.findContractById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(contractRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newContract));

        StepVerifier.create(contractService.addContract(newContract))
                .consumeNextWith(response ->{
                    assertEquals(newContract.getName(), response.getName());
                })
                .verifyComplete();

        Mockito.verify(contractRepository, Mockito.times(1)).save(newContract);
        Mockito.verify(contractRepository, Mockito.times(1)).findContractById(newContract.getId());
    }

    @DisplayName("given contract already in database then return nothing")
    @Test
    public void givenExistedContract_thenReturnNothing(){
        Contract newContract = new Contract();
        newContract.setId(contract.getId());
        newContract.setCompanyId(contract.getCompanyId());
        newContract.setName(contract.getName());
        newContract.setMinimumWorkHour(contract.getMinimumWorkHour());
        newContract.setMaximumWorkHour(contract.getMaximumWorkHour());

        Mockito.when(contractRepository.findContractById(Mockito.anyInt()))
                .thenReturn(Mono.empty());
        Mockito.when(contractRepository.save(Mockito.any()))
                .thenReturn(Mono.just(newContract));

        StepVerifier.create(contractService.addContract(newContract))
                .consumeNextWith(response -> contractService.addContract(response))
                .expectNextCount(0)
                .verifyComplete();
    }

    @DisplayName("given contract with updated value then return updated contract")
    @Test
    public void givenContractWithUpdatedValue_thenReturnUpdatedContract(){
        Contract updatedContract = new Contract();
        updatedContract.setId(contract.getId());
        updatedContract.setCompanyId(contract.getCompanyId());
        updatedContract.setName("Human Recruitment Contract");
        updatedContract.setMinimumWorkHour(contract.getMinimumWorkHour());
        updatedContract.setMaximumWorkHour(contract.getMaximumWorkHour());

        Mockito.when(contractRepository.findContractById(Mockito.anyInt()))
                .thenReturn(Mono.just(contract));
        Mockito.when(contractRepository.save(Mockito.any()))
                .thenReturn(Mono.just(contract));

        StepVerifier.create(contractService.updateContract(updatedContract, updatedContract.getId()))
                .consumeNextWith(response -> {
                    assertEquals(updatedContract.getId(), 1);
                    assertEquals(updatedContract.getName(), "Human Recruitment Contract");
                })
                .verifyComplete();
    }
}
