CREATE DATABASE IF NOT EXISTS gamer_db;
USE gamer_db;

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `company` (`id`, `name`) VALUES (1, 'Wild Rose Group');

DROP TABLE IF EXISTS `contract`;

CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum_work_hour` int(11) NOT NULL,
  `maximum_work_hour` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `contract_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `contract` (`id`, `company_id`, `name`, `minimum_work_hour`, `maximum_work_hour`) VALUES (1, 1, 'Engineering Contract', 8, 10);
INSERT INTO `contract` (`id`, `company_id`, `name`, `minimum_work_hour`, `maximum_work_hour`) VALUES (2, 1, 'Auditor Contract', 8, 10);

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (1, 1, 'Room-Mar', 20);
INSERT INTO `room` (`id`, `company_id`, `name`, `capacity`) VALUES (2, 1, 'Room-Fin', 40);

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `minimum_employee_onsite` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `company_id` (`company_id`),
  KEY `contract_id` (`contract_id`),
  CONSTRAINT `department_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `department_ibfk_2` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
  CONSTRAINT `department_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `contract_id`, `minimum_employee_onsite`) VALUES (1, 'Marketing', 1, 1, 1, 10);
INSERT INTO `department` (`id`, `name`, `company_id`, `room_id`, `contract_id`, `minimum_employee_onsite`) VALUES (2, 'Finance', 1, 2, 2, 40);


DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) UNIQUE NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255),
  `role` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (1, "placeholder@1", "placeholderusername", "placeholder_password", "Francis", "Webb", "Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (2, "placeholder@2", "placeholderusername", "placeholder_password", "Dahlia", "Rowland","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (3, "placeholder@3", "placeholderusername", "placeholder_password", "Brynne", "Dillon","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (4, "placeholder@4", "placeholderusername", "placeholder_password", "Cain", "Boyle","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (5, "placeholder@5", "placeholderusername", "placeholder_password", "Julie", "Nolan","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (6, "placeholder@6", "placeholderusername", "placeholder_password", "Samuel", "Mcclure","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (7, "placeholder@7", "placeholderusername", "placeholder_password", "Michelle", "Pitts","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (8, "placeholder@8", "placeholderusername", "placeholder_password", "Bert", "Macias","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (9, "placeholder@9", "placeholderusername", "placeholder_password", "Barrett", "Odom","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (10, "placeholder@10", "placeholderusername", "placeholder_password", "Dalton", "Willis","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (11, "placeholder@11", "placeholderusername", "placeholder_password", "Thane", "Dejesus","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (12, "placeholder@12", "placeholderusername", "placeholder_password", "Rooney", "Wood","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (13, "placeholder@13", "placeholderusername", "placeholder_password", "Anika", "Maxwell","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (14, "placeholder@14", "placeholderusername", "placeholder_password", "Cassidy", "Morton","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (15, "placeholder@15", "placeholderusername", "placeholder_password", "Jane", "Carrillo","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (16, "placeholder@16", "placeholderusername", "placeholder_password", "Yuri", "David","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (17, "placeholder@17", "placeholderusername", "placeholder_password", "Amity", "Vasquez","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (18, "placeholder@18", "placeholderusername", "placeholder_password", "Vincent", "Mcintyre","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (19, "placeholder@19", "placeholderusername", "placeholder_password", "Harper", "Reid","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (20, "placeholder@20", "placeholderusername", "placeholder_password", "Ronan", "Sellers","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (21, "placeholder@21", "placeholderusername", "placeholder_password", "Merrill", "Velez","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (22, "placeholder@22", "placeholderusername", "placeholder_password", "Belle", "Spencer","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (23, "placeholder@23", "placeholderusername", "placeholder_password", "Howard", "Snow","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (24, "placeholder@24", "placeholderusername", "placeholder_password", "Demetria", "Kramer","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (25, "placeholder@25", "placeholderusername", "placeholder_password", "Molly", "Barlow","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (26, "placeholder@26", "placeholderusername", "placeholder_password", "Kyle", "Vang","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (27, "placeholder@27", "placeholderusername", "placeholder_password", "Clinton", "Clemons","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (28, "placeholder@28", "placeholderusername", "placeholder_password", "Kadeem", "Randolph","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (29, "placeholder@29", "placeholderusername", "placeholder_password", "Ifeoma", "Petty","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (30, "placeholder@30", "placeholderusername", "placeholder_password", "Calvin", "Huber","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (31, "placeholder@31", "placeholderusername", "placeholder_password", "Velma", "Benson","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (32, "placeholder@32", "placeholderusername", "placeholder_password", "Damian", "Montgomery","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (33, "placeholder@33", "placeholderusername", "placeholder_password", "Quamar", "Garrett","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (34, "placeholder@34", "placeholderusername", "placeholder_password", "Todd", "Decker","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (35, "placeholder@35", "placeholderusername", "placeholder_password", "Danielle", "Collier","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (36, "placeholder@36", "placeholderusername", "placeholder_password", "Nero", "Cardenas","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (37, "placeholder@37", "placeholderusername", "placeholder_password", "Nadine", "Porter","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (38, "placeholder@38", "placeholderusername", "placeholder_password", "Tamara", "Bentley","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (39, "placeholder@39", "placeholderusername", "placeholder_password", "Ora", "Duke","Employee", 1, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (40, "placeholder@40", "placeholderusername", "placeholder_password", "Pearl", "Ellison","Employee", 1, 1);

INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (41, "placeholder@41", "placeholderusername", "placeholder_password", "Employee41", "41Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (42, "placeholder@42", "placeholderusername", "placeholder_password", "Employee42", "42Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (43, "placeholder@43", "placeholderusername", "placeholder_password", "Employee43", "43Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (44, "placeholder@44", "placeholderusername", "placeholder_password", "Employee44", "44Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (45, "placeholder@45", "placeholderusername", "placeholder_password", "Employee45", "45Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (46, "placeholder@46", "placeholderusername", "placeholder_password", "Employee46", "46Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (47, "placeholder@47", "placeholderusername", "placeholder_password", "Employee47", "47Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (48, "placeholder@48", "placeholderusername", "placeholder_password", "Employee48", "48Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (49, "placeholder@49", "placeholderusername", "placeholder_password", "Employee49", "49Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (50, "placeholder@50", "placeholderusername", "placeholder_password", "Employee50", "50Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (51, "placeholder@51", "placeholderusername", "placeholder_password", "Employee51", "51Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (52, "placeholder@52", "placeholderusername", "placeholder_password", "Employee52", "52Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (53, "placeholder@53", "placeholderusername", "placeholder_password", "Employee53", "53Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (54, "placeholder@54", "placeholderusername", "placeholder_password", "Employee54", "54Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (55, "placeholder@55", "placeholderusername", "placeholder_password", "Employee55", "55Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (56, "placeholder@56", "placeholderusername", "placeholder_password", "Employee56", "56Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (57, "placeholder@57", "placeholderusername", "placeholder_password", "Employee57", "57Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (58, "placeholder@58", "placeholderusername", "placeholder_password", "Employee58", "58Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (59, "placeholder@59", "placeholderusername", "placeholder_password", "Employee59", "59Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (60, "placeholder@60", "placeholderusername", "placeholder_password", "Employee60", "60Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (61, "placeholder@61", "placeholderusername", "placeholder_password", "Employee61", "61Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (62, "placeholder@62", "placeholderusername", "placeholder_password", "Employee62", "62Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (63, "placeholder@63", "placeholderusername", "placeholder_password", "Employee63", "63Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (64, "placeholder@64", "placeholderusername", "placeholder_password", "Employee64", "64Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (65, "placeholder@65", "placeholderusername", "placeholder_password", "Employee65", "65Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (66, "placeholder@66", "placeholderusername", "placeholder_password", "Employee66", "66Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (67, "placeholder@67", "placeholderusername", "placeholder_password", "Employee67", "67Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (68, "placeholder@68", "placeholderusername", "placeholder_password", "Employee68", "68Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (69, "placeholder@69", "placeholderusername", "placeholder_password", "Employee69", "69Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (70, "placeholder@70", "placeholderusername", "placeholder_password", "Employee70", "70Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (71, "placeholder@71", "placeholderusername", "placeholder_password", "Employee71", "71Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (72, "placeholder@72", "placeholderusername", "placeholder_password", "Employee72", "72Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (73, "placeholder@73", "placeholderusername", "placeholder_password", "Employee73", "73Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (74, "placeholder@74", "placeholderusername", "placeholder_password", "Employee74", "74Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (75, "placeholder@75", "placeholderusername", "placeholder_password", "Employee75", "75Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (76, "placeholder@76", "placeholderusername", "placeholder_password", "Employee76", "76Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (77, "placeholder@77", "placeholderusername", "placeholder_password", "Employee77", "77Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (78, "placeholder@78", "placeholderusername", "placeholder_password", "Employee78", "78Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (79, "placeholder@79", "placeholderusername", "placeholder_password", "Employee79", "79Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (80, "placeholder@80", "placeholderusername", "placeholder_password", "Employee80", "80Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (81, "placeholder@81", "placeholderusername", "placeholder_password", "Employee81", "81Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (82, "placeholder@82", "placeholderusername", "placeholder_password", "Employee82", "82Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (83, "placeholder@83", "placeholderusername", "placeholder_password", "Employee83", "83Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (84, "placeholder@84", "placeholderusername", "placeholder_password", "Employee84", "84Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (85, "placeholder@85", "placeholderusername", "placeholder_password", "Employee85", "85Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (86, "placeholder@86", "placeholderusername", "placeholder_password", "Employee86", "86Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (87, "placeholder@87", "placeholderusername", "placeholder_password", "Employee87", "87Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (88, "placeholder@88", "placeholderusername", "placeholder_password", "Employee88", "88Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (89, "placeholder@89", "placeholderusername", "placeholder_password", "Employee89", "89Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (90, "placeholder@90", "placeholderusername", "placeholder_password", "Employee90", "90Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (91, "placeholder@91", "placeholderusername", "placeholder_password", "Employee91", "91Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (92, "placeholder@92", "placeholderusername", "placeholder_password", "Employee92", "92Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (93, "placeholder@93", "placeholderusername", "placeholder_password", "Employee93", "93Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (94, "placeholder@94", "placeholderusername", "placeholder_password", "Employee94", "94Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (95, "placeholder@95", "placeholderusername", "placeholder_password", "Employee95", "95Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (96, "placeholder@96", "placeholderusername", "placeholder_password", "Employee96", "96Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (97, "placeholder@97", "placeholderusername", "placeholder_password", "Employee97", "97Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (98, "placeholder@98", "placeholderusername", "placeholder_password", "Employee98", "98Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (99, "placeholder@99", "placeholderusername", "placeholder_password", "Employee99", "99Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (100, "placeholder@100", "placeholderusername", "placeholder_password", "Employee100", "100Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (101, "placeholder@101", "placeholderusername", "placeholder_password", "Employee101", "101Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (102, "placeholder@102", "placeholderusername", "placeholder_password", "Employee102", "102Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (103, "placeholder@103", "placeholderusername", "placeholder_password", "Employee103", "103Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (104, "placeholder@104", "placeholderusername", "placeholder_password", "Employee104", "104Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (105, "placeholder@105", "placeholderusername", "placeholder_password", "Employee105", "105Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (106, "placeholder@106", "placeholderusername", "placeholder_password", "Employee106", "106Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (107, "placeholder@107", "placeholderusername", "placeholder_password", "Employee107", "107Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (108, "placeholder@108", "placeholderusername", "placeholder_password", "Employee108", "108Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (109, "placeholder@109", "placeholderusername", "placeholder_password", "Employee109", "109Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (110, "placeholder@110", "placeholderusername", "placeholder_password", "Employee110", "110Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (111, "placeholder@111", "placeholderusername", "placeholder_password", "Employee111", "111Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (112, "placeholder@112", "placeholderusername", "placeholder_password", "Employee112", "112Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (113, "placeholder@113", "placeholderusername", "placeholder_password", "Employee113", "113Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (114, "placeholder@114", "placeholderusername", "placeholder_password", "Employee114", "114Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (115, "placeholder@115", "placeholderusername", "placeholder_password", "Employee115", "115Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (116, "placeholder@116", "placeholderusername", "placeholder_password", "Employee116", "116Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (117, "placeholder@117", "placeholderusername", "placeholder_password", "Employee117", "117Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (118, "placeholder@118", "placeholderusername", "placeholder_password", "Employee118", "118Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (119, "placeholder@119", "placeholderusername", "placeholder_password", "Employee119", "119Employee", "Employee", 2, 1);
INSERT INTO `employee` (`id`, `email`, `username`, `password`, `first_name`, `last_name`, `role`, `department_id`, `company_id`) VALUES (120, "placeholder@120", "placeholderusername", "placeholder_password", "Employee120", "120Employee", "Employee", 2, 1);

DROP TABLE IF EXISTS `shift`;

CREATE TABLE `shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_hour` int(11) NOT NULL,
  `start_minute` int(11) NOT NULL DEFAULT 0,
  `end_hour` int(11) NOT NULL,
  `end_minute` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `shift` (`id`, `name`, `start_hour`, `start_minute`, `end_hour`, `end_minute`) VALUES (1, 'Pagi', 5, 0, 11, 0);
INSERT INTO `shift` (`id`, `name`, `start_hour`, `start_minute`, `end_hour`, `end_minute`) VALUES (2, 'Sore', 12, 0, 18, 0);
INSERT INTO `shift` (`id`, `name`, `start_hour`, `start_minute`, `end_hour`, `end_minute`) VALUES (3, 'Malam', 19, 0, 22, 0);

INSERT INTO `shift` (`id`, `name`, `start_hour`, `start_minute`, `end_hour`, `end_minute`) VALUES (4, 'Pagi', 9, 0, 17, 0);

DROP TABLE IF EXISTS `department_shift`;

CREATE TABLE `department_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`department_id`,`shift_id`),
  KEY `department_id` (`department_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `department_shift_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE,
  CONSTRAINT `department_shift_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `department_shift` (`id`, `department_id`, `shift_id`) VALUES (1, 1, 1);
INSERT INTO `department_shift` (`id`, `department_id`, `shift_id`) VALUES (2, 1, 2);
INSERT INTO `department_shift` (`id`, `department_id`, `shift_id`) VALUES (3, 1, 3);

INSERT INTO `department_shift` (`id`, `department_id`, `shift_id`) VALUES (4, 2, 4);

DROP TABLE IF EXISTS `availability`;

CREATE TABLE `availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `availability_status` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`employee_id`,`shift_id`, `day`),
  KEY `employee_id` (`employee_id`),
  KEY `department_id` (`department_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `availability_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `availability_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE,
  CONSTRAINT `availability_ibfk_3` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (1,1,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (2,2,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (3,3,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (4,4,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (5,5,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (6,6,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (7,7,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (8,8,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (9,9,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (10,10,1,1,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (31,11,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (32,12,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (33,13,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (34,14,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (35,15,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (36,16,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (37,17,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (38,18,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (39,19,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (40,20,1,2,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (41,1,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (42,2,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (43,3,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (44,4,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (45,5,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (46,6,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (47,7,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (48,8,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (49,9,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (50,10,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (51,11,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (52,12,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (53,13,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (54,14,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (55,15,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (56,16,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (57,17,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (58,18,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (59,19,1,3,"2022-03-16",1,"overseas");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (60,20,1,3,"2022-03-16",1,"overseas");

INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (61,41,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (62,42,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (63,43,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (64,44,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (65,45,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (66,46,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (67,47,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (68,48,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (69,49,2,4,"2022-03-16",2,"cuti");
INSERT INTO `availability` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `availability_status`, `reason`) VALUES (70,50,2,4,"2022-03-16",2,"cuti");

DROP TABLE IF EXISTS `schedule_request`;

CREATE TABLE `schedule_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `status` varchar(255) NOT NULL,
  `minimum_work_hour_weight` FLOAT NOT NULL,
  `maximum_work_hour_weight` FLOAT NOT NULL,
  `minimum_onsite_weight` FLOAT NOT NULL,
  `room_capacity_weight` FLOAT NOT NULL,
  `preference_weight` FLOAT NOT NULL,
  `different_employee_per_shift_weight` FLOAT NOT NULL,
  `different_employee_total_weight` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`department_id`,`start_date`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `schedule_request_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `employee_schedule`;

CREATE TABLE `employee_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_request_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `schedule_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`employee_id`,`shift_id`, `day`),
  KEY `schedule_request_id` (`schedule_request_id`),
  KEY `employee_id` (`employee_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `employee_schedule_ibfk_1` FOREIGN KEY (`schedule_request_id`) REFERENCES `schedule_request` (`id`) ON DELETE CASCADE,
  CONSTRAINT `employee_schedule_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `employee_schedule_ibfk_3` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- INSERT INTO `employee_schedule` (`id`, `schedule_request_id`, `employee_id`, `shift_id`, `day`, `schedule_status`) VALUES (1, 1, 3, 1, '2022-02-20', 0);

DROP TABLE IF EXISTS `preference`;

CREATE TABLE `preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `preference_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`employee_id`,`shift_id`, `day`),
  KEY `employee_id` (`employee_id`),
  KEY `department_id` (`department_id`),
  KEY `shift_id` (`shift_id`),
  CONSTRAINT `preference_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `preference_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE,
  CONSTRAINT `preference_ibfk_3` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (21,21,1,3,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (22,22,1,3,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (23,23,1,2,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (24,24,1,2,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (25,25,1,1,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (26,26,1,3,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (27,27,1,3,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (28,28,1,2,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (29,29,1,2,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (30,30,1,2,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (31,31,1,3,"2022-03-16",2);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (32,32,1,3,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (33,33,1,3,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (34,34,1,3,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (35,35,1,2,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (36,36,1,3,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (37,37,1,1,"2022-03-16",0);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (38,38,1,2,"2022-03-16",2);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (39,39,1,2,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (40,40,1,1,"2022-03-16",1);

INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (41,51,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (42,52,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (43,53,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (44,54,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (45,55,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (46,56,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (47,57,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (48,58,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (49,59,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (50,60,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (51,61,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (52,62,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (53,63,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (54,64,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (55,65,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (56,66,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (57,67,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (58,68,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (59,69,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (60,70,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (61,71,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (62,72,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (63,73,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (64,74,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (65,75,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (66,76,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (67,77,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (68,78,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (69,79,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (70,80,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (71,81,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (72,82,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (73,83,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (74,84,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (75,85,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (76,86,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (77,87,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (78,88,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (79,89,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (80,90,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (81,91,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (82,92,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (83,93,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (84,94,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (85,95,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (86,96,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (87,97,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (88,98,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (89,99,2,4,"2022-03-16",1);
INSERT INTO `preference` (`id`, `employee_id`, `department_id`, `shift_id`, `day`, `preference_status`) VALUES (90,100,2,4,"2022-03-16",1);
