package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.EmployeeSchedule;
import com.gamer.gamerbackend.repository.EmployeeScheduleRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Slf4j
@Service
public class EmployeeScheduleServiceImpl implements EmployeeScheduleService {
    EmployeeScheduleRepository employeeScheduleRepository;

    @Autowired
    public EmployeeScheduleServiceImpl(EmployeeScheduleRepository employeeScheduleRepository){
        this.employeeScheduleRepository = employeeScheduleRepository;
    }

    @Override
    public Mono<EmployeeSchedule> findEmployeeScheduleById(Integer id){
        return employeeScheduleRepository.findEmployeeScheduleById(id);
    }

    @Override
    public Flux<EmployeeSchedule> findEmployeeScheduleByEmployeeId(Integer employeeId){
        return employeeScheduleRepository.findByEmployeeId(employeeId);
    }

    @Override
    public Flux<EmployeeSchedule> findEmployeeScheduleByScheduleRequestId(Integer scheduleRequestId){
        return employeeScheduleRepository.findByScheduleRequestId(scheduleRequestId);
    }

    @Override
    public Flux<EmployeeSchedule> findEmployeeScheduleByEmployeeIdAndDay(Integer employeeId, LocalDate day){
        return employeeScheduleRepository.findByEmployeeIdAndDay(employeeId, day);
    }

    @Override
    public Flux<EmployeeSchedule> findEmployeeScheduleByEmployeeAndBetweenDays(Integer employeeId, String startDate, String endDate){
        return employeeScheduleRepository.findByEmployeeIdAndDayBetween(employeeId, startDate, endDate);
    }

    @Override
    public Flux<EmployeeSchedule> findEmployeeScheduleByBetweenDays(String startDate, String endDate){
        return employeeScheduleRepository.findByDayBetween(startDate, endDate);
    }

    @Override
    public Mono<EmployeeSchedule> insertEmployeeSchedule(EmployeeSchedule employeeSchedule){
        return employeeScheduleRepository.findEmployeeScheduleById(employeeSchedule.getId())
                .switchIfEmpty(employeeScheduleRepository.save(EntityUtils.persistable(employeeSchedule, true)))
                .doOnSuccess(logger -> log.info("New Schedule Inserted"));
    }

    @Override
    public Mono<EmployeeSchedule> updateEmployeeSchedule(EmployeeSchedule updatedEmployeeSchedule, Integer employeeScheduleId){
        return employeeScheduleRepository.findEmployeeScheduleById(employeeScheduleId)
                .switchIfEmpty(Mono.error(new Exception("No Schedule Found")))
                .flatMap(employeeSchedule -> {
                    Integer employeeId = updatedEmployeeSchedule.getEmployeeId() != null ? updatedEmployeeSchedule.getEmployeeId() : employeeSchedule.getEmployeeId();
                    Integer shiftId = updatedEmployeeSchedule.getShiftId() != null ? updatedEmployeeSchedule.getShiftId() : employeeSchedule.getShiftId();
                    LocalDate day = updatedEmployeeSchedule.getDay() != null ? updatedEmployeeSchedule.getDay() : employeeSchedule.getDay();
                    int scheduleStatus = updatedEmployeeSchedule.getScheduleStatus() != 0 ? updatedEmployeeSchedule.getScheduleStatus() : employeeSchedule.getScheduleStatus();

                    employeeSchedule.setEmployeeId(employeeId);
                    employeeSchedule.setShiftId(shiftId);
                    employeeSchedule.setDay(day);
                    employeeSchedule.setScheduleStatus(scheduleStatus);

                    return employeeScheduleRepository.save(employeeSchedule);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Schedule Updated"));
    }

}
