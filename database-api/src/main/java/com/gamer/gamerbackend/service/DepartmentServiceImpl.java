package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Company;
import com.gamer.gamerbackend.entity.Department;
import com.gamer.gamerbackend.entity.Room;
import com.gamer.gamerbackend.repository.*;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class DepartmentServiceImpl implements DepartmentService {
    DepartmentRepository departmentRepository;
    EmployeeRepository employeeRepository;
    ShiftRepository shiftRepository;
    CompanyRepository companyRepository;
    RoomRepository roomRepository;

    @Autowired
    public DepartmentServiceImpl(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository,
                                 ShiftRepository shiftRepository, CompanyRepository companyRepository,
                                 RoomRepository roomRepository){
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
        this.shiftRepository = shiftRepository;
        this.companyRepository = companyRepository;
        this.roomRepository = roomRepository;
    }

    @Override
    public Mono<Department> findDepartmentById(Integer id){
        return departmentRepository.findDepartmentById(id)
//                .flatMap(department -> employeeRepository.findByDepartmentId(department.getId())
//                        .collectList()
//                        .flatMap(employees -> {
//                            department.setEmployees(employees);
//                            return Mono.just(department);
//                        })
//                        .switchIfEmpty(Mono.just(department)))
                ;
    }

    @Override
    public Flux<Department> findDepartmentsByCompanyId(Integer companyId){
        return departmentRepository.findByCompanyId(companyId)
//                .flatMap(department -> employeeRepository.findByDepartmentId(department.getId())
//                        .collectList()
//                        .flatMap(employees -> {
//                            department.setEmployees(employees);
//                            return Mono.just(department);
//                        })
//                        .switchIfEmpty(Mono.just(department)))
                ;
    }

    @Override
    public Mono<Department> insertDepartment(Department department){
        return departmentRepository.findDepartmentById(department.getId())
                .switchIfEmpty(departmentRepository.save(EntityUtils.persistable(department, true)))
                .doOnSuccess(logger -> log.info("New Department Inserted: " + department.getName()));
    }

    @Override
    public Mono<Department> updateDepartment(Department updatedDepartment, Integer departmentId){
        return departmentRepository.findDepartmentById(departmentId)
                .switchIfEmpty(Mono.error(new Exception("No Department found with that id")))
                .flatMap(department1 -> {
                    Integer companyId = updatedDepartment.getCompanyId() != null ? updatedDepartment.getCompanyId() : department1.getCompanyId();
                    Integer roomId = updatedDepartment.getRoomId() != null ? updatedDepartment.getRoomId() : department1.getRoomId();
                    Integer contractId = updatedDepartment.getContractId() != null ? updatedDepartment.getContractId() : department1.getContractId();
                    String name = updatedDepartment.getName() != null ? updatedDepartment.getName() : department1.getName();
                    int minimumSite = updatedDepartment.getMinimumEmployeeOnsite() != 0 ? updatedDepartment.getMinimumEmployeeOnsite() : department1.getMinimumEmployeeOnsite();

                    department1.setCompanyId(companyId);
                    department1.setRoomId(roomId);
                    department1.setName(name);
                    department1.setMinimumEmployeeOnsite(minimumSite);
                    department1.setContractId(contractId);
                    return departmentRepository.save(department1);

                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Department Updated"));
    }
}
