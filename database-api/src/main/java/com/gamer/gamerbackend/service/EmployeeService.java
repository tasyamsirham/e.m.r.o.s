package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Employee;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EmployeeService {
    Mono<Employee> findEmployeeById(Integer id);

    Flux<Employee> findAllEmployeeByDepartmentId(Integer departmentId);

    Mono<Employee> insertEmployee(Employee employee);

    Mono<Employee> updateEmployee(Employee updatedEmployee, Integer employeeId);
}
