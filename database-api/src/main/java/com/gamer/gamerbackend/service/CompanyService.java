package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Company;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CompanyService {
    Mono<Company> findCompanyById(Integer id);

    Flux<Company> findAllCompany();

    Mono<Company> addCompany(Company company);

    Mono<Company> updateCompany(Company company, Integer id);

    void deleteCompany(Company company);
}
