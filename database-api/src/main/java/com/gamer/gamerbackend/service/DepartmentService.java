package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Company;
import com.gamer.gamerbackend.entity.Department;
import com.gamer.gamerbackend.entity.Room;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DepartmentService {
    Mono<Department> findDepartmentById(Integer id);

    Flux<Department> findDepartmentsByCompanyId(Integer companyId);

    Mono<Department> insertDepartment(Department department);

    Mono<Department> updateDepartment(Department updatedDepartment, Integer departmentId);
}
