package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.dto.converter.DepartmentConverter;
import com.gamer.gamerbackend.entity.Company;
import com.gamer.gamerbackend.repository.CompanyRepository;
import com.gamer.gamerbackend.repository.DepartmentRepository;
import com.gamer.gamerbackend.repository.EmployeeRepository;
import com.gamer.gamerbackend.repository.RoomRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Slf4j
@Service
public class CompanyServiceImpl implements CompanyService {
    CompanyRepository companyRepository;
    DepartmentRepository departmentRepository;
    RoomRepository roomRepository;
    EmployeeRepository employeeRepository;
    DepartmentConverter departmentConverter;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, DepartmentRepository departmentRepository,
                              RoomRepository roomRepository, EmployeeRepository employeeRepository, DepartmentConverter departmentConverter){
        this.companyRepository = companyRepository;
        this.departmentRepository = departmentRepository;
        this.roomRepository = roomRepository;
        this.employeeRepository = employeeRepository;
        this.departmentConverter = departmentConverter;
    }

    @Override
    public Mono<Company> findCompanyById(Integer id){
        return companyRepository.findCompanyById(id)
                .flatMap(company -> roomRepository.findByCompanyId(company.getId())
                        .collectList()
                        .flatMap(rooms -> {
                            company.setRooms(rooms);
                            return Mono.just(company);
                        })
                        .switchIfEmpty(Mono.just(company)))
                .flatMap(company -> departmentRepository.findByCompanyId(company.getId())
                        .collectList()
                        .flatMap(departmentList -> {
                            company.setDepartments(departmentConverter.convertDepartmentToDepartmentResponse(departmentList));
                            return Mono.just(company);
                        })
                        .switchIfEmpty(Mono.just(company))
                );
    }

    @Override
    public Flux<Company> findAllCompany(){
        return companyRepository.findAll()
                .flatMap(company -> roomRepository.findByCompanyId(company.getId())
                        .collectList()
                        .flatMap(rooms -> {
                            company.setRooms(rooms);
                            return Mono.just(company);
                        })
                        .switchIfEmpty(Mono.just(company)))
                .flatMap(company -> departmentRepository.findByCompanyId(company.getId())
                        .collectList()
                        .flatMap(departmentList -> {
                            company.setDepartments(departmentConverter.convertDepartmentToDepartmentResponse(departmentList));
                            return Mono.just(company);
                        })
                        .switchIfEmpty(Mono.just(company))
                );
    }

    @Override
    public Mono<Company> addCompany(Company company){
        return companyRepository.findCompanyById(company.getId())
                .switchIfEmpty(companyRepository.save(EntityUtils.persistable(company, true)))
                .doOnSuccess(logger -> log.info("New Company Inserted: " + company.getName()));

    }

    @Override
    public Mono<Company> updateCompany(Company updatedCompany, Integer id){
        return companyRepository.findCompanyById(id)
                .switchIfEmpty(Mono.error(new Exception("Company with ID " + id + " not found")))
                .flatMap(company -> {
                    company.setName(updatedCompany.getName());
                    return companyRepository.save(company);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Company Updated"));
    }

    @Override
    public void deleteCompany(Company company){
        companyRepository.delete(company);
    }
}
