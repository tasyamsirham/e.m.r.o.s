package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.dto.converter.DepartmentConverter;
import com.gamer.gamerbackend.entity.Contract;
import com.gamer.gamerbackend.repository.ContractRepository;
import com.gamer.gamerbackend.repository.DepartmentRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class ContractServiceImpl implements ContractService {
    ContractRepository contractRepository;
    DepartmentRepository departmentRepository;
    DepartmentConverter departmentConverter;

    @Autowired
    public ContractServiceImpl(ContractRepository contractRepository, DepartmentConverter departmentConverter,
                               DepartmentRepository departmentRepository){
        this.contractRepository = contractRepository;
        this.departmentRepository = departmentRepository;
        this.departmentConverter = departmentConverter;
    }

    @Override
    public Mono<Contract> findContractById(Integer id){
        return contractRepository.findContractById(id)
//                .flatMap(contract -> departmentRepository.findByContractId(contract.getId())
//                        .collectList()
//                        .flatMap(departmentList -> {
//                            contract.setDepartments(departmentConverter.convertDepartmentToDepartmentResponse(departmentList));
//                            return Mono.just(contract);
//                        })
//                        .switchIfEmpty(Mono.just(contract))
//                )
                ;
    }

    @Override
    public Flux<Contract> findAllContract(){
        return contractRepository.findAll()
//                .flatMap(contract -> departmentRepository.findByContractId(contract.getId())
//                        .collectList()
//                        .flatMap(departmentList -> {
//                            contract.setDepartments(departmentConverter.convertDepartmentToDepartmentResponse(departmentList));
//                            return Mono.just(contract);
//                        })
//                        .switchIfEmpty(Mono.just(contract))
//                )
                ;
    }

    @Override
    public Flux<Contract> findAllContractByCompanyId(Integer companyId){
        return contractRepository.findContractByCompanyId(companyId);
    }

    @Override
    public Mono<Contract> addContract(Contract contract){
        return contractRepository.findContractById(contract.getId())
                .switchIfEmpty(contractRepository.save(EntityUtils.persistable(contract, true)))
                .doOnSuccess(logger -> log.info("New Contract Inserted"));
    }

    @Override
    public Mono<Contract> updateContract(Contract contract, Integer id){
        return contractRepository.findContractById(id)
                .switchIfEmpty(Mono.error(new Exception("Contract with ID " + id + " not found")))
                .flatMap(contract1 -> {
                    String name = contract.getName() != null ? contract.getName() : contract1.getName();
                    Integer companyId = contract.getCompanyId() != null ? contract.getCompanyId() : contract1.getCompanyId();
                    Integer minimumWorkHour = contract.getMinimumWorkHour() != null ? contract.getMinimumWorkHour() : contract1.getMinimumWorkHour();
                    Integer maximumWorkHour = contract.getMaximumWorkHour() != null ? contract.getMaximumWorkHour() : contract1.getMaximumWorkHour();

                    contract1.setName(name);
                    contract1.setCompanyId(companyId);
                    contract1.setMinimumWorkHour(minimumWorkHour);
                    contract1.setMaximumWorkHour(maximumWorkHour);
                    return contractRepository.save(contract1);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Contract Updated"));
    }

    @Override
    public void deleteContract(Contract contract){
        contractRepository.delete(contract);
    }
}
