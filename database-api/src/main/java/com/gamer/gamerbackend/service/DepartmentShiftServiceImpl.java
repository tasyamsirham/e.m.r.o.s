package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.DepartmentShift;
import com.gamer.gamerbackend.repository.DepartmentShiftRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class DepartmentShiftServiceImpl implements DepartmentShiftService{
    DepartmentShiftRepository departmentShiftRepository;

    @Autowired
    public DepartmentShiftServiceImpl(DepartmentShiftRepository departmentShiftRepository){
        this.departmentShiftRepository = departmentShiftRepository;
    }

    @Override
    public Mono<DepartmentShift> findDepartmentShiftById(Integer id){
        return departmentShiftRepository.findDepartmentShiftById(id);
    }

    @Override
    public Flux<DepartmentShift> findDepartmentShiftByDepartmentId(Integer departmentId){
        return departmentShiftRepository.findDepartmentShiftByDepartmentId(departmentId);
    }

    @Override
    public Flux<DepartmentShift> findDepartmentShiftByShiftId(Integer shiftId){
        return departmentShiftRepository.findDepartmentShiftByShiftId(shiftId);
    }

    @Override
    public Flux<DepartmentShift> findAllDepartmentShift(){
        return departmentShiftRepository.findAll();
    }

    @Override
    public Mono<DepartmentShift> insertDepartmentShift(DepartmentShift departmentShift){
        return departmentShiftRepository.findDepartmentShiftById(departmentShift.getId())
                .switchIfEmpty(departmentShiftRepository.save(EntityUtils.persistable(departmentShift, true)))
                .doOnSuccess(logger -> log.info("New Department_Shift Inserted with id: " + departmentShift.getId()));
    }

    @Override
    public Mono<DepartmentShift> updateDepartmentShift(DepartmentShift departmentShift, Integer id){
        return departmentShiftRepository.findDepartmentShiftById(id)
                .switchIfEmpty(Mono.error(new Exception("Department_Shift with ID " + id + " not found")))
                .flatMap(departmentShift1 -> {
                    Integer departmentId = departmentShift.getDepartmentId() != null ? departmentShift.getDepartmentId() : departmentShift1.getDepartmentId();
                    Integer shiftId = departmentShift.getShiftId() != null ? departmentShift.getShiftId() : departmentShift1.getShiftId();

                    departmentShift1.setDepartmentId(departmentId);
                    departmentShift1.setShiftId(shiftId);
                    return departmentShiftRepository.save(departmentShift1);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Department_Shift Updated"));
    }
}
