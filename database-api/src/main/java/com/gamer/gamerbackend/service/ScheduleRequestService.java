package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.ScheduleRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ScheduleRequestService {
    Mono<ScheduleRequest> findScheduleRequestById(Integer id);

    Flux<ScheduleRequest> findAllScheduleRequest();

    Flux<ScheduleRequest> findAllScheduleRequestByDepartmentId(Integer deparmentId);

    Mono<ScheduleRequest> insertScheduleRequest(ScheduleRequest scheduleRequest);

    Mono<ScheduleRequest> updateScheduleRequest(ScheduleRequest scheduleRequest, Integer id);
}
