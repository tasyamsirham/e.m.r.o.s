package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.DepartmentShift;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DepartmentShiftService {
    Mono<DepartmentShift> findDepartmentShiftById(Integer id);

    Flux<DepartmentShift> findDepartmentShiftByDepartmentId(Integer departmentId);

    Flux<DepartmentShift> findDepartmentShiftByShiftId(Integer shiftId);

    Flux<DepartmentShift> findAllDepartmentShift();

    Mono<DepartmentShift> insertDepartmentShift(DepartmentShift departmentShift);

    Mono<DepartmentShift> updateDepartmentShift(DepartmentShift departmentShift, Integer id);
}
