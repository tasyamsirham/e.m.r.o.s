package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.ScheduleRequest;
import com.gamer.gamerbackend.repository.ScheduleRequestRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Slf4j
@Service
public class ScheduleRequestServiceImpl implements ScheduleRequestService{
    ScheduleRequestRepository scheduleRequestRepository;

    @Autowired
    private ScheduleRequestServiceImpl(ScheduleRequestRepository scheduleRequestRepository){
        this.scheduleRequestRepository = scheduleRequestRepository;
    }

    @Override
    public Mono<ScheduleRequest> findScheduleRequestById(Integer id){
        return scheduleRequestRepository.findScheduleRequestById(id);
    }

    @Override
    public Flux<ScheduleRequest> findAllScheduleRequest(){
        return scheduleRequestRepository.findAll();
    }

    @Override
    public Flux<ScheduleRequest> findAllScheduleRequestByDepartmentId(Integer deparmentId){
        return scheduleRequestRepository.findScheduleRequestByDepartmentId(deparmentId);
    }

    @Override
    public Mono<ScheduleRequest> insertScheduleRequest(ScheduleRequest scheduleRequest){
        return scheduleRequestRepository.findScheduleRequestById(scheduleRequest.getId())
                .switchIfEmpty(scheduleRequestRepository.save(EntityUtils.persistable(scheduleRequest,true)))
                .doOnSuccess(logger -> log.info("New Schedule Request Inserted with ID: " + scheduleRequest.getId()));
    }

    @Override
    public Mono<ScheduleRequest> updateScheduleRequest(ScheduleRequest scheduleRequest, Integer id){
        return scheduleRequestRepository.findScheduleRequestById(id)
                .switchIfEmpty(Mono.error(new Exception("Schedule Request with ID " + id + " does not exist")))
                .flatMap(scheduleRequest1 -> {
                    Integer departmentId = scheduleRequest.getDepartmentId() != null ? scheduleRequest.getDepartmentId() : scheduleRequest1.getDepartmentId();
                    LocalDate startDate = scheduleRequest.getStartDate() != null ? scheduleRequest.getStartDate() : scheduleRequest1.getStartDate();
                    LocalDate endDate = scheduleRequest.getEndDate() != null ? scheduleRequest.getEndDate() : scheduleRequest1.getEndDate();
                    String status = scheduleRequest.getStatus() != null ? scheduleRequest.getStatus() : scheduleRequest1.getStatus();
                    Float minimumWorkHourWeight = scheduleRequest.getMinimumWorkHourWeight() != null ? scheduleRequest.getMinimumWorkHourWeight() : scheduleRequest1.getMinimumWorkHourWeight();
                    Float maximumWorkHourWeight = scheduleRequest.getMaximumWorkHourWeight() != null ? scheduleRequest.getMaximumWorkHourWeight() : scheduleRequest1.getMaximumWorkHourWeight();
                    Float minimumOnsiteWeight = scheduleRequest.getMinimumOnsiteWeight() != null ? scheduleRequest.getMinimumOnsiteWeight() : scheduleRequest1.getMinimumOnsiteWeight();
                    Float preferenceWeight = scheduleRequest.getPreferenceWeight() != null ? scheduleRequest.getPreferenceWeight() : scheduleRequest1.getPreferenceWeight();
                    Float roomCapacityWeight = scheduleRequest.getRoomCapacityWeight() != null ? scheduleRequest.getRoomCapacityWeight() : scheduleRequest1.getRoomCapacityWeight();
                    Float differentEmployeePerShiftWeight = scheduleRequest.getDifferentEmployeePerShiftWeight() != null ? scheduleRequest.getDifferentEmployeePerShiftWeight() : scheduleRequest1.getDifferentEmployeePerShiftWeight();
                    Float differentEmployeeTotalWeight = scheduleRequest.getDifferentEmployeeTotalWeight() != null ? scheduleRequest.getDifferentEmployeeTotalWeight() : scheduleRequest1.getDifferentEmployeeTotalWeight();

                    scheduleRequest1.setDepartmentId(departmentId);
                    scheduleRequest1.setStartDate(startDate);
                    scheduleRequest1.setEndDate(endDate);
                    scheduleRequest1.setStatus(status);
                    scheduleRequest1.setMinimumWorkHourWeight(minimumWorkHourWeight);
                    scheduleRequest1.setMaximumWorkHourWeight(maximumWorkHourWeight);
                    scheduleRequest1.setMinimumOnsiteWeight(minimumOnsiteWeight);
                    scheduleRequest1.setPreferenceWeight(preferenceWeight);
                    scheduleRequest1.setRoomCapacityWeight(roomCapacityWeight);
                    scheduleRequest1.setDifferentEmployeePerShiftWeight(differentEmployeePerShiftWeight);
                    scheduleRequest1.setDifferentEmployeeTotalWeight(differentEmployeeTotalWeight);

                    return scheduleRequestRepository.save(scheduleRequest1);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger ->log.info("Schedule Request Updated"));
    }
}
