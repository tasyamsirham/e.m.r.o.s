package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Availability;
import com.gamer.gamerbackend.repository.AvailabilityRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Slf4j
@Service
public class AvailabilityServiceImpl implements AvailabilityService {
    AvailabilityRepository availabilityRepository;

    @Autowired
    public AvailabilityServiceImpl(AvailabilityRepository availabilityRepository){
        this.availabilityRepository = availabilityRepository;
    }

    @Override
    public Mono<Availability> findAvailabilityById(Integer id){
        return availabilityRepository.findAvailabilityById(id);
    }

    @Override
    public Flux<Availability> findAvailabilityByEmployeeId(Integer employeeId){
        return availabilityRepository.findByEmployeeId(employeeId);
    }

    @Override
    public Flux<Availability> findAvailabilityByEmployeeIdAndDay(Integer employeeId, LocalDate day){
        return availabilityRepository.findByEmployeeIdAndDay(employeeId, day);
    }

    @Override
    public Flux<Availability> findAvailabilityByEmployeeIdAndBetweenDay(Integer employeeId, String startDate, String endDate){
        return availabilityRepository.findByEmployeeIdAndDayBetween(employeeId, startDate, endDate);
    }

    @Override
    public Flux<Availability> findAvailabilityByDayBetween(String startDate, String endDate){
        return availabilityRepository.findByDayBetween(startDate, endDate);
    }

    @Override
    public Mono<Availability> insertAvailability(Availability availability){
        return availabilityRepository.findAvailabilityById(availability.getId())
                .switchIfEmpty(availabilityRepository.save(EntityUtils.persistable(availability, true)))
                .doOnSuccess(logger -> log.info("New Availability Inserted"));
    }

    @Override
    public Mono<Availability> updateAvailability(Availability updatedAvailability, Integer availabilityId){
        return availabilityRepository.findAvailabilityById(availabilityId)
                .switchIfEmpty(Mono.error(new Exception("No Schedule Found")))
                .flatMap(availability -> {
                    Integer departmentId = updatedAvailability.getDepartmentId() != null ? updatedAvailability.getDepartmentId() : availability.getDepartmentId();
                    Integer employeeId = updatedAvailability.getEmployeeId() != null ? updatedAvailability.getEmployeeId() : availability.getEmployeeId();
                    Integer shiftId = updatedAvailability.getShiftId() != null ? updatedAvailability.getShiftId() : availability.getShiftId();
                    LocalDate day = updatedAvailability.getDay() != null ? updatedAvailability.getDay() : availability.getDay();
                    int availabilityStatus = updatedAvailability.getAvailabilityStatus() != 0 ? updatedAvailability.getAvailabilityStatus() : availability.getAvailabilityStatus();
                    String reason = updatedAvailability.getReason() != null ? updatedAvailability.getReason() : availability.getReason();

                    availability.setEmployeeId(employeeId);
                    availability.setShiftId(shiftId);
                    availability.setDay(day);
                    availability.setAvailabilityStatus(availabilityStatus);
                    availability.setReason(reason);

                    return availabilityRepository.save(availability);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Availability Updated"));
    }
}
