package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Shift;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ShiftService {
    Mono<Shift> findShiftById(Integer id);

    Flux<Shift> findAllShift();

    Flux<Shift> findAllShiftByDepartmentId(Integer departmentId);

    Mono<Shift> insertShift(Shift shift);

    Mono<Shift> updateShift(Shift updatedShift, Integer shiftId);
}
