package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Availability;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface AvailabilityService {
    Mono<Availability> findAvailabilityById(Integer id);

    Flux<Availability> findAvailabilityByEmployeeId(Integer employeeId);

    Flux<Availability> findAvailabilityByEmployeeIdAndDay(Integer employeeId, LocalDate day);

    Flux<Availability> findAvailabilityByEmployeeIdAndBetweenDay(Integer employeeId, String startDate, String endDate);

    Flux<Availability> findAvailabilityByDayBetween(String startDate, String endDate);

    Mono<Availability> insertAvailability(Availability availability);

    Mono<Availability> updateAvailability(Availability updatedAvailability, Integer availabilityId);
}
