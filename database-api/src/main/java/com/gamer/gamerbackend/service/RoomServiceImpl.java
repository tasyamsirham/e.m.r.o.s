package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Room;
import com.gamer.gamerbackend.repository.CompanyRepository;
import com.gamer.gamerbackend.repository.DepartmentRepository;
import com.gamer.gamerbackend.repository.RoomRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class RoomServiceImpl implements RoomService {
    RoomRepository roomRepository;
    CompanyRepository companyRepository;
    DepartmentRepository departmentRepository;

    @Autowired
    public RoomServiceImpl(CompanyRepository companyRepository, RoomRepository roomRepository,
                           DepartmentRepository departmentRepository){
        this.roomRepository = roomRepository;
        this.companyRepository = companyRepository;
        this.departmentRepository = departmentRepository;
    }

    @Override
    public Mono<Room> findRoomById(Integer id){
        return roomRepository.findRoomById(id)
                .flatMap(room -> departmentRepository.findDepartmentByRoomId(room.getId())
                        .flatMap(department -> {
                            room.setDepartment(department);
                            return Mono.just(room);
                        })
                        .switchIfEmpty(Mono.just(room)));
    }

    @Override
    public Flux<Room> findAllRoomByCompanyId(Integer companyId){
        return roomRepository.findByCompanyId(companyId)
                .flatMap(room -> departmentRepository.findDepartmentByRoomId(room.getId())
                        .flatMap(department -> {
                            room.setDepartment(department);
                            return Mono.just(room);
                        })
                        .switchIfEmpty(Mono.just(room)));
    }

    @Override
    public Mono<Room> insertRoom(Room room){
        return roomRepository.findRoomById(room.getId())
                .switchIfEmpty(roomRepository.save(EntityUtils.persistable(room, true)))
                .doOnSuccess(logger -> log.info("Created new room with id: " + room.getId()));
    }

    @Override
    public Mono<Room> updateRoom(Room newRoom, Integer roomId){
        return roomRepository.findRoomById(roomId)
                .switchIfEmpty(Mono.error(new Exception("No Room found with that id")))
                .flatMap(room -> {
                    Integer companyId = newRoom.getCompanyId() != null ? newRoom.getCompanyId() : room.getCompanyId();
                    String name = newRoom.getName() != null ? newRoom.getName() : room.getName();
                    int capacity = newRoom.getCapacity() != 0 ? newRoom.getCapacity() : room.getCapacity();

                    room.setCompanyId(companyId);
                    room.setName(name);
                    room.setCapacity(capacity);
                    return roomRepository.save(room);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Room updated"));
    }
}
