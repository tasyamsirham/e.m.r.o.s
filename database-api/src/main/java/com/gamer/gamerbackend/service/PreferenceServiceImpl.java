package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Preference;
import com.gamer.gamerbackend.repository.PreferenceRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Slf4j
@Service
public class PreferenceServiceImpl implements PreferenceService{
    PreferenceRepository preferenceRepository;

    @Autowired
    public PreferenceServiceImpl(PreferenceRepository preferenceRepository){
        this.preferenceRepository = preferenceRepository;
    }

    @Override
    public Mono<Preference> findPreferenceById(Integer id){
        return preferenceRepository.findPreferenceById(id);
    }

    @Override
    public Flux<Preference> findPreferenceByEmployeeId(Integer employeeId){
        return preferenceRepository.findByEmployeeId(employeeId);
    }

    @Override
    public Flux<Preference> findPreferenceByEmployeeIdAndDay(Integer employeeId, LocalDate day){
        return preferenceRepository.findByEmployeeIdAndDay(employeeId, day);
    }

    @Override
    public Flux<Preference> findPreferenceByEmployeeIdAndBetweenDay(Integer employeeId, String startDate, String endDate){
        return preferenceRepository.findByEmployeeIdAndDayBetween(employeeId, startDate, endDate);
    }

    @Override
    public Flux<Preference> findPreferenceByDayBetween(String startDate, String endDate){
        return preferenceRepository.findByDayBetween(startDate, endDate);
    }

    @Override
    public Mono<Preference> insertPreference(Preference preference){
        return preferenceRepository.findPreferenceById(preference.getId())
                .switchIfEmpty(preferenceRepository.save(EntityUtils.persistable(preference, true)))
                .doOnSuccess(logger -> log.info("New Preference Inserted"));
    }

    @Override
    public Mono<Preference> updatePreference(Preference updatedPreference, Integer preferenceId){
        return preferenceRepository.findPreferenceById(preferenceId)
                .switchIfEmpty(Mono.error(new Exception("No Schedule Found")))
                .flatMap(preference -> {
                    Integer employeeid = updatedPreference.getEmployeeId() != null ? updatedPreference.getEmployeeId() : preference.getEmployeeId();
                    Integer departmentId = updatedPreference.getDepartmentId() != null ? updatedPreference.getDepartmentId() : preference.getDepartmentId();
                    Integer shiftId = updatedPreference.getShiftId() != null ? updatedPreference.getShiftId() : preference.getShiftId();
                    LocalDate day = updatedPreference.getDay() != null ? updatedPreference.getDay() : preference.getDay();
                    int preferenceStatus = updatedPreference.getPreferenceStatus() != 0 ? updatedPreference.getPreferenceStatus() : preference.getPreferenceStatus();

                    preference.setEmployeeId(employeeid);
                    preference.setDepartmentId(departmentId);
                    preference.setShiftId(shiftId);
                    preference.setDay(day);
                    preference.setPreferenceStatus(preferenceStatus);

                    return preferenceRepository.save(preference);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Preference Updated"));
    }
}
