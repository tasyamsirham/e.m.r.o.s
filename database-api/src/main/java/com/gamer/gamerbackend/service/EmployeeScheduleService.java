package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.EmployeeSchedule;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface EmployeeScheduleService {
    Mono<EmployeeSchedule> findEmployeeScheduleById(Integer id);

    Flux<EmployeeSchedule> findEmployeeScheduleByEmployeeId(Integer id);

    Flux<EmployeeSchedule> findEmployeeScheduleByScheduleRequestId(Integer scheduleRequestId);

    Flux<EmployeeSchedule> findEmployeeScheduleByEmployeeIdAndDay(Integer id, LocalDate day);

    Flux<EmployeeSchedule> findEmployeeScheduleByEmployeeAndBetweenDays(Integer employeeId, String startDate, String endDate);

    Flux<EmployeeSchedule> findEmployeeScheduleByBetweenDays(String startDate, String endDate);

    Mono<EmployeeSchedule> insertEmployeeSchedule(EmployeeSchedule employeeSchedule);

    Mono<EmployeeSchedule> updateEmployeeSchedule(EmployeeSchedule updatedEmployeeSchedule, Integer employeeScheduleId);
}
