package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Shift;
import com.gamer.gamerbackend.repository.ShiftRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class ShiftServiceImpl implements ShiftService{
    ShiftRepository shiftRepository;

    @Autowired
    public ShiftServiceImpl(ShiftRepository shiftRepository){
        this.shiftRepository = shiftRepository;
    }

    @Override
    public Mono<Shift> findShiftById(Integer id){
        return shiftRepository.findShiftById(id);
    }

    @Override
    public Flux<Shift> findAllShift(){
        return shiftRepository.findAll();
    }

    @Override
    public Flux<Shift> findAllShiftByDepartmentId(Integer departmentId){
        return shiftRepository.findByDepartmentId(departmentId);
    }

    @Override
    public Mono<Shift> insertShift(Shift shift){
        return shiftRepository.findShiftById(shift.getId())
                .switchIfEmpty(shiftRepository.save(EntityUtils.persistable(shift, true)))
                .doOnSuccess(logger -> log.info("New Shift Inserted: " + shift.getName()));
    }

    @Override
    public Mono<Shift> updateShift(Shift updatedShift, Integer shiftId){
        return shiftRepository.findShiftById(shiftId)
                .switchIfEmpty(Mono.error(new Exception("No Shift found with that id")))
                .flatMap(shift1 -> {
                    String name = updatedShift.getName() != null ? updatedShift.getName() : shift1.getName();
                    int startHour = updatedShift.getStartHour() != 0 ? updatedShift.getStartHour() : shift1.getStartHour();
                    int startMinute = updatedShift.getStartMinute() != 0 ? updatedShift.getStartMinute() : shift1.getStartMinute();
                    int endHour = updatedShift.getEndHour() != 0 ? updatedShift.getEndHour() : shift1.getEndHour();
                    int endMinute = updatedShift.getEndMinute() != 0 ? updatedShift.getEndMinute() : shift1.getEndMinute();

                    shift1.setName(name);
                    shift1.setStartHour(startHour);
                    shift1.setStartMinute(startMinute);
                    shift1.setEndHour(endHour);
                    shift1.setEndMinute(endMinute);

                    return shiftRepository.save(shift1);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Shift Updated"));
    }
}
