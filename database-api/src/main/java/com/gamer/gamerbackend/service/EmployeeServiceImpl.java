package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Employee;
import com.gamer.gamerbackend.repository.AvailabilityRepository;
import com.gamer.gamerbackend.repository.EmployeeRepository;
import com.gamer.gamerbackend.repository.EmployeeScheduleRepository;
import com.gamer.gamerbackend.repository.PreferenceRepository;
import com.gamer.gamerbackend.util.EntityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.transaction.Transactional;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {
    EmployeeRepository employeeRepository;
    AvailabilityRepository availabilityRepository;
    PreferenceRepository preferenceRepository;
    EmployeeScheduleRepository employeeScheduleRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, AvailabilityRepository availabilityRepository,
                               PreferenceRepository preferenceRepository, EmployeeScheduleRepository employeeScheduleRepository){
        this.employeeRepository = employeeRepository;
        this.availabilityRepository = availabilityRepository;
        this.preferenceRepository = preferenceRepository;
        this.employeeScheduleRepository = employeeScheduleRepository;

    }

    @Override
    public Mono<Employee> findEmployeeById(Integer id){
        return employeeRepository.findEmployeeById(id)
                .flatMap(employee -> availabilityRepository.findByEmployeeId(employee.getId())
                        .collectList()
                        .flatMap(availabilities -> {
                            employee.setAvailabilities(availabilities);
                            return Mono.just(employee);
                        })
                        .switchIfEmpty(Mono.just(employee))
                ).flatMap(employee -> preferenceRepository.findByEmployeeId(employee.getId())
                        .collectList()
                        .flatMap(preferences -> {
                            employee.setPreferences(preferences);
                            return Mono.just(employee);
                        })
                        .switchIfEmpty(Mono.just(employee))
                ).flatMap(employee -> employeeScheduleRepository.findByEmployeeId(employee.getId())
                        .collectList()
                        .flatMap(employeeSchedules -> {
                            employee.setEmployeeSchedules(employeeSchedules);
                            return Mono.just(employee);
                        })
                        .switchIfEmpty(Mono.just(employee))
                );
    }

    @Override
    public Flux<Employee> findAllEmployeeByDepartmentId(Integer departmentId){
        return employeeRepository.findByDepartmentId(departmentId)
                .flatMap(employee -> availabilityRepository.findByEmployeeId(employee.getId())
                            .collectList()
                            .flatMap(availabilities -> {
                                employee.setAvailabilities(availabilities);
                                return Mono.just(employee);
                            })
                            .switchIfEmpty(Mono.just(employee))
                ).flatMap(employee -> preferenceRepository.findByEmployeeId(employee.getId())
                        .collectList()
                        .flatMap(preferences -> {
                            employee.setPreferences(preferences);
                            return Mono.just(employee);
                        })
                        .switchIfEmpty(Mono.just(employee))
                ).flatMap(employee -> employeeScheduleRepository.findByEmployeeId(employee.getId())
                        .collectList()
                        .flatMap(employeeSchedules -> {
                            employee.setEmployeeSchedules(employeeSchedules);
                            return Mono.just(employee);
                        })
                        .switchIfEmpty(Mono.just(employee))
                );
    }

    @Override
    public Mono<Employee> insertEmployee(Employee employee){
        return employeeRepository.findEmployeeById(employee.getId())
                .switchIfEmpty(employeeRepository.save(EntityUtils.persistable(employee, true)))
                .doOnSuccess(logger -> log.info("New Employee Inserted: " + employee.getFirstName() + " " + employee.getLastName()));
    }

    @Override
    public Mono<Employee> updateEmployee(Employee updatedEmployee, Integer employeeId){
        return employeeRepository.findEmployeeById(employeeId)
                .switchIfEmpty(Mono.error(new Exception("No Employee found with that id")))
                .flatMap(employee -> {
                    Integer companyId = updatedEmployee.getCompanyId() != null ? updatedEmployee.getCompanyId() : employee.getCompanyId();
                    Integer departmentId = updatedEmployee.getDepartmentId() != null ? updatedEmployee.getDepartmentId() : employee.getDepartmentId();
                    String email = updatedEmployee.getEmail() != null ? updatedEmployee.getEmail() : employee.getEmail();
                    String username = updatedEmployee.getUsername() != null ? updatedEmployee.getUsername() : employee.getUsername();
                    String password = updatedEmployee.getPassword() != null ? updatedEmployee.getPassword() : employee.getPassword();
                    String firstName = updatedEmployee.getFirstName() != null ? updatedEmployee.getFirstName() : employee.getFirstName();
                    String lastName = updatedEmployee.getLastName() != null ? updatedEmployee.getLastName() : employee.getLastName();
                    String role = updatedEmployee.getRole() != null ? updatedEmployee.getRole() : employee.getRole();

                    employee.setDepartmentId(departmentId);
                    employee.setCompanyId(companyId);
                    employee.setEmail(email);
                    employee.setUsername(username);
                    employee.setPassword(password);
                    employee.setFirstName(firstName);
                    employee.setLastName(lastName);
                    employee.setRole(role);
                    return employeeRepository.save(employee);
                })
                .flatMap(Mono::just)
                .doOnSuccess(logger -> log.info("Employee Info Updated"));
    }
}
