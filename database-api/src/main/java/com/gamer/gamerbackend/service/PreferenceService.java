package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Preference;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface PreferenceService {
    Mono<Preference> findPreferenceById(Integer id);

    Flux<Preference> findPreferenceByEmployeeId(Integer employeeId);

    Flux<Preference> findPreferenceByEmployeeIdAndDay(Integer employeeId, LocalDate day);

    Flux<Preference> findPreferenceByEmployeeIdAndBetweenDay(Integer employeeId, String startDate, String endDate);

    Flux<Preference> findPreferenceByDayBetween(String startDate, String endDate);

    Mono<Preference> insertPreference(Preference preference);

    Mono<Preference> updatePreference(Preference updatedPreference, Integer preferenceId);
}
