package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Room;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RoomService {
    Mono<Room> findRoomById(Integer id);

    Flux<Room> findAllRoomByCompanyId(Integer companyId);

    Mono<Room> insertRoom(Room room);

    Mono<Room> updateRoom(Room room, Integer roomId);
}
