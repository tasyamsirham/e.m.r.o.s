package com.gamer.gamerbackend.service;

import com.gamer.gamerbackend.entity.Contract;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ContractService {
    Mono<Contract> findContractById(Integer id);

    Flux<Contract> findAllContract();

    Flux<Contract> findAllContractByCompanyId(Integer departmentId);

    Mono<Contract> addContract(Contract contract);

    Mono<Contract> updateContract(Contract contract, Integer id);

    void deleteContract(Contract contract);
}
