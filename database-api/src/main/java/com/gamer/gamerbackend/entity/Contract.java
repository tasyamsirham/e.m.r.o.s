package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.dto.response.DepartmentResponse;
import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("contract")
public class Contract extends AbstractEntity<Contract, Integer> {
    @Id
    private Integer id;
    private Integer companyId;
    private String name;
    private Integer minimumWorkHour;
    private Integer maximumWorkHour;

    @Transient
    private List<DepartmentResponse> departments;
}
