package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("schedule_request")
public class ScheduleRequest extends AbstractEntity<ScheduleRequest, Integer> {
    @Id
    private Integer id;
    private Integer departmentId;
    private LocalDate startDate;
    private LocalDate endDate;
    private String status;
    private Float minimumWorkHourWeight;
    private Float maximumWorkHourWeight;
    private Float minimumOnsiteWeight;
    private Float roomCapacityWeight;
    private Float preferenceWeight;
    private Float differentEmployeePerShiftWeight;
    private Float differentEmployeeTotalWeight;
}
