package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("room")
public class Room extends AbstractEntity<Room, Integer> {
    @Id
    private Integer id;
    private Integer companyId;
    private String name;
    private int capacity;
    @Transient
    private Department department;

}
