package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("employee")
public class Employee extends AbstractEntity<Employee, Integer> {
    @Id
    private Integer id;
    private Integer departmentId;
    private Integer companyId;
    private String email;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String role;
    @Transient
    private List<Availability> availabilities;
    @Transient
    private List<Preference> preferences;
    @Transient
    private List<EmployeeSchedule> employeeSchedules;



}
