package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import java.io.Serializable;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("preference")
public class Preference extends AbstractEntity<Preference, Integer> {
    @Id
    private Integer id;
    private Integer employeeId;
    private Integer departmentId;
    private Integer shiftId;
    private LocalDate day;
    private int preferenceStatus;

}
