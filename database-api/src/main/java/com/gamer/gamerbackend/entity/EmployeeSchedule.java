package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("employee_schedule")
public class EmployeeSchedule extends AbstractEntity<EmployeeSchedule, Integer> {
    @Id
    private Integer id;
    private Integer employeeId;
    private Integer scheduleRequestId;
    private Integer shiftId;
    private LocalDate day;
    private int scheduleStatus;

}
