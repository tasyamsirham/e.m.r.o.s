package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("availability")
public class Availability extends AbstractEntity<Availability, Integer> {
    @Id
    private Integer id;
    private Integer departmentId;
    private Integer employeeId;
    private Integer shiftId;
    private LocalDate day;
    private int availabilityStatus;
    private String reason;
}
