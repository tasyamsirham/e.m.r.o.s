package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.dto.response.DepartmentResponse;
import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.annotation.Transient;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("company")
public class Company extends AbstractEntity<Company, Integer> {
    @Id
    private Integer id;
    private String name;

    @Transient
    private List<DepartmentResponse> departments;
    @Transient
    private List<Room> rooms;
}

