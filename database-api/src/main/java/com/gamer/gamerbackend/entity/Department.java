package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.annotation.Transient;

import javax.persistence.OneToOne;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("department")
public class Department extends AbstractEntity<Department, Integer> {
    @Id
    private Integer id;
    private String name;
    private Integer companyId;
    private Integer roomId;
    private Integer contractId;
    private int minimumEmployeeOnsite;
//    @Transient
//    private List<Employee> employees;
}
