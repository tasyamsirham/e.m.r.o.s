package com.gamer.gamerbackend.entity;

import com.gamer.gamerbackend.util.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.annotation.Transient;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("shift")
public class Shift extends AbstractEntity<Shift, Integer> {
    @Id
    private Integer id;
    private String name;
    private int startHour;
    private int startMinute;
    private int endHour;
    private int endMinute;
//    @Transient
//    private List<Availability> availability;
//    @Transient
//    private List<Preference> preferences;
//    @Transient
//    private List<EmployeeSchedule> employeeSchedules;
}
