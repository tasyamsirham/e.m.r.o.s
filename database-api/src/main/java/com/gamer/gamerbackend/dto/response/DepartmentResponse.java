package com.gamer.gamerbackend.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentResponse {
    private Integer id;
    private String name;
    private Integer companyId;
    private Integer roomId;
    private int minimum_employee_onsite;
}
