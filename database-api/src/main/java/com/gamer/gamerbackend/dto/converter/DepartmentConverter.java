package com.gamer.gamerbackend.dto.converter;

import com.gamer.gamerbackend.dto.response.DepartmentResponse;
import com.gamer.gamerbackend.entity.Department;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DepartmentConverter {
    public List<DepartmentResponse> convertDepartmentToDepartmentResponse(List<Department> departmentList){
        List<DepartmentResponse> departmentResponseList = new ArrayList<DepartmentResponse>();

        departmentList.forEach(department -> {
            DepartmentResponse departmentResponse =
                    new DepartmentResponse(department.getId(), department.getName(), department.getCompanyId(),
                    department.getRoomId(), department.getMinimumEmployeeOnsite());

            departmentResponseList.add(departmentResponse);
        });

        return departmentResponseList;
    }
}
