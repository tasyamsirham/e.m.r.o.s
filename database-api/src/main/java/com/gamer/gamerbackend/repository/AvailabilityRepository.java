package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Availability;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Repository
public interface AvailabilityRepository extends ReactiveCrudRepository<Availability, Long> {
    Mono<Availability> findAvailabilityById(Integer id);

    Flux<Availability> findByEmployeeId(Integer employeeId);

    Flux<Availability> findByEmployeeIdAndDay(Integer employeeId, LocalDate day);

    Flux<Availability> findByEmployeeIdAndDayBetween(Integer employeeId, String startDate, String endDate);

    Flux<Availability> findByDayBetween(String startDate, String endDate);
}
