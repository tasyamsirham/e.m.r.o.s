package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Company;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface CompanyRepository extends ReactiveCrudRepository<Company, Integer> {
    Mono<Company> findCompanyById(Integer id);
}
