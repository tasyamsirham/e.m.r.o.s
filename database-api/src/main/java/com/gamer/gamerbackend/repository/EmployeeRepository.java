package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Employee;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

@Repository
public interface EmployeeRepository extends ReactiveCrudRepository<Employee, Long> {
    Mono<Employee> findEmployeeById(Integer id);

    Flux<Employee> findByDepartmentId(Integer departmentId);
}
