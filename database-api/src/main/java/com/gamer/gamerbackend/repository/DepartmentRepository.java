package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Department;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface DepartmentRepository extends ReactiveCrudRepository<Department, Integer> {
    Flux<Department> findByCompanyId(Integer companyId);

    Flux<Department> findByContractId(Integer contractId);

    Mono<Department> findDepartmentByName(String name);

    Mono<Department> findDepartmentById(Integer id);

    Mono<Department> findDepartmentByRoomId(Integer roomId);

}
