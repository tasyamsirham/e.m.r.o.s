package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.ScheduleRequest;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface ScheduleRequestRepository extends ReactiveCrudRepository<ScheduleRequest, Integer> {
    Mono<ScheduleRequest> findScheduleRequestById(Integer id);

    Flux<ScheduleRequest> findScheduleRequestByDepartmentId(Integer departmentId);
}
