package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Shift;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface ShiftRepository extends ReactiveCrudRepository<Shift, Integer> {
    Mono<Shift> findShiftById(Integer id);

    @Query("select s.id, s.name, s.start_hour, s.start_minute, s.end_hour, s.end_minute " +
            "from shift s, department_shift d " +
            "where d.shift_id = s.id " +
            "AND d.department_id = :departmentId")
    Flux<Shift> findByDepartmentId(Integer departmentId);
}
