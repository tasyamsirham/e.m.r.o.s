package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Room;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface RoomRepository extends ReactiveCrudRepository<Room, Integer> {
    Mono<Room> findRoomById(Integer id);
    Flux<Room> findByCompanyId(Integer companyId);
}
