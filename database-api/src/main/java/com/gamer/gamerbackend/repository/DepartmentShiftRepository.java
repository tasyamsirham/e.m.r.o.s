package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.DepartmentShift;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface DepartmentShiftRepository extends ReactiveCrudRepository<DepartmentShift, Integer> {
    Mono<DepartmentShift> findDepartmentShiftById(Integer id);

    Flux<DepartmentShift> findDepartmentShiftByDepartmentId(Integer departmentId);

    Flux<DepartmentShift> findDepartmentShiftByShiftId(Integer shiftId);
}
