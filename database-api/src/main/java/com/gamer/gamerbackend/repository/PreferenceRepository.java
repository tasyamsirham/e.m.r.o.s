package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Preference;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Repository
public interface PreferenceRepository extends ReactiveCrudRepository<Preference, Long> {
    Mono<Preference> findPreferenceById(Integer id);

    Flux<Preference> findByEmployeeId(Integer employeeId);

    Flux<Preference> findByEmployeeIdAndDay(Integer employeeId, LocalDate day);

    Flux<Preference> findByEmployeeIdAndDayBetween(Integer employeeId, String startDate, String endDate);

    Flux<Preference> findByDayBetween(String startDate, String endDate);
}
