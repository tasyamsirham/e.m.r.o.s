package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.Contract;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface ContractRepository extends ReactiveCrudRepository<Contract, Integer> {
    Mono<Contract> findContractById(Integer id);

    Flux<Contract> findContractByCompanyId(Integer companyId);
}
