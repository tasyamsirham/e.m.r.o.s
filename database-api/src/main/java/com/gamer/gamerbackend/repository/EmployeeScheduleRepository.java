package com.gamer.gamerbackend.repository;

import com.gamer.gamerbackend.entity.EmployeeSchedule;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Repository
public interface EmployeeScheduleRepository extends ReactiveCrudRepository<EmployeeSchedule,Long> {
    Mono<EmployeeSchedule> findEmployeeScheduleById(Integer id);

    Flux<EmployeeSchedule> findByEmployeeId(Integer employeeId);

    Flux<EmployeeSchedule> findByScheduleRequestId(Integer scheduleRequestId);

    Flux<EmployeeSchedule> findByEmployeeIdAndDay(Integer employeeId, LocalDate day);

    Flux<EmployeeSchedule> findByEmployeeIdAndDayBetween(Integer employeeId, String startDate, String endDate);

    Flux<EmployeeSchedule> findByDayBetween(String startDate, String endDate);
}
