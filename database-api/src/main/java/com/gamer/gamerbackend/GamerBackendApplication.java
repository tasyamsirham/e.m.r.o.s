package com.gamer.gamerbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamerBackendApplication.class, args);
	}

}
