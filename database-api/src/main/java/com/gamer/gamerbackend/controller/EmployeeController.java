package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.Employee;
import com.gamer.gamerbackend.repository.EmployeeRepository;
import com.gamer.gamerbackend.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "employee/v1")
public class EmployeeController {
    private final EmployeeService employeeService;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeController(EmployeeService employeeService, EmployeeRepository employeeRepository){
        this.employeeService = employeeService;
        this.employeeRepository = employeeRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<Employee> getEmployeeById(@PathVariable int id){
        return employeeService.findEmployeeById(id);
    }

    @GetMapping(value = "/all/department/{departmentId}")
    public Flux<Employee> getAllEmployeeByDepartment(@PathVariable int departmentId){
        return employeeService.findAllEmployeeByDepartmentId(departmentId);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Employee>> insertEmployee(@RequestBody Employee employee){
        return employeeService.insertEmployee(employee).map(employee1 -> {
            return new ResponseEntity<>(employee1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{employeeId}")
    public Mono<ResponseEntity<Employee>> updateEmployee(@RequestBody Employee employee,
                                                         @PathVariable Integer employeeId){
        return employeeService.updateEmployee(employee, employeeId).map(employee1 -> {
            return new ResponseEntity<>(employee1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{employeeId}")
    public Mono<ResponseEntity<String>> deleteEmployee(@PathVariable Integer employeeId){
        return employeeService.findEmployeeById(employeeId)
                .flatMap(employee -> {
                    return employeeRepository.delete(employee)
                            .then(Mono.just(new ResponseEntity<>("Employee Deleted", HttpStatus.OK)));
                });
    }

}
