package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.Shift;
import com.gamer.gamerbackend.repository.ShiftRepository;
import com.gamer.gamerbackend.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "shift/v1")
public class ShiftController {
    private final ShiftService shiftService;
    private final ShiftRepository shiftRepository;

    @Autowired
    public ShiftController(ShiftService shiftService,  ShiftRepository shiftRepository){
        this.shiftService = shiftService;
        this.shiftRepository = shiftRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<Shift> getShiftById(@PathVariable Integer id){
        return shiftService.findShiftById(id);
    }

    @GetMapping(value = "/all")
    public Flux<Shift> getAllShift(){
        return shiftService.findAllShift();
    }

    @GetMapping(value = "/department/{departmentId}")
    public Flux<Shift> getAllShiftByDepartmentId(@PathVariable Integer departmentId){
        return shiftService.findAllShiftByDepartmentId(departmentId);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Shift>> insertShift(@RequestBody Shift shift){
        return shiftService.insertShift(shift).map(shift1 -> {
            return new ResponseEntity<>(shift1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{shiftId}")
    public Mono<ResponseEntity<Shift>> updateShift(@RequestBody Shift shift,
                                                   @PathVariable Integer shiftId){
        return shiftService.updateShift(shift, shiftId).map(shift1 -> {
            return new ResponseEntity<>(shift1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{shiftId}")
    public Mono<ResponseEntity<String>> deleteShift(@PathVariable Integer shiftId){
        return shiftService.findShiftById(shiftId)
                .flatMap(shift -> {
                    return shiftRepository.delete(shift)
                            .then(Mono.just(new ResponseEntity<>("Shift Deleted", HttpStatus.OK)));
                });
    }
}
