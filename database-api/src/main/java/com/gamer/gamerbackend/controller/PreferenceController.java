package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.EmployeeSchedule;
import com.gamer.gamerbackend.entity.Preference;
import com.gamer.gamerbackend.repository.PreferenceRepository;
import com.gamer.gamerbackend.service.PreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@RestController
@RequestMapping(value = "preference/v1")
public class PreferenceController {
    private final PreferenceService preferenceService;
    private final PreferenceRepository preferenceRepository;

    @Autowired
    public PreferenceController(PreferenceService preferenceService, PreferenceRepository preferenceRepository){
        this.preferenceService = preferenceService;
        this.preferenceRepository = preferenceRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<Preference> getPreferenceById(@PathVariable Integer id){
        return preferenceService.findPreferenceById(id);
    }

    @GetMapping(value = "/all/employee/{employeeId}")
    public Flux<Preference> getPreferenceByEmployeeId(@PathVariable Integer employeeId){
        return preferenceService.findPreferenceByEmployeeId(employeeId);
    }

    @GetMapping(value = "/all/employee")
    public Flux<Preference> getPreferenceByEmployeeAndDate(@RequestParam(value = "employeeId") Integer employeeId,
                                                                       @RequestParam(value = "day1", required=false) String day1,
                                                                       @RequestParam(value = "day2", required=false) String day2){
        if (day1 == null && day2 == null){
            return preferenceService.findPreferenceByEmployeeId(employeeId);
        }else if (day2 == null){
            LocalDate startDate = LocalDate.parse(day1);
            return preferenceService.findPreferenceByEmployeeIdAndDay(employeeId, startDate);
        }
        return preferenceService.findPreferenceByEmployeeIdAndBetweenDay(employeeId, day1, day2);
    }

    @GetMapping(value = "/all/date")
    public Flux<Preference> getPreferenceByDate(@RequestParam(value = "day1", required=false) String day1,
                                                @RequestParam(value = "day2", required=false) String day2){
        if (day2 == null){
            day2 = day1;
        }
        return preferenceService.findPreferenceByDayBetween(day1, day2);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Preference>> insertPreference(@RequestBody Preference preference){
        return preferenceService.insertPreference(preference).map(preference1 -> {
            return new ResponseEntity<>(preference1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{preferenceId}")
    public Mono<ResponseEntity<Preference>> updatePreference(@RequestBody Preference preference,
                                                             @PathVariable Integer preferenceId){
        return preferenceService.updatePreference(preference, preferenceId).map(preference1 -> {
            return new ResponseEntity<>(preference1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "delete/{preferenceId}")
    public Mono<ResponseEntity<String>> deletePreference(@PathVariable Integer preferenceId){
        return preferenceService.findPreferenceById(preferenceId)
                .flatMap(preference -> {
                return preferenceRepository.delete(preference)
                        .then(Mono.just(new ResponseEntity<>("Preference Has Been Deleted", HttpStatus.OK)));
            });
    }
}
