package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.Department;
import com.gamer.gamerbackend.repository.DepartmentRepository;
import com.gamer.gamerbackend.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "department/v1")
public class DepartmentController {
    private final DepartmentService departmentService;
    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentController(DepartmentService departmentService, DepartmentRepository departmentRepository){
        this.departmentService = departmentService;
        this.departmentRepository = departmentRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<Department> getDepartmentById(@PathVariable Integer id){
        return departmentService.findDepartmentById(id);
    }

    @GetMapping(value = "/all/{companyId}")
    public Flux<Department> getDepartmentsByCompanyId(@PathVariable Integer companyId){
        return departmentService.findDepartmentsByCompanyId(companyId);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Department>> insertDepartment(@RequestBody Department department){
        return departmentService.insertDepartment(department).map(department1 -> {
            return new ResponseEntity<>(department1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{departmentId}")
    public Mono<ResponseEntity<Department>> updateDepartment(@RequestBody Department department,
                                                             @PathVariable Integer departmentId){
        return departmentService.updateDepartment(department, departmentId).map(department1 -> {
            return new ResponseEntity<>(department1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{departmentId}")
    public Mono<ResponseEntity<String>> deleteDepartment(@PathVariable Integer departmentId){
        return departmentService.findDepartmentById(departmentId)
                .flatMap(department -> {
                    return departmentRepository.delete(department)
                            .then(Mono.just(new ResponseEntity<>("Department deleted", HttpStatus.OK)));
                });
    }

}
