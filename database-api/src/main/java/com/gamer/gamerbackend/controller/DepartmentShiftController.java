package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.DepartmentShift;
import com.gamer.gamerbackend.repository.DepartmentShiftRepository;
import com.gamer.gamerbackend.service.DepartmentShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "department-shift/v1")
public class DepartmentShiftController {
    private final DepartmentShiftService departmentShiftService;
    private final DepartmentShiftRepository departmentShiftRepository;

    @Autowired
    public DepartmentShiftController(DepartmentShiftService departmentShiftService, DepartmentShiftRepository departmentShiftRepository){
        this.departmentShiftService = departmentShiftService;
        this.departmentShiftRepository = departmentShiftRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<DepartmentShift> getDepartmentShiftById(@PathVariable Integer id){
        return departmentShiftService.findDepartmentShiftById(id);
    }

    @GetMapping(value = "/department/{departmentId}")
    public Flux<DepartmentShift> getDepartmentShiftBytDepartmentId(@PathVariable Integer departmentId){
        return departmentShiftService.findDepartmentShiftByDepartmentId(departmentId);
    }

    @GetMapping(value = "/shift/{shiftId}")
    public Flux<DepartmentShift> getDepartmentShiftByShiftId(@PathVariable Integer shiftId){
        return departmentShiftService.findDepartmentShiftByShiftId(shiftId);
    }

    @GetMapping(value = "/all")
    public Flux<DepartmentShift> getAllDepartmentShift(){
        return departmentShiftService.findAllDepartmentShift();
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<DepartmentShift>> insertDepartmentShift(@RequestBody DepartmentShift departmentShift){
        return departmentShiftService.insertDepartmentShift(departmentShift).map(departmentShift1 -> {
            return new ResponseEntity<>(departmentShift1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{id}")
    public Mono<ResponseEntity<DepartmentShift>> updateDepartmentShift(@PathVariable Integer id,
                                                                       @RequestBody DepartmentShift departmentShift){
        return departmentShiftService.updateDepartmentShift(departmentShift, id).map(departmentShift1 -> {
            return new ResponseEntity<>(departmentShift1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{id}")
    public Mono<ResponseEntity<String>> deleteDepartmentShift(@PathVariable Integer id){
        return departmentShiftRepository.findDepartmentShiftById(id)
                .flatMap(departmentShift -> {
                    return departmentShiftRepository.delete(departmentShift)
                            .then(Mono.just(new ResponseEntity<>("Department_Shift Deleted", HttpStatus.OK)));
                });
    }
}
