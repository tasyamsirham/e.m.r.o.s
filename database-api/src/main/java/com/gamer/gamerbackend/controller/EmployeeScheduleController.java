package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.EmployeeSchedule;
import com.gamer.gamerbackend.service.EmployeeScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@RestController
@RequestMapping(value = "employeeschedule/v1")
public class EmployeeScheduleController {
    private final EmployeeScheduleService employeeScheduleService;

    @Autowired
    public EmployeeScheduleController (EmployeeScheduleService employeeScheduleService){
        this.employeeScheduleService = employeeScheduleService;
    }

    @GetMapping(value = "/{id}")
    public Mono<EmployeeSchedule> getEmployeeScheduleById(@PathVariable Integer id){
        return employeeScheduleService.findEmployeeScheduleById(id);
    }

    @GetMapping(value = "/all/employee/{employeeId}")
    public Flux<EmployeeSchedule> getEmployeeScheduleByEmployeeId(@PathVariable Integer employeeId){
        return employeeScheduleService.findEmployeeScheduleByEmployeeId(employeeId);
    }

    @GetMapping(value = "/schedule-request/{scheduleRequestId}")
    public Flux<EmployeeSchedule> getEmployeeScheduleByScheduleRequestId(@PathVariable Integer scheduleRequestId){
        return employeeScheduleService.findEmployeeScheduleByScheduleRequestId(scheduleRequestId);
    }

    @GetMapping(value = "/all/employee")
    public Flux<EmployeeSchedule> getEmployeeScheduleByEmployeeAndDate(@RequestParam(value = "employeeId") Integer employeeId,
                                                                       @RequestParam(value = "day1", required=false) String day1,
                                                                       @RequestParam(value = "day2", required=false) String day2){
        if (day1 == null && day2 == null){
            return employeeScheduleService.findEmployeeScheduleByEmployeeId(employeeId);
        }else if (day2 == null){
            LocalDate startDate = LocalDate.parse(day1);
            return employeeScheduleService.findEmployeeScheduleByEmployeeIdAndDay(employeeId, startDate);
        }
        return employeeScheduleService.findEmployeeScheduleByEmployeeAndBetweenDays(employeeId, day1, day2);
    }

    @GetMapping(value = "/all/date")
    public Flux<EmployeeSchedule> getEmployeeScheduleBetweenDays(@RequestParam(value = "day1", required=false) String day1,
                                                                 @RequestParam(value = "day2", required=false) String day2){
        if (day2 == null){
            day2 = day1;
        }

        return employeeScheduleService.findEmployeeScheduleByBetweenDays(day1, day2);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<EmployeeSchedule>> insertEmployeeSchedule(@RequestBody EmployeeSchedule employeeSchedule){
        return employeeScheduleService.insertEmployeeSchedule(employeeSchedule).map(employeeSchedule1 -> {
            return new ResponseEntity<>(employeeSchedule1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{employeeScheduleId}")
    public Mono<ResponseEntity<EmployeeSchedule>> updateEmployeeSchedule(@RequestBody EmployeeSchedule employeeSchedule,
                                                                         @PathVariable Integer employeeScheduleId){
        return employeeScheduleService.updateEmployeeSchedule(employeeSchedule, employeeScheduleId).map(employeeSchedule1 -> {
            return new ResponseEntity<>(employeeSchedule1, HttpStatus.OK);
        });
    }

}
