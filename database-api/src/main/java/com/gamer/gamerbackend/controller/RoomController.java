package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.Room;
import com.gamer.gamerbackend.repository.RoomRepository;
import com.gamer.gamerbackend.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "room/v1")
public class RoomController {
    private final RoomService roomService;
    private final RoomRepository roomRepository;

    @Autowired
    public RoomController(RoomService roomService, RoomRepository roomRepository){
        this.roomService = roomService;
        this.roomRepository = roomRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<Room> getRoomById(@PathVariable Integer id){
        return roomService.findRoomById(id);
    }

    @GetMapping(value = "/all/{companyId}")
    public Flux<Room> getAllRoomByCompanyId(@PathVariable Integer companyId){
        return roomService.findAllRoomByCompanyId(companyId);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Room>> insertRoom(@RequestBody Room room){
        return roomService.insertRoom(room).map(room1 -> {
            return new ResponseEntity<>(room1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{roomId}")
    public Mono<ResponseEntity<Room>> updateRoom(@RequestBody Room room,
                                                 @PathVariable Integer roomId){
        return roomService.updateRoom(room, roomId).map(room1 -> {
            return new ResponseEntity<>(room1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{roomId}")
    public Mono<ResponseEntity<String>> deleteRoom(@PathVariable Integer roomId){
        return roomService.findRoomById(roomId)
                .flatMap(room -> {
                    return roomRepository.delete(room)
                            .then(Mono.just(new ResponseEntity<>("Room deleted", HttpStatus.OK)));
                });
    }
}
