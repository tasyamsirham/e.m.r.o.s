package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.Company;
import com.gamer.gamerbackend.repository.CompanyRepository;
import com.gamer.gamerbackend.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "company/v1")
public class CompanyController {
    private final CompanyService companyService;
    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyController(CompanyService companyService, CompanyRepository companyRepository){
        this.companyService = companyService;
        this.companyRepository = companyRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<Company> getCompanyById(@PathVariable Integer id){
        return companyService.findCompanyById(id);
    }

    @GetMapping(value = "/all")
    public Flux<Company> getAllCompany(){
        return companyService.findAllCompany();
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Company>> addCompany(@RequestBody Company company) {
        return companyService.addCompany(company).map(company1 -> {
            return new ResponseEntity<>(company1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{id}")
    public Mono<ResponseEntity<Company>> updateCompany(@PathVariable Integer id,
                                                       @RequestBody Company company){
        return companyService.updateCompany(company, id).map(company1 -> {
            return new ResponseEntity<>(company1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{id}")
    public Mono<ResponseEntity<String>> deleteCompany(@PathVariable Integer id){
        return companyService.findCompanyById(id)
                .flatMap(company -> {
                    return companyRepository.delete(company)
                            .then(Mono.just(new ResponseEntity<>("Company deleted", HttpStatus.OK)));
                });
    }


}
