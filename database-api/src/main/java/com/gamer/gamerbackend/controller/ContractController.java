package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.Contract;
import com.gamer.gamerbackend.repository.ContractRepository;
import com.gamer.gamerbackend.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "contract/v1")
public class ContractController {
    private final ContractService contractService;
    private final ContractRepository contractRepository;

    @Autowired
    public ContractController(ContractService contractService, ContractRepository contractRepository){
        this.contractService = contractService;
        this.contractRepository = contractRepository;
    }

    @GetMapping(value = "/{id}")
    public Mono<Contract> getContractById(@PathVariable Integer id){
        return contractService.findContractById(id);
    }

    @GetMapping(value = "/all")
    public Flux<Contract> getAllContract(){
        return contractService.findAllContract();
    }

    @GetMapping(value = "/company/{companyId}")
    public Flux<Contract> getAllContractByCompanyId(@PathVariable Integer companyId){
        return contractService.findAllContractByCompanyId(companyId);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Contract>> addContract(@RequestBody Contract contract){
        return contractService.addContract(contract).map(contract1 -> {
            return new ResponseEntity<>(contract1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{id}")
    public Mono<ResponseEntity<Contract>> updateContract(@PathVariable Integer id,
                                                         @RequestBody Contract contract){
        return contractService.updateContract(contract, id).map(contract1 -> {
            return new ResponseEntity<>(contract1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{id}")
    public Mono<ResponseEntity<String>> deleteContract(@PathVariable Integer id){
        return contractService.findContractById(id).
                flatMap(contract -> {
                    return contractRepository.delete(contract)
                            .then(Mono.just(new ResponseEntity<>("Contract Deleted", HttpStatus.OK)));
        });
    }
}
