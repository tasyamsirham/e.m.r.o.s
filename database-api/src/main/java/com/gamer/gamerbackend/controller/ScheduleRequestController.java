package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.ScheduleRequest;
import com.gamer.gamerbackend.repository.ScheduleRequestRepository;
import com.gamer.gamerbackend.service.ScheduleRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "schedule-request/v1")
public class ScheduleRequestController {
    private final ScheduleRequestService scheduleRequestService;
    private final ScheduleRequestRepository scheduleRequestRepository;

    @Autowired
    public ScheduleRequestController(ScheduleRequestService scheduleRequestService, ScheduleRequestRepository scheduleRequestRepository){
        this.scheduleRequestService = scheduleRequestService;
        this.scheduleRequestRepository = scheduleRequestRepository;
    }

    @GetMapping(value = "/{id}")
    private Mono<ScheduleRequest> getScheduleRequestById(@PathVariable Integer id){
        return scheduleRequestService.findScheduleRequestById(id);
    }

    @GetMapping(value = "/all")
    private Flux<ScheduleRequest> getAllScheduleRequest(){
        return scheduleRequestService.findAllScheduleRequest();
    }

    @GetMapping(value = "/department/{departmentId}")
    private Flux<ScheduleRequest> getAllScheduleRequestByDepartmentId(@PathVariable Integer departmentId){
        return scheduleRequestService.findAllScheduleRequestByDepartmentId(departmentId);
    }

    @PostMapping(value = "/add")
    private Mono<ResponseEntity<ScheduleRequest>> insertScheduleRequest(@RequestBody ScheduleRequest scheduleRequest){
        return scheduleRequestService.insertScheduleRequest(scheduleRequest).map(scheduleRequest1 -> {
            return new ResponseEntity<>(scheduleRequest1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{id}")
    private Mono<ResponseEntity<ScheduleRequest>> updateScheduleRequest(@PathVariable Integer id,
                                                                        @RequestBody ScheduleRequest scheduleRequest){
        return scheduleRequestService.updateScheduleRequest(scheduleRequest, id).map(scheduleRequest1 -> {
            return new ResponseEntity<>(scheduleRequest1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{id}")
    private Mono<ResponseEntity<String>> deleteScheduleRequest(@PathVariable Integer id){
        return scheduleRequestService.findScheduleRequestById(id)
                .flatMap(scheduleRequest -> {
                    return scheduleRequestRepository.delete(scheduleRequest)
                            .then(Mono.just(new ResponseEntity<>("Schedule Request Deleted", HttpStatus.OK)));
                });
    }
}
