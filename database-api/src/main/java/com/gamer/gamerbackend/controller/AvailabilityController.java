package com.gamer.gamerbackend.controller;

import com.gamer.gamerbackend.entity.Availability;
import com.gamer.gamerbackend.repository.AvailabilityRepository;
import com.gamer.gamerbackend.service.AvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@RestController
@RequestMapping(value = "availability/v1")
public class AvailabilityController {
    private final AvailabilityService availabilityService;
    private final AvailabilityRepository availabilityRepository;

    @Autowired
    public AvailabilityController(AvailabilityService availabilityService, AvailabilityRepository availabilityRepository){
        this.availabilityRepository = availabilityRepository;
        this.availabilityService = availabilityService;
    }

    @GetMapping(value = "/{id}")
    public Mono<Availability> getAvailabilityById(@PathVariable Integer id){
        return availabilityService.findAvailabilityById(id);
    }

    @GetMapping(value = "/all/employee/{employeeId}")
    public Flux<Availability> getAvailabilityByEmployee(@PathVariable Integer employeeId){
        return availabilityService.findAvailabilityByEmployeeId(employeeId);
    }

    @GetMapping(value = "/all/employee")
    public Flux<Availability> getAvailabilityByEmployeeAndDate(@RequestParam(value = "employeeId") Integer employeeId,
                                                               @RequestParam(value = "day1", required=false) String day1,
                                                               @RequestParam(value = "day2", required=false) String day2){
        if (day1 == null && day2 == null){
            return availabilityService.findAvailabilityByEmployeeId(employeeId);
        }else if (day2 == null){
            LocalDate startDate = LocalDate.parse(day1);
            return availabilityService.findAvailabilityByEmployeeIdAndDay(employeeId, startDate);
        }
        return availabilityService.findAvailabilityByEmployeeIdAndBetweenDay(employeeId, day1, day2);
    }

    @GetMapping(value = "/all/date")
    public Flux<Availability> getAvailabilityByDate(@RequestParam(value = "day1", required=false) String day1,
                                                    @RequestParam(value = "day2", required=false) String day2){
        if (day2 == null){
            day2 = day1;
        }
        return availabilityService.findAvailabilityByDayBetween(day1, day2);
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<Availability>> insertAvailability(@RequestBody Availability availability){
        return availabilityService.insertAvailability(availability).map(availability1 -> {
            return new ResponseEntity<>(availability1, HttpStatus.CREATED);
        });
    }

    @PutMapping(value = "/update/{availabilityId}")
    public Mono<ResponseEntity<Availability>> updateAvailability(@RequestBody Availability availability,
                                                                 @PathVariable Integer availabilityId){
        return availabilityService.updateAvailability(availability, availabilityId).map(availability1 -> {
            return new ResponseEntity<>(availability1, HttpStatus.OK);
        });
    }

    @DeleteMapping(value = "/delete/{availabilityId}")
    public Mono<ResponseEntity<String>> deleteAvailability(@PathVariable Integer availabilityId){
        return availabilityService.findAvailabilityById(availabilityId)
                .flatMap(availability -> {
                    return availabilityRepository.delete(availability)
                            .then(Mono.just(new ResponseEntity<>("Availability Has Been Deleted", HttpStatus.OK)));
                });
    }

}
