provider "google" {
  project = "tugas-akhir-348208"
  region  = "asia-southeast2"
  zone    = "asia-southeast2-a"
}

# Create a single Compute Engine instance Master
resource "google_compute_instance" "master" {
  name         = "p-emros-master-01"
  machine_type = "e2-medium"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

# Create a single Compute Engine instance Master
resource "google_compute_instance" "frontend" {
  for_each     = toset(["01"])
  name         = "p-emros-frontend-01"
  machine_type = "e2-small"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

# Create a single Compute Engine instance Master
resource "google_compute_instance" "load-balancer" {
  name         = "p-emros-lb-01"
  machine_type = "e2-standard-2"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

# Create more Compute Engine instance Workers
resource "google_compute_instance" "worker-small" {
  for_each     = toset(["01","02"])
  name         = "p-emros-worker-small-${each.key}"
  machine_type = "e2-small"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size = "15"
    }
  }

  network_interface {
    network = "default"
  }
}

# Create more Compute Engine instance Workers
resource "google_compute_instance" "worker-medium" {
  for_each     = toset(["01","02","03","04"])
  name         = "p-emros-worker-medium-${each.key}"
  machine_type = "e2-medium"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size = "15"
    }
  }

  network_interface {
    network = "default"
  }
}

# Create more Compute Engine instance CI/CD
resource "google_compute_instance" "cd-emros" {
  name         = "p-emros-cd"
  machine_type = "e2-small"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}